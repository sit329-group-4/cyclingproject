module spi_controller
(
	input i_clk,
	
	input i_sclk,
	input i_ss_n,
	input i_mosi,
	
	output o_miso,
	
	// Data
	input [2:0] i_gear_setting,
	input [17:0] i_weight,
	input [7:0] i_wheel_resistance,
	input [20:0] i_cadence,
	input [20:0] i_power,
	input [6:0] i_sweat_level,
	output reg [14:0] o_speed,
	output reg [8:0] o_wind_direction,
	output reg [14:0] o_wind_magnitude,
	output reg [14:0] o_cadence_pulse,
	output reg signed [7:0] o_cur_slope,
	output reg signed [7:0] o_front_wind_resistance
);

	localparam REG_GEAR_SETTING = 1,
				REG_WEIGHT = 2,
				REG_WHEEL_RESISTANCE = 3,
				REG_CADENCE = 4,
				REG_POWER = 5,
				REG_SWEAT_LEVEL = 6,
				REG_SPEED = 7,
				REG_WIND_DIRECTION = 8,
				REG_WIND_MAGNITUDE = 9,
				REG_CADENCE_PULSE = 10,
				REG_CUR_SLOPE = 11,
				REG_FRONT_WIND_RESISTANCE = 12;

	wire w_busy;
	
	reg r_st_load_en;
	reg r_st_load_trdy;
	reg r_st_load_rrdy;
	reg r_st_load_roe;
	
	wire w_trdy;
	wire w_rrdy;
	wire w_roe;
	
	reg r_tx_load_en;
	reg [31:0] r_tx_load_data;
	
	reg r_rx_req;
	wire [31:0] w_rx_data;
	
	spi_slave
	#(
		.cpol(1'b0),
		.cpha(1'b0),
		.d_width(32)
	)
	spi_slave
	(
		.sclk(i_sclk),
		.reset_n(1'b1),
		.ss_n(i_ss_n),
		.mosi(i_mosi),
		.miso(o_miso),
		
		.busy(w_busy),
		
		.st_load_en(r_st_load_en),
		.st_load_trdy(r_st_load_trdy),
		.st_load_rrdy(r_st_load_rrdy),
		.st_load_roe(r_st_load_roe),
		
		.trdy(w_trdy),
		.rrdy(w_rrdy),
		.roe(w_roe),
		
		.tx_load_en(r_tx_load_en),
		.tx_load_data(r_tx_load_data),
		
		.rx_req(r_rx_req),
		.rx_data(w_rx_data)
	);
	
	always @(*)
	begin
		r_rx_req = 0;
		
		// When it isn't busy, is when we start doing stuff 
		if (!w_busy)
		begin
			if (w_rrdy)
			begin
				r_rx_req = 1;
			end
		end
	end
	
	// In this block we set the data for sending
	always @(*)
	begin
		r_tx_load_en = 0;
		
		case (w_rx_data[31:27])
			REG_GEAR_SETTING:
				begin
					r_tx_load_data = {29'd0, i_gear_setting};
				end
			REG_WEIGHT:
				begin
					r_tx_load_data = {14'd0, i_weight};
				end
			REG_WHEEL_RESISTANCE:
				begin
					r_tx_load_data = {24'd0, i_wheel_resistance};
				end
			REG_CADENCE:
				begin
					r_tx_load_data = {11'd0, i_cadence};
				end
			REG_POWER:
				begin
					r_tx_load_data = {11'd0, i_power};
				end
			REG_SWEAT_LEVEL:
				begin
					r_tx_load_data = {25'd0, i_sweat_level};
				end
			default:
				begin
					r_tx_load_data = 32'd0;
				end
		endcase
		
		if (!w_busy)
		begin
			case (w_rx_data[31:27])
				REG_GEAR_SETTING:
					begin
						r_tx_load_en = 1;
					end
				REG_WEIGHT:
					begin
						r_tx_load_en = 1;
					end
				REG_WHEEL_RESISTANCE:
					begin
						r_tx_load_en = 1;
					end
				REG_CADENCE:
					begin
						r_tx_load_en = 1;
					end
				REG_POWER:
					begin
						r_tx_load_en = 1;
					end
				REG_SWEAT_LEVEL:
					begin
						r_tx_load_en = 1;
					end
			endcase
		end
	end
	
	// Load data into registers
	always @(posedge i_clk)
	begin
		o_speed <= o_speed;
		o_wind_direction <= o_wind_direction;
		o_wind_magnitude <= o_wind_magnitude;
		o_cadence_pulse <= o_cadence_pulse;
		o_cur_slope <= o_cur_slope;
		o_front_wind_resistance <= o_front_wind_resistance;
		
		case (w_rx_data[31:27])
			REG_SPEED:
				begin
					o_speed <= w_rx_data[14:0];
				end
			REG_WIND_DIRECTION:
				begin
					o_wind_direction <= w_rx_data[8:0];
				end
			REG_WIND_MAGNITUDE:
				begin
					o_wind_magnitude <= w_rx_data[14:0];
				end
			REG_CADENCE_PULSE:
				begin
					o_cadence_pulse <= w_rx_data[14:0];
				end
			REG_CUR_SLOPE:
				begin
					o_cur_slope <= w_rx_data[7:0];
				end
			REG_FRONT_WIND_RESISTANCE:
				begin
					o_front_wind_resistance <= w_rx_data[7:0];
				end
		endcase
	end
	
endmodule
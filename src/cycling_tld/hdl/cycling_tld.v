// ===== Cycling TLD =====
// --- Overview ---
//		This is the overall top level design for the cycling project.
//		It integrates all the components together into one design
//
// --- Design ---
//		Combine everything together

module cycling_tld
(
	input i_clk,
	
	input i_button_1,
	input i_button_2,
	input i_button_board,
	
	input i_bpm_input,
	
	output o_led_board,
	output [7:0] o_leds,
	
	// SPI Related
	input i_spi_sclk,
	input i_spi_ss_n,
	input i_spi_mosi,
	output o_spi_miso
);
	
	// === Constants ===
	localparam CLK_SPEED = 50_000_000;
	
	// --- Cadence ---
	localparam CADENCE_IPS = 1000;  // "Increments Per Second". For Cadence detection
	
	// --- Wheel Resistance ---
	//     These change the weightings of the wheel resistance controller
	//     Allowing fine adjustment of the ride feel
	localparam WHEEL_RES_BASE_RESISTANCE = 18'sb0000001010_00000000;
	localparam WHEEL_RES_SLOPE_WEIGHTING = 18'sb00000000001_00000000;
	localparam WHEEL_RES_WIND_RESISTANCE_WEIGHTING = 18'sb0000000001_00000000;
	localparam WHEEL_RES_WEIGHT_WEIGHTING = 18'sb0000000001_00000000;
	
	localparam WHEEL_RES_GEAR_SETTING_WEIGHTING = 18'sb0000000001_00000000;
	localparam WHEEL_RES_GEAR_1_VALUE = 10'd10;
	localparam WHEEL_RES_GEAR_2_VALUE = 10'd20;
	localparam WHEEL_RES_GEAR_3_VALUE = 10'd30;
	localparam WHEEL_RES_GEAR_4_VALUE = 10'd40;
	localparam WHEEL_RES_GEAR_5_VALUE = 10'd50;
	
	
	// ===  Inputs & Outputs ===
	// --- SPI Signals ---
	wire [14:0] w_speed;  // Q7.8. Range 0.0 - 100.0
	wire [8:0] w_wind_direction; // 0 - 359
	wire [14:0] w_wind_magnitude;  // 0 - 100% (Q7.8)
	wire [14:0] w_cadence_pulse_number;
	wire signed [7:0] w_cur_slope;  // -100 - 100% inclusive both sides
	wire signed [7:0] w_front_wind_resistance;
	
	// --- Board Button Debouncer ---
	wire w_button_board_debounced;
	
	// --- Cadence Detector ---
	wire w_cadence_pulse;
	wire [20:0] w_cadence;  // In RPM (Q9.12)
	
	// --- Weight Detector ---
	wire [17:0] w_weight;  // In Kg (Q10.8)
	
	wire w_weight_button;
	wire w_weight_led;
	
	// --- Power Detector ---
	wire [20:0] w_power;  // In Watts (Q11.10)
	
	// -- Gear Detector ---
	wire [2:0] w_gear_setting;  // From 0 - 4 inclusive (Symbolic for 1 - 5 gears)
	
	// push buttons to go up and down gears
	wire w_gear_up; 
	wire w_gear_down;
	
	// --- Biometrics Detector ---
	wire [6:0] w_sweat_level;  // 0 - 100%
	wire [18:0] w_bpm;
	
	// --- Wheel Resistance Controller ---
	wire [7:0] w_wheel_resistance;  // 0 - 100% inclusive both sides
	
	// --- Wind Resistance Controller ---
	wire signed [7:0] w_wind_resistance;  // -100 - 100% inclusive both sides *Confirm this is correct
	wire [14:0] w_calculated_wind_magnitude;  // 0 - 100% (Q7.8) * NOTE: Reminder to check why this is fixed point
	wire [8:0] w_calculated_wind_direction;  // 0 - 100%
	
	// --- Wind Controller ---
	
	// --- Wheel Resistance Driver ---
	wire w_wheel_resistance_pwm;
	
	// --- Fan Driver ---
	wire w_fan_1;
	wire w_fan_2;
	wire w_fan_3;
	wire w_fan_4;
	wire w_fan_5;
	wire w_fan_6;
	
	// --- Assignments ---
	assign w_weight_button = w_button_board_debounced;
	assign w_gear_up = i_button_1;
	assign w_gear_down = i_button_2;
	
	assign o_led_board = w_weight_led;
	assign o_leds = {2'b00, w_fan_6, w_fan_5, w_fan_4, w_fan_3, w_fan_2, w_fan_1};
	
	// SPI
	spi_controller spi_controller
	(
		.i_clk(i_clk),
		.i_sclk(i_spi_sclk),
		.i_ss_n(i_spi_ss_n),
		.i_mosi(i_spi_mosi),
		.o_miso(o_spi_miso),
		.i_gear_setting(w_gear_setting),
		.i_weight(w_weight),
		.i_wheel_resistance(w_wheel_resistance),
		.i_cadence(w_cadence),
		.i_power(w_power),
		.i_sweat_level(w_sweat_level),
		.o_speed(w_speed),
		.o_wind_direction(w_wind_direction),
		.o_wind_magnitude(w_wind_magnitude),
		.o_cadence_pulse(w_cadence_pulse_number),
		.o_cur_slope(w_cur_slope),
		.o_front_wind_resistance(w_front_wind_resistance)
	);
	
	// === Debouncers ===
	// Only needed for the sensor board button. The built-in buttons work fine as is
	debounce
	#(
		.clk_freq(CLK_SPEED),
		.stable_time(10)
	)
	board_debouncer
	(
		.clk(i_clk),
		.reset_n(1'b1),
		.button(i_button_board),
		.result(w_button_board_debounced)
	);
	
	
	// === Detectors ===
	cadence_detector
	#(
		.CLK_SPEED(CLK_SPEED),
		.INCREMENTS_PER_SECOND(CADENCE_IPS)
	)
	cadence_detector
	(
		.i_clk(i_clk),
		.i_cadence_pulse(w_cadence_pulse),
		.o_cadence(w_cadence)  // In RPM (Q9.12)
	);
	
	weight_detector weight_detector
	(
		.clk(i_clk),
		.btn(w_weight_button),
		.main_led(w_weight_led),
		.weight(w_weight)  // In Kg (Q10.8)
	);
	
	power_detector power_detector
	(
		.i_wheel_resistance(w_wheel_resistance),  // 0-100% inclusive both sides
		.i_cadence(w_cadence),  // In RPM (Q9.12)
		.o_power(w_power)  // In Watts (Q11.10)
	);
	
	gear_detector gear_detector
	(
		.i_clk(i_clk),
		.i_gear_up(w_gear_up),
		.i_gear_down(w_gear_down),
		.o_gear_setting(w_gear_setting),
		.o_seven_seg()
	);
	
	biometrics_detector biometrics_detector (
		.clk(i_clk),
		.btn(i_bpm_input),
		.bpm(w_bpm)  // [18:0]
	);
	
	
	// === Controllers ===
	wheel_resistance_controller
	#(
		.BASE_RESISTANCE(WHEEL_RES_BASE_RESISTANCE),
		.SLOPE_WEIGHTING(WHEEL_RES_SLOPE_WEIGHTING),
		.WIND_RESISTANCE_WEIGHTING(WHEEL_RES_WIND_RESISTANCE_WEIGHTING),
		.WEIGHT_WEIGHTING(WHEEL_RES_WEIGHT_WEIGHTING),
		
		.GEAR_SETTING_WEIGHTING(WHEEL_RES_GEAR_SETTING_WEIGHTING),
		.GEAR_1_VALUE(WHEEL_RES_GEAR_1_VALUE),
		.GEAR_2_VALUE(WHEEL_RES_GEAR_2_VALUE),
		.GEAR_3_VALUE(WHEEL_RES_GEAR_3_VALUE),
		.GEAR_4_VALUE(WHEEL_RES_GEAR_4_VALUE),
		.GEAR_5_VALUE(WHEEL_RES_GEAR_5_VALUE)
	)
	wheel_resistance_controller
	(
		.i_current_slope(w_cur_slope),  // -100 - 100% inclusive both sides
		.i_gear_setting(w_gear_setting),  // From 0 - 4 inclusive (Symbolic for 1 - 5 gears)
		.i_wind_resistance(w_front_wind_resistance),  // -100 - 100% inclusive both sides *Confirm this is correct
		.i_weight(w_weight),  // In Kg (Q10.8)
		.o_wheel_resistance(w_wheel_resistance)  // 0 - 100% inclusive both sides
	);
	
	wind_controller wind_controller
	(
		.i_clk(i_clk),
		.i_speed(w_speed),  // Q7.8. Range 0.0 - 100.0
		.i_wind_magnitude(w_wind_magnitude),  // Q7.8. Range 0.0 - 100.0
		.i_wind_direction(w_wind_direction),  // Range 0 - 359
		.o_normalised_magnitude(w_calculated_wind_magnitude), // Q7.8. Range 0.0 - 100.0
		.o_normalised_direction(w_calculated_wind_direction)  // Range 0 - 359
	);
	
	
	// === Drivers ===
	wheel_resistance_driver wheel_resistance_driver
	(
		.clk(i_clk),
		.PWM_in(w_wheel_resistance),  // 0 - 100% inclusive both sides
		.PWM_out(w_wheel_resistance_pwm)
	);
	
	fan_driver fan_driver
	(
		.i_clk(i_clk),
		.i_sweat_level(w_sweat_level),  // 0 - 100%
		.i_wind_magnitude(w_calculated_wind_magnitude),  // Q7.8. Range 0 - 100%
		.i_wind_direction(w_calculated_wind_direction),  // 0 - 100%
		
		.o_fan_1(w_fan_1),
		.o_fan_2(w_fan_2),
		.o_fan_3(w_fan_3),
		.o_fan_4(w_fan_4),
		.o_fan_5(w_fan_5),
		.o_fan_6(w_fan_6)
	);
	
endmodule
transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl {E:/FPGA Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl/PWM.v}
vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl {E:/FPGA Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl/minus_one.v}
vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl {E:/FPGA Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl/down_clocking_odd.v}
vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl {E:/FPGA Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl/down_clocking_even.v}
vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl {E:/FPGA Projects/cyclingproject/src/components/drivers/wheel_resistance_driver/hdl/wheel_resistance_driver.v}


module percent_pwm
(
	input i_clk,
	
	input [5:0] i_divider_amount,
	input [6:0] i_pwm_period,
	
	output reg o_pwm
);

	// Store the inverted PWM period. Otherwise 100 would mean the pwm would be always low instead of always high
	wire [6:0] w_pwm_period;
	
	wire w_divider_out;
	
	reg [6:0] r_counter = 0;

	clk_divider clk_divider
	(
		.i_clk(i_clk),
		.i_divider_amount(i_divider_amount),
		.o_divider(w_divider_out)
	);
	
	assign w_pwm_period = 7'd100 - i_pwm_period;
	
	always @(posedge i_clk)
	begin
		o_pwm <= 1'b0;
	
		if (w_divider_out)
		begin
			if (r_counter == 99)
			begin
				r_counter <= 0;
			end
			else
			begin
				r_counter <= r_counter + 7'd1;
			end
		end
		
		if (r_counter >= w_pwm_period)
		begin
			o_pwm <= 1'b1;
		end
	end
	

endmodule
module clk_divider
(
	input i_clk,
	
	input [5:0] i_divider_amount,
	
	output reg o_divider = 0
);

	reg [32:0] r_count = 0;
		
	always @(posedge i_clk)
	begin
		if (i_divider_amount > 1)
		begin
			o_divider <= 0;
		
			if (r_count == (1 << i_divider_amount - 1))
			begin
				o_divider <= 1;
				r_count <= 0;
			end
			else
			begin
				r_count <= r_count + 1;
			end
		end
		else
		begin
			o_divider <= 1;
		end
	end

endmodule
//

module fan_multiplier
#(
	parameter INPUT_INTEGER_BITS = 7,
	parameter INPUT_FRACTIONAL_BITS = 0,
	parameter MULTIPLIER_FRACTIONAL_BITS = 8
)
(
	// Qx.y
	input [INPUT_INTEGER_BITS + INPUT_FRACTIONAL_BITS - 1:0] i_value,
	// Q2.8
	input [2+MULTIPLIER_FRACTIONAL_BITS-1:0] i_multiplier,
	
	// Range of 0 - 100
	output [6:0] o_fan_percentage
);
	
	localparam MULTIPLIER_INTEGER_BITS = 2;
	localparam MULTIPLIER_TOTAL = MULTIPLIER_INTEGER_BITS + MULTIPLIER_FRACTIONAL_BITS;
	
	// How many bits the input needs to be shifted by to get it to the same number of
	// fractional bits as the multiplier
	localparam INPUT_FRACTIONAL_ADJUSTED = MULTIPLIER_FRACTIONAL_BITS - INPUT_FRACTIONAL_BITS;
	
	// The result of the multiplication.
	// It will be Q9.16 as it is the result of Q7.8 * Q2.8
	reg [INPUT_INTEGER_BITS + MULTIPLIER_FRACTIONAL_BITS + MULTIPLIER_TOTAL - 1:0] r_multiply_result;
	
	// Clamp the result to keep it within the range 0 - 100
	clamper
	#(
		.FRACTIONAL_BITS(MULTIPLIER_FRACTIONAL_BITS+MULTIPLIER_FRACTIONAL_BITS)
	)
	clamper
	(
		.i_unclamped(r_multiply_result),
		.o_clamped(o_fan_percentage)
	);
	
	// Needs to take an INPUT_TOTAL bit and a Q2.8 and multiply them
	// First it needs to convert the first number to Q7.8
	// Then multiply and the result will be a Q9.16 (25 bits total)
	always @(*)
	begin: multiplier_block
		reg [INPUT_INTEGER_BITS+MULTIPLIER_FRACTIONAL_BITS-1:0] a_value_shifted;  // Q7.8
		
		a_value_shifted = i_value << INPUT_FRACTIONAL_ADJUSTED;
		r_multiply_result = a_value_shifted * i_multiplier;
	end
	
endmodule
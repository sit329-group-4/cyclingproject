//


module slipstream_processor
(
	// Range 0 - 359 (360+ means no obstruction)
	input [8:0] i_angle,

	// Range 0 - 100
	input [6:0] i_fan_1,
	input [6:0] i_fan_2,
	input [6:0] i_fan_3,
	input [6:0] i_fan_4,
	input [6:0] i_fan_5,
	input [6:0] i_fan_6,
	
	// Range 0 - 100
	output [6:0] o_fan_1,
	output [6:0] o_fan_2,
	output [6:0] o_fan_3,
	output [6:0] o_fan_4,
	output [6:0] o_fan_5,
	output [6:0] o_fan_6
);
	
	// Slipstream fan multipliers
	// All of them are Q2.8 with range 1.0 - 0.0
	wire [9:0] w_fan_1_multiplier;
	wire [9:0] w_fan_2_multiplier;
	wire [9:0] w_fan_3_multiplier;
	wire [9:0] w_fan_4_multiplier;
	wire [9:0] w_fan_5_multiplier;
	wire [9:0] w_fan_6_multiplier;
	
	// The lookup table of fan multipliers.
	// Precalculated by running the script in ./hdl/converter
	slipstream_lut slipstream_lut
	(
		.i_angle(i_angle),
		.o_fan_1(w_fan_1_multiplier),
		.o_fan_2(w_fan_2_multiplier),
		.o_fan_3(w_fan_3_multiplier),
		.o_fan_4(w_fan_4_multiplier),
		.o_fan_5(w_fan_5_multiplier),
		.o_fan_6(w_fan_6_multiplier)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0)
	)
	fan_multiplier_1
	(
		.i_value(i_fan_1),
		.i_multiplier(w_fan_1_multiplier),
		.o_fan_percentage(o_fan_1)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0)
	)
	fan_multiplier_2
	(
		.i_value(i_fan_2),
		.i_multiplier(w_fan_2_multiplier),
		.o_fan_percentage(o_fan_2)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0)
	)
	fan_multiplier_3
	(
		.i_value(i_fan_3),
		.i_multiplier(w_fan_3_multiplier),
		.o_fan_percentage(o_fan_3)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0)
	)
	fan_multiplier_4
	(
		.i_value(i_fan_4),
		.i_multiplier(w_fan_4_multiplier),
		.o_fan_percentage(o_fan_4)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0)
	)
	fan_multiplier_5
	(
		.i_value(i_fan_5),
		.i_multiplier(w_fan_5_multiplier),
		.o_fan_percentage(o_fan_5)
	);
	
	fan_multiplier#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0)
	)
	fan_multiplier_6
	(
		.i_value(i_fan_6),
		.i_multiplier(w_fan_6_multiplier),
		.o_fan_percentage(o_fan_6)
	);
	
endmodule
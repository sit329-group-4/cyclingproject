// This module clamps the value to 0 - 100, and also rounds up as necessary

module clamper
#(
	parameter FRACTIONAL_BITS = 16
)
(
	// Q9.16
	input [9+FRACTIONAL_BITS-1:0] i_unclamped,
	
	// Range 0 - 100
	output reg [6:0] o_clamped
);

	// Clamps the input to the upper value of 100
	// Input is Q9.16
	// Return value is Q8.1
	function automatic [8:0] upper_clamp (input [9+FRACTIONAL_BITS-1:0] fi_unclamped);
		begin
			// If the value is greater than or equal 100, return 100
			// Otherwise return the value in Q8.1
			// The equals is needed because we don't want 100.5 to round up after it has already done the upper-limit check
			if (fi_unclamped >= (25'd100 << FRACTIONAL_BITS))
			begin
				upper_clamp = (9'd100 << 1);
			end
			else
			begin
				upper_clamp = fi_unclamped[9-1+FRACTIONAL_BITS-1:FRACTIONAL_BITS-1];
			end
		end
	endfunction
	
	// Rounds a clamped value
	// Input is Q8.1
	// Output is 7 bits with range 0 - 100
	function automatic [6:0] round(input [8:0] fi_clamped);
		reg [8:0] accumulator;  // Q8.1
		begin
			if (fi_clamped[0] == 1'b1)
			begin
				accumulator = {(fi_clamped[8:1] + 8'd1), 1'b0};
			end
			else
			begin
				accumulator = fi_clamped;
			end
			
			round = accumulator[7:1];
		end
	endfunction
	
	// The first rounding block
	always @(*)
	begin
		o_clamped = round(upper_clamp(i_unclamped));
	end
	
endmodule
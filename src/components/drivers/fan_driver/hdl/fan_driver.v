// ===== Fan Driver =====
// --- Design Doc ---
//		Description: 
//			The fan driver will be responsible for cooling the user down when they are sweating excessively or for
//			simulating crosswinds and headwinds. 
//
//		System Integrations: 
//			Input Systems (If any): 
//				Biometrics (Sweat) – This module will take in the sweat input from the biometrics module to work
//										out if the user is sweating enough to need cooling 
//				Wind Controller – This module will take in the wind information to know how to change the fan ratios
//									to simulate the passed in wind 
//
//			Output Systems (If any): 
//				The fan driver module will not output to any other systems. 
//
//		Inputs (and what they mean): 
//			Sweat level – The amount the user is sweating. A certain amount will be too much and the system will begin
//							cooling down the user 
//			Wind Range, Magnitude, and Offset – tells the fan controller from which direction, the strength, and how to
//												occlude the wind that is being sent to it. 
//												By occlude I mean the offset for the range where the wind is
//												blocked (@Matt Merrett for a better explanation) 
//		Outputs: 
//			Fan 0-X PWM – Will output PWM signals to each fan to set them to a given speed depending on the direction they
//							blow onto the user. The fan driver module will output in RPM to each of the fans how fast the
//							fan should be blowing onto the user.
//
//		Considerations: 
//			Just like the fan driver, this will use a PWM controller. They shouldn’t be too hard to implement
//			and there are plenty of example implementations and IP cores out there to do it for us. 
//
//		Testbench Notes:
//			The percentages on the outputs are sometimes out by up to about 18-20%, but only about maybe 5 in 500 times from a random selection.
//			The rest of the time it is within 5% of expected values
//			I could go in and hand tune the tables or add some extra fractional bits but I think this should probably be good enough.
//			NOTE: I went in an increased the number of fractional bits to 30 instead of 8. no discernable difference.
//					That leads me to believe it is somewhere else. 
//			Either way, I think this is good enough

module fan_driver
(
	input i_clk,

	// Range 0 - 100
	input [6:0] i_sweat_level,
	// Q7.8. Range 0.0 - 100.0
	input [14:0] i_wind_magnitude,
	// Range 0 - 359
	input [8:0] i_wind_direction,
	
	// Range 0 - 359 (360+ means no obstruction)
	input [8:0] i_slipstream_angle,

	// PWM Outputs
	output o_fan_1,
	output o_fan_2,
	output o_fan_3,
	output o_fan_4,
	output o_fan_5,
	output o_fan_6,
	
	// Debug Outputs
	// Mostly for testbench usage
	// Range 0 - 100
	output [6:0] d_fan_1,
	output [6:0] d_fan_2,
	output [6:0] d_fan_3,
	output [6:0] d_fan_4,
	output [6:0] d_fan_5,
	output [6:0] d_fan_6
);

	localparam SWEAT_ACTIVATION_LEVEL = 6'd50,
		SWEAT_DEACTIVATION_LEVEL = 6'd35;
		
	// Specifies if cooling is needed due to the current sweat level
	reg r_cooling_needed = 0;
	
	// all of the fan multipliers are Q2.30
	// This is the fan multiplier value to use for wind coming from a specific direction
	wire [31:0] w_fan_1_multiplier;
	wire [31:0] w_fan_2_multiplier;
	wire [31:0] w_fan_3_multiplier;
	wire [31:0] w_fan_4_multiplier;
	wire [31:0] w_fan_5_multiplier;
	wire [31:0] w_fan_6_multiplier;
	
	// Temporary storage for half of the multiplier for each
	// Verilog was being a pain and wouldn't let me slice the halved value in-place so I have to store it temporarily
	// Used for when the user needs cooling. It takes the 0-100% range and changes it to 0-50% so it can be used with
	// the base 50 value that is added
	reg [63:0] r_fan_1_multiplier_half = 0;
	reg [63:0] r_fan_2_multiplier_half = 0;
	reg [63:0] r_fan_3_multiplier_half = 0;
	reg [63:0] r_fan_4_multiplier_half = 0;
	reg [63:0] r_fan_5_multiplier_half = 0;
	reg	[63:0] r_fan_6_multiplier_half = 0;
	
	// Represents that current fan multiplier, whether the full range or the half range when cooling is needed
	// Q2.30
	reg [31:0] r_fan_1_multiplier_processed = 0;
	reg [31:0] r_fan_2_multiplier_processed = 0;
	reg [31:0] r_fan_3_multiplier_processed = 0;
	reg [31:0] r_fan_4_multiplier_processed = 0;
	reg [31:0] r_fan_5_multiplier_processed = 0;
	reg [31:0] r_fan_6_multiplier_processed = 0;
	
	// Stores the results of the fan multiplications
	// Then it is passed through the slipstream processor
	wire [6:0] w_fan_1_multiplied;
	wire [6:0] w_fan_2_multiplied;
	wire [6:0] w_fan_3_multiplied;
	wire [6:0] w_fan_4_multiplied;
	wire [6:0] w_fan_5_multiplied;
	wire [6:0] w_fan_6_multiplied;
	
	wire [6:0] w_fan_1_slipstreamed;
	wire [6:0] w_fan_2_slipstreamed;
	wire [6:0] w_fan_3_slipstreamed;
	wire [6:0] w_fan_4_slipstreamed;
	wire [6:0] w_fan_5_slipstreamed;
	wire [6:0] w_fan_6_slipstreamed;
	
	// The final fan output percentage.
	// The fan output percentage but with the base 50 cooling amount added
	// Gets sent to the 6 PWM controllers
	reg [6:0] r_fan_1 = 0;
	reg [6:0] r_fan_2 = 0;
	reg [6:0] r_fan_3 = 0;
	reg [6:0] r_fan_4 = 0;
	reg [6:0] r_fan_5 = 0;
	reg [6:0] r_fan_6 = 0;
	
	// Debug Ports
	// These are setup so that the testbench is more easily able to access
	// the values that the module is calculating so it can be compared to
	// expected values.
	assign d_fan_1 = r_fan_1;
	assign d_fan_2 = r_fan_2;
	assign d_fan_3 = r_fan_3;
	assign d_fan_4 = r_fan_4;
	assign d_fan_5 = r_fan_5;
	assign d_fan_6 = r_fan_6;

	// Fan lookup table for the different multipliers based on direction
	fan_lut fan_lookup
	(
		.i_angle(i_wind_direction),
		.o_fan_1(w_fan_1_multiplier), // all of the fan multipliers are Q2.8
		.o_fan_2(w_fan_2_multiplier),
		.o_fan_3(w_fan_3_multiplier),
		.o_fan_4(w_fan_4_multiplier),
		.o_fan_5(w_fan_5_multiplier),
		.o_fan_6(w_fan_6_multiplier)
	);
	
	// Fan Multipliers for each fan
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	fan_multiplier_1
	(
		.i_value(i_wind_magnitude),
		.i_multiplier(r_fan_1_multiplier_processed),
		.o_fan_percentage(w_fan_1_multiplied)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	fan_multiplier_2
	(
		.i_value(i_wind_magnitude),
		.i_multiplier(r_fan_2_multiplier_processed),
		.o_fan_percentage(w_fan_2_multiplied)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	fan_multiplier_3
	(
		.i_value(i_wind_magnitude),
		.i_multiplier(r_fan_3_multiplier_processed),
		.o_fan_percentage(w_fan_3_multiplied)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	fan_multiplier_4
	(
		.i_value(i_wind_magnitude),
		.i_multiplier(r_fan_4_multiplier_processed),
		.o_fan_percentage(w_fan_4_multiplied)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	fan_multiplier_5
	(
		.i_value(i_wind_magnitude),
		.i_multiplier(r_fan_5_multiplier_processed),
		.o_fan_percentage(w_fan_5_multiplied)
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	fan_multiplier_6
	(
		.i_value(i_wind_magnitude),
		.i_multiplier(r_fan_6_multiplier_processed),
		.o_fan_percentage(w_fan_6_multiplied)
	);
	
	// Slipstream processor for the fans
	/* slipstream_processor slipstream_processor
	(
		// 0 - 359 (360+ means no obstruction)
		.i_angle(i_slipstream_angle),

		// range 0 - 100
		.i_fan_1(w_fan_1_multiplied),
		.i_fan_2(w_fan_2_multiplied),
		.i_fan_3(w_fan_3_multiplied),
		.i_fan_4(w_fan_4_multiplied),
		.i_fan_5(w_fan_5_multiplied),
		.i_fan_6(w_fan_6_multiplied),
		
		// range 0 - 100
		.o_fan_1(w_fan_1_slipstreamed),
		.o_fan_2(w_fan_2_slipstreamed),
		.o_fan_3(w_fan_3_slipstreamed),
		.o_fan_4(w_fan_4_slipstreamed),
		.o_fan_5(w_fan_5_slipstreamed),
		.o_fan_6(w_fan_6_slipstreamed)
	); */
	
	assign w_fan_1_slipstreamed = w_fan_1_multiplied;
	assign w_fan_2_slipstreamed = w_fan_2_multiplied;
	assign w_fan_3_slipstreamed = w_fan_3_multiplied;
	assign w_fan_4_slipstreamed = w_fan_4_multiplied;
	assign w_fan_5_slipstreamed = w_fan_5_multiplied;
	assign w_fan_6_slipstreamed = w_fan_6_multiplied;
	
	// PWM drivers for the different fans.
	percent_pwm fan_1_pwm_driver
	(
		.i_clk(i_clk),
		.i_divider_amount(6'd0),
		.i_pwm_period(r_fan_1),
		.o_pwm(o_fan_1)
	);
	
	percent_pwm fan_2_pwm_driver
	(
		.i_clk(i_clk),
		.i_divider_amount(6'd0),
		.i_pwm_period(r_fan_2),
		.o_pwm(o_fan_2)
	);
	
	percent_pwm fan_3_pwm_driver
	(
		.i_clk(i_clk),
		.i_divider_amount(6'd0),
		.i_pwm_period(r_fan_3),
		.o_pwm(o_fan_3)
	);
	
	percent_pwm fan_4_pwm_driver
	(
		.i_clk(i_clk),
		.i_divider_amount(6'd0),
		.i_pwm_period(r_fan_4),
		.o_pwm(o_fan_4)
	);
	
	percent_pwm fan_5_pwm_driver
	(
		.i_clk(i_clk),
		.i_divider_amount(6'd0),
		.i_pwm_period(r_fan_5),
		.o_pwm(o_fan_5)
	);
	
	percent_pwm fan_6_pwm_driver
	(
		.i_clk(i_clk),
		.i_divider_amount(6'd0),
		.i_pwm_period(r_fan_6),
		.o_pwm(o_fan_6)
	);
	
	// Sweat Level Detection Block
	// When the user begins to sweat too much, cooling is set to have a base level of 50% plus the normal fan percentage/50%
	always @(*)
	begin
		if (r_cooling_needed == 1)
		begin
			r_cooling_needed = 1;
			
			if (i_sweat_level < SWEAT_DEACTIVATION_LEVEL)
			begin
				r_cooling_needed = 0;
			end
		end
		else
		begin
			r_cooling_needed = 0;
			
			if (i_sweat_level >= SWEAT_ACTIVATION_LEVEL)
			begin
				r_cooling_needed = 1;
			end
		end
	end
	
	// This block processes the multipliers for the fans
	// If cooling is needed the multiplier is halved
	always @(*)
	begin
		r_fan_1_multiplier_half = (w_fan_1_multiplier * (1'b1 << 29));
		r_fan_2_multiplier_half = (w_fan_2_multiplier * (1'b1 << 29));
		r_fan_3_multiplier_half = (w_fan_3_multiplier * (1'b1 << 29));
		r_fan_4_multiplier_half = (w_fan_4_multiplier * (1'b1 << 29));
		r_fan_5_multiplier_half = (w_fan_5_multiplier * (1'b1 << 29));
		r_fan_6_multiplier_half = (w_fan_6_multiplier * (1'b1 << 29));
	
		if (r_cooling_needed)
		begin // Q2.30 + Q2.30 = Q4.60
			r_fan_1_multiplier_processed = r_fan_1_multiplier_half >> 30;
			r_fan_2_multiplier_processed = r_fan_2_multiplier_half >> 30;
			r_fan_3_multiplier_processed = r_fan_3_multiplier_half >> 30;
			r_fan_4_multiplier_processed = r_fan_4_multiplier_half >> 30;
			r_fan_5_multiplier_processed = r_fan_5_multiplier_half >> 30;
			r_fan_6_multiplier_processed = r_fan_6_multiplier_half >> 30;
		end
		else
		begin
			r_fan_1_multiplier_processed = w_fan_1_multiplier;
			r_fan_2_multiplier_processed = w_fan_2_multiplier;
			r_fan_3_multiplier_processed = w_fan_3_multiplier;
			r_fan_4_multiplier_processed = w_fan_4_multiplier;
			r_fan_5_multiplier_processed = w_fan_5_multiplier;
			r_fan_6_multiplier_processed = w_fan_6_multiplier;
		end
	end
	
	// If cooling is needed, adds a base 50, otherwise outputs at the full range
	always @(posedge i_clk)
	begin
		if (r_cooling_needed == 1'b1)
		begin
			r_fan_1 = 7'd50 + w_fan_1_slipstreamed;
			r_fan_2 = 7'd50 + w_fan_2_slipstreamed;
			r_fan_3 = 7'd50 + w_fan_3_slipstreamed;
			r_fan_4 = 7'd50 + w_fan_4_slipstreamed;
			r_fan_5 = 7'd50 + w_fan_5_slipstreamed;
			r_fan_6 = 7'd50 + w_fan_6_slipstreamed;
		end
		else
		begin
			r_fan_1 = w_fan_1_slipstreamed;
			r_fan_2 = w_fan_2_slipstreamed;
			r_fan_3 = w_fan_3_slipstreamed;
			r_fan_4 = w_fan_4_slipstreamed;
			r_fan_5 = w_fan_5_slipstreamed;
			r_fan_6 = w_fan_6_slipstreamed;
		end
	end
endmodule
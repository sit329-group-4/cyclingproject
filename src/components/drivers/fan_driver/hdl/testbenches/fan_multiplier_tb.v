// Testbench for testing fan_multiplier

`timescale 1ns/1ns

module fan_multiplier_tb;
	
	`include "../../../misc/tb_functions/assert_display.v"
	
	integer t_case = 0;
	wire integer t_total_cases;
	
	// IO for UUT1
	// Inputs
	wire [6:0] t_value_1;
	wire [9:0] t_multiplier_1;  // Q2.8
	
	// Outputs
	wire [6:0] t_fan_percentage_1;  // Range 0 - 100
	wire [6:0] t_fan_percentage_1_expected;  // Range 0 - 100
	
	// IO for UUT2
	// Inputs
	wire [14:0] t_value_2;  // Q7.8
	wire [31:0] t_multiplier_2;  // Q2.30
	
	// Outputs
	wire [6:0] t_fan_percentage_2;  // Range 0 - 100
	wire [6:0] t_fan_percentage_2_expected;  // Range 0 - 100
	
	// Instantiate the Units Under Test (UUTs)
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(0),
		.MULTIPLIER_FRACTIONAL_BITS(8)
	)
	UUT1
	(
		.i_value(t_value_1),
		.i_multiplier(t_multiplier_1),  // Q2.8
		.o_fan_percentage(t_fan_percentage_1)  // Range 0 - 100
	);
	
	fan_multiplier
	#(
		.INPUT_INTEGER_BITS(7),
		.INPUT_FRACTIONAL_BITS(8),
		.MULTIPLIER_FRACTIONAL_BITS(30)
	)
	UUT2
	(
		.i_value(t_value_2),  // Q7.8
		.i_multiplier(t_multiplier_2),  // Q2.30
		.o_fan_percentage(t_fan_percentage_2)  // Range 0 - 100
	);
	
	// Instantiate the stimulus table
	// It contains test stimulus and the expected outputs
	fan_multiplier_stimulus fan_multiplier_stimulus
	(
		.i_case(t_case),
		.o_value_1(t_value_1),
		.o_multiplier_1(t_multiplier_1),
		.o_fan_percentage_1(t_fan_percentage_1_expected),
		.o_value_2(t_value_2),
		.o_multiplier_2(t_multiplier_2),
		.o_fan_percentage_2(t_fan_percentage_2_expected),
		.o_total_cases(t_total_cases)
	);
	
	initial
	begin
		
		$dumpfile("out.vcd");
		$dumpvars(1, fan_multiplier_tb);
		$timeformat(-9, 0, " ns", 20);
		
		$display("Starting Tests...");
		
		#1
		
		for (t_case = 0; t_case < t_total_cases; t_case = t_case + 1)
		begin
			#1;
			assert_unit(t_fan_percentage_1 == t_fan_percentage_1_expected);
			assert_unit(t_fan_percentage_2 == t_fan_percentage_2_expected);
			
			if (t_fan_percentage_1 != t_fan_percentage_1_expected)
			begin
				$display("UUT1 Test case failed: %d", t_case);
			end
			
			if (t_fan_percentage_2 != t_fan_percentage_2_expected)
			begin
				$display("UUT2 Test case failed: %d", t_case);
			end
		end
		
		#10;
		assert_unit_stop();
	end
	
endmodule

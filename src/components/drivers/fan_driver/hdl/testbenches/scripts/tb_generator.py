while True:
    module_name = input("Module Name: ")

    file_template = \
f"""// Testbench for testing {module_name}


module {module_name}_tb;
\t
\t
\t
\t{module_name} UUT
\t(
\t\t
\t);
\t
\tinitial
\tbegin
\t\t$dumpfile("out.vcd");
\t\t$dumpvars(1, {module_name}_tb);
\t\t
\t\t#10;
\t\tassert_unit_stop();
\tend
\t
endmodule
"""

    with open(f"../{module_name}_tb.v", "w") as f:
        f.write(file_template)
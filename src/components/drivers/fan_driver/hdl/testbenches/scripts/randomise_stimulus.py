import random
import traceback
from enum import Enum
import random
from math import radians, sin, cos
from typing import List, Tuple, Union

from fxpmath import Fxp

"""
Template for the file that this will generate...

module test_stimulus
(
    input [31:0] i_test_case,
    
    {ports}
    
    output reg [31:0] o_max_value = max
);

    always @(i_test_case)
    begin
        case (i_test_case)
            0:
                begin
                    blah blah blah
                end
        endcase
    end
endmodule
"""

total_cases = 500


class Direction(Enum):
    IN = ("o", "output reg", "reg", "IN")
    OUT = ("o", "output reg", "wire", "OUT")
    IN_CASE = ("i", "input integer", "reg", "IN_CASE")
    OUT_CASE = ("o", "output integer", "reg", "OUT_CASE")


class Port:
    def __init__(self, name, direction, signed=False, n_bits=None, n_int=None, n_frac=None, upper=None, lower=None, initial=None):
        self.name = name
        self.direction = direction
        self.signed = signed
        
        self.n_bits = n_bits
        self.n_int = n_int
        self.n_frac = n_frac
        
        self.upper = upper
        self.lower = lower
        
        self.initial = initial
    
    def name_module(self) -> str:
        return f"{self.direction.value[0]}_{self.name}"
    
    def name_testbench(self) -> str:
        return f"t_{self.name}"

    def bit_length(self) -> int:
        if self.n_bits is None:
            if self.n_int and self.n_frac:
                return self.n_int + self.n_frac
            elif self.n_int:
                return self.n_int
            elif self.n_frac:
                return self.n_frac
            else:
                return 0
        else:
            return self.n_bits

    def vector_string(self) -> str:
        bits = self.bit_length()

        if bits > 0:
            return f"[{bits - 1}:0]"
        return ""

    def size_comment(self) -> str:
        if (self.direction == Direction.IN_CASE) or (self.direction == Direction.OUT_CASE):
            return ""

        number_stat = self.fxp(0)
        if number_stat.n_frac > 0:
            return f"  // Q{number_stat.n_int}.{number_stat.n_frac}"
        return ""
    
    def port_instantiation(self) -> str:
        return str(self.direction.value[1]) + (" signed " if self.signed else " ") + ((self.vector_string() + " ") if self.bit_length() > 1 else "") + self.name_module() + (f" = {self.initial}" if self.initial is not None else "") + "," + self.size_comment()
    
    def testbench_instantiation(self) -> str:
        return str(self.direction.value[2]) + (" signed" if self.signed else " ") + ((self.vector_string() + " ") if self.bit_length() > 1 else "") + self.name_module() + (" = 0" if self.direction == Direction.IN else "") + ";" + self.size_comment()
    
    def fxp(self, value) -> Fxp:
        return Fxp(value, signed=self.signed, n_word=self.n_bits, n_int=self.n_int, n_frac=self.n_frac)
    
    def generate_random(self) -> Tuple["Port", Fxp]:
        number_stats = self.fxp(0)
        
        random_func = random.uniform if number_stats.n_frac != 0 else random.randint
        
        if self.lower and self.upper:
            number = self.fxp(random_func(self.lower, self.upper))
        elif self.lower:
            number = self.fxp(random_func(self.lower, number_stats.upper))
        elif self.upper:
            number = self.fxp(random_func(number_stats.lower, self.upper))
        else:
            number = self.fxp(random_func(number_stats.lower, number_stats.upper))
            
        return self, number
        
    def neuter_randomness(self) -> "Port":
        self.generate_random = lambda: self.initial
        return self
        
    
class FanPort(Port):
    def __init__(self, angle_degrees, *args, **kwargs):
        self.angle_degrees = angle_degrees
        super().__init__(*args, **kwargs)


class Module:
    name: str
    ports: List[Port]
    case_port: Port = Port("case", Direction.IN_CASE)
    total_cases_port: Port = Port("total_cases", Direction.OUT_CASE, initial=total_cases)
    
    def __init__(self):
        pass
        
    def generate_case(self) -> List[Tuple[Port, Fxp]]:
        raise NotImplementedError()
        
    def file_name(self):
        return f"{self.name}_stimulus.v"
        
    def generate_file(self):
        cases = []
        for i in range(total_cases):
            cases.append(self.generate_case())
            print("Generated Case", i)
        
        with open(f"../{self.file_name()}", "w") as f:
            header = f"// Auto-generated test stimulus. This hasn't been formatted for human readability.\n\nmodule {self.name}_stimulus\n(\n"
            header += "\t" + "\n\t".join([self.case_port.port_instantiation()] + [port.port_instantiation() for port in self.ports] + [self.total_cases_port.port_instantiation()])
            header = header[:-1]
            header += "\n);\n\n"
            
            f.write(header)

            midsection = f"\talways @(*)\n\tbegin\n\t\tcase ({self.case_port.name_module()})\n"

            for i, case in enumerate(cases):
                midsection += f"\t\t\t{i}:\n\t\t\tbegin\n"
                for port in case:
                    midsection += f"\t\t\t\t{port[0].name_module()} = {port[0].bit_length()}'b{port[1].bin()};  // {port[1]}\n"
                midsection += "\t\t\tend\n"
            midsection += "\t\tendcase\n\tend\nendmodule"

            f.write(midsection)


class FanMultiplier(Module):
    name: str = "fan_multiplier"
    ports: List[Port] = [
        Port("value_1", Direction.IN, n_int=7, n_frac=0),
        Port("multiplier_1", Direction.IN, n_int=2, n_frac=8, upper=4.0),
        Port("fan_percentage_1", Direction.OUT, n_int=7, n_frac=0),
        Port("value_2", Direction.IN, n_int=7, n_frac=8),
        Port("multiplier_2", Direction.IN, n_int=2, n_frac=30, upper=4.0),
        Port("fan_percentage_2", Direction.OUT, n_int=7, n_frac=0),
    ]
    
    def generate_case(self) -> List[Tuple[Port, Fxp]]:
        ret_list = []
        
        for i, port in enumerate(self.ports):
            if (i % 3) == 2:
                ret_list.append(self.calculate_result(port, ret_list[i-2], ret_list[i-1]))
                continue
            ret_list.append(port.generate_random())

        return ret_list
        
    def calculate_result(self, port, value1: Tuple[Port, Fxp], value2: Tuple[Port, Fxp]) -> Tuple[Port, Fxp]:
        result = value1[1] * value2[1]

        if float(result.astype(float)) > 100:
            result(100)
        elif float(result.astype(float)) < 0:
            result(0)
        else:
            # We need to seperate out the integer part from the fractional part
            # Then we need to check if the fractional part is greater or equal to 1/2
            # If it is then we increment the integer part and turn it back into a fixed point
            result_int_str = str(int(result.real))
            result_fract_string = str(result)[len(result_int_str):]

            if len(result_fract_string) > 1:
                if int(result_fract_string[1]) >= 5:
                    result_int_str = str(int(result_int_str) + 1)
                    print(f"result ({result}) rounded to {result_int_str}")

            result(int(result_int_str))

        result.resize(signed=False, n_int=8, n_frac=0)
        
        # clamp the values
        if int(result) >= 100:
            return port, port.fxp(100)
        else:
            return port, result.like(port.fxp(0))


class FanDriver_NoSlipstream(Module):
    name: str = "fan_driver"
    ports: List[Union[Port, FanPort]] = [  # Sweat level could have done with having a step or list of values to select from
        Port("sweat_level", Direction.IN, n_int=7, n_frac=0, upper=100),
        Port("wind_magnitude", Direction.IN, n_int=7, n_frac=8),
        Port("wind_direction", Direction.IN, n_int=9, n_frac=0, upper=359),
        Port("slipstream_angle", Direction.IN, n_int=9, n_frac=0, initial=360).neuter_randomness(),
        FanPort(30, "fan_1", Direction.OUT, n_int=7, n_frac=0),
        FanPort(-30, "fan_2", Direction.OUT, n_int=7, n_frac=0),
        FanPort(-90, "fan_3", Direction.OUT, n_int=7, n_frac=0),
        FanPort(-150, "fan_4", Direction.OUT, n_int=7, n_frac=0),
        FanPort(150, "fan_5", Direction.OUT, n_int=7, n_frac=0),
        FanPort(90, "fan_6", Direction.OUT, n_int=7, n_frac=0)
    ]

    needs_cooling: bool = False
    
    def generate_case(self) -> List[Tuple[Port, Fxp]]:
        ret_list = []

        ret_list.append(self.ports[0].generate_random())
        ret_list.append(self.ports[1].generate_random())
        ret_list.append(self.ports[2].generate_random())
        ret_list.append(self.generate_result(self.ports[4], ret_list[0], ret_list[1], ret_list[2]))
        ret_list.append(self.generate_result(self.ports[5], ret_list[0], ret_list[1], ret_list[2]))
        ret_list.append(self.generate_result(self.ports[6], ret_list[0], ret_list[1], ret_list[2]))
        ret_list.append(self.generate_result(self.ports[7], ret_list[0], ret_list[1], ret_list[2]))
        ret_list.append(self.generate_result(self.ports[8], ret_list[0], ret_list[1], ret_list[2]))
        ret_list.append(self.generate_result(self.ports[9], ret_list[0], ret_list[1], ret_list[2]))

        return ret_list
        
    def generate_result(self, fan: FanPort, sweat: Tuple[Port, Fxp], wind_magnitude: Tuple[Port, Fxp], wind_direction: Tuple[Port, Fxp]) -> Tuple[FanPort, Fxp]:
        magnitude = wind_magnitude[1]
        direction_degs = int(wind_direction[1].astype(int))
        direction_rads = radians(direction_degs)
        angle_degs = fan.angle_degrees
        angle_rads = radians(angle_degs)

        result = Fxp(0).like(fan.fxp(0))

        if sweat[1] >= 50:
            self.needs_cooling = True

        # If it was sweating, then we need to cool back down below the hysteresis to reset
        if self.needs_cooling:
            result = Fxp(50).like(fan.fxp(0))
            magnitude /= 2.0

            if sweat[1] < 35:
                self.needs_cooling = False
                result = Fxp(0).like(fan.fxp(0))
                magnitude = wind_magnitude[1]

        diff_angle_degs = max(direction_degs, angle_degs) - min(direction_degs, angle_degs)
        diff_angle_rads = max(direction_rads, angle_rads) - min(direction_rads, angle_rads)

        if 90 <= diff_angle_degs <= 270:
            return fan, result

        multiplier = cos(diff_angle_rads)
        result += magnitude * multiplier

        return fan, result.like(fan.fxp(0))


FanMultiplier().generate_file()
FanDriver_NoSlipstream().generate_file()

// Testbench to test the fan driver
// NOTE: As I am using auto-generated cases, and the algorithm generating them is sort of a mix of floating and fixed, the hardware model
//			wont be as accurate. Instead of checking for exact values, we will let it be within a couple percent either side.

`timescale 1ns/1ns

module fan_driver_tb;

	`include "../../../misc/tb_functions/assert_display.v"
	
	function automatic [6:0] max(input [6:0] value1, input [6:0] value2);
		begin
			if (value1 >= value2)
			begin
				max = value1;
			end
			else
			begin
				max = value2;
			end
		end
	endfunction
	
	function automatic [6:0] min(input [6:0] value1, input [6:0] value2);
		begin
			if (value1 <= value2)
			begin
				min = value1;
			end
			else
			begin
				min = value2;
			end
		end
	endfunction
	
	function automatic [6:0] five_percent(input [6:0] value);
		reg [62:0] shifted; // Q7.56
		reg [119:0] multiplied; // Q1.56 * Q7.56 = Q8.112
		reg [6:0] back_shifted;
		begin
			shifted = value << 56;
			multiplied = shifted * 57'b000001100110011001100110011001100110011001100110011001101;  // * 0.05
			back_shifted = multiplied >> 112;
			
			if (multiplied > (back_shifted << 112))
			begin
				five_percent = back_shifted + 7'd1;
			end
			else
			begin
				five_percent = back_shifted;
			end
		end
	endfunction
	
	function automatic within_spec(input [6:0] value1, input [6:0] value2);
		begin
			within_spec = (max(value1, value2) - min(value1, value2)) <= five_percent(max(value1, value2));
		end
	endfunction

	// TB variables
	integer t_case = 0;
	wire integer t_total_cases;

	// Inputs
	reg t_clk = 0;
	wire [6:0] t_sweat_level;
	wire [14:0] t_wind_magnitude;
	wire [8:0] t_wind_direction;  // NOTE: This should use mod 360 to make the input not mind about the value
	wire [8:0] t_slipstream_angle;

	// Outputs
	wire t_fan_1;
	wire t_fan_2;
	wire t_fan_3;
	wire t_fan_4;
	wire t_fan_5;
	wire t_fan_6;
	
	// Debug Outputs
	wire [6:0] t_d_fan_1;
	wire [6:0] t_d_fan_2;
	wire [6:0] t_d_fan_3;
	wire [6:0] t_d_fan_4;
	wire [6:0] t_d_fan_5;
	wire [6:0] t_d_fan_6;
	
	// Expected Outputs
	// From the stimulus table
	wire [6:0] t_fan_1_expected;
	wire [6:0] t_fan_2_expected;
	wire [6:0] t_fan_3_expected;
	wire [6:0] t_fan_4_expected;
	wire [6:0] t_fan_5_expected;
	wire [6:0] t_fan_6_expected;

	// Instantiate the Unit Under Test (UUT)
	fan_driver uut (
		.i_clk(t_clk),
		.i_sweat_level(t_sweat_level),
		.i_wind_magnitude(t_wind_magnitude),
		.i_wind_direction(t_wind_direction),
		.i_slipstream_angle(t_slipstream_angle),
		.o_fan_1(t_fan_1),
		.o_fan_2(t_fan_2),
		.o_fan_3(t_fan_3),
		.o_fan_4(t_fan_4),
		.o_fan_5(t_fan_5),
		.o_fan_6(t_fan_6),
		.d_fan_1(t_d_fan_1),
		.d_fan_2(t_d_fan_2),
		.d_fan_3(t_d_fan_3),
		.d_fan_4(t_d_fan_4),
		.d_fan_5(t_d_fan_5),
		.d_fan_6(t_d_fan_6)
	);
	
	// Instantiate the stimulus table
	// It contains test stimulus and the expected outputs
	fan_driver_stimulus fan_driver_stimulus
	(
		.i_case(t_case),
		.o_sweat_level(t_sweat_level),
		.o_wind_magnitude(t_wind_magnitude),  // Q7.8
		.o_wind_direction(t_wind_direction),
		.o_slipstream_angle(t_slipstream_angle),
		.o_fan_1(t_fan_1_expected),
		.o_fan_2(t_fan_2_expected),
		.o_fan_3(t_fan_3_expected),
		.o_fan_4(t_fan_4_expected),
		.o_fan_5(t_fan_5_expected),
		.o_fan_6(t_fan_6_expected),
		.o_total_cases(t_total_cases)
	);
	
	always #1 t_clk = ~t_clk;
	
	initial
	begin
		
		$dumpfile("out.vcd");
		$dumpvars(1, fan_driver_tb);
		$timeformat(-9, 0, " ns", 20);
		
		$display("Starting Tests...");
		
		@(posedge t_clk);
		#2
		
		for (t_case = 0; t_case < t_total_cases; t_case = t_case + 1)
		begin
			#4;
			$display("Test case %d, Time %d", t_case, $time);
			assert_unit(within_spec(t_d_fan_1, t_fan_1_expected));
			assert_unit(within_spec(t_d_fan_2, t_fan_2_expected));
			assert_unit(within_spec(t_d_fan_3, t_fan_3_expected));
			assert_unit(within_spec(t_d_fan_4, t_fan_4_expected));
			assert_unit(within_spec(t_d_fan_5, t_fan_5_expected));
			assert_unit(within_spec(t_d_fan_6, t_fan_6_expected));
			
			if (!within_spec(t_d_fan_1, t_fan_1_expected))
			begin
				$write("Fan 1 test case failed: %d, ", t_case);
				$display("Expected: %d, Actual: %d", t_fan_1_expected, t_d_fan_1);
			end
			
			if (!within_spec(t_d_fan_2, t_fan_2_expected))
			begin
				$write("Fan 2 test case failed: %d, ", t_case);
				$display("Expected: %d, Actual: %d", t_fan_2_expected, t_d_fan_2);
			end
			
			if (!within_spec(t_d_fan_3, t_fan_3_expected))
			begin
				$write("Fan 3 test case failed: %d, ", t_case);
				$display("Expected: %d, Actual: %d", t_fan_3_expected, t_d_fan_3);
			end
			
			if (!within_spec(t_d_fan_4, t_fan_4_expected))
			begin
				$write("Fan 4 test case failed: %d, ", t_case);
				$display("Expected: %d, Actual: %d", t_fan_4_expected, t_d_fan_4);
			end
			
			if (!within_spec(t_d_fan_5, t_fan_5_expected))
			begin
				$write("Fan 5 test case failed: %d, ", t_case);
				$display("Expected: %d, Actual: %d", t_fan_5_expected, t_d_fan_5);
			end
			
			if (!within_spec(t_d_fan_6, t_fan_6_expected))
			begin
				$write("Fan 6 test case failed: %d, ", t_case);
				$display("Expected: %d, Actual: %d", t_fan_6_expected, t_d_fan_6);
			end
		end
		
		assert_unit_stop();
	end
endmodule
// Testbench for testing clamper
`timescale 1ns/1ns

module clamper_tb;

	`include "../../../misc/tb_functions/assert_display.v"
	
	// Input
	reg [24:0] t_unclamped = 25'd0;  // Q9.16
	
	// Output
	wire [6:0] t_clamped;
	
	// Instantiate the Unit Under Test (UUT)
	clamper UUT
	(
		.i_unclamped(t_unclamped),
		.o_clamped(t_clamped)
	);
	
	initial
	begin: tb_block
		integer i;
		reg [6:0] expected;
		
		$dumpfile("out.vcd");
		$dumpvars(1, clamper_tb);
		$timeformat(-9, 0, " ns", 20);
		
		$display("Starting Tests...");
		
		// Test 1
		#1;
		$display("Test 1: Checking upper and lower bounds are correct");
		$write("time: %t, ", $time);
		assert_unit(t_clamped == 7'd0);
		t_unclamped = 25'h0640000;
		expected = 7'd100;
		#1;
		$write("time: %t, ", $time);
		assert_unit(t_clamped == 7'd100);
		
		// Test 2
		$display("Test 2: Checking rounding around 0");
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'd1 << i;
			expected = 7'd0;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd0);
		end
		
		$display("Test 2.1");
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = (25'd1 << 15) + (25'd1 << i);
			expected = 7'd1;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd1);
		end
		
		// Test 3
		$display("Test 3: Checking rounding just below 100");
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'h0630000 + (25'd1 << i);
			expected = 7'd99;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd99);
		end
		
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'h0630000 + (25'd1 << 15) + (25'd1 << i);
			expected = 7'd100;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd100);
		end
		
		// Test 4
		$display("Test 4: Checking the rounding just above 100");
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'h0640000 + (25'd1 << i);
			expected = 7'd100;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd100);
		end
		
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'h0640000 + (25'd1 << 15) + (25'd1 << i);
			expected = 7'd100;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd100);
		end
		
		// Test 5
		$display("Test 5: Check clamping and rounding at the max value");
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'h1FF0000 + (25'd1 << i);
			expected = 7'd100;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd100);
		end
		
		for (i = 0; i < 15; i = i + 1)
		begin
			t_unclamped = 25'h1FF0000 + (25'd1 << 15) + (25'd1 << i);
			expected = 7'd100;
			#1;
			$write("time: %t, ", $time);
			assert_unit(t_clamped == 7'd100);
		end
		
		$display("Test 6: Check clamping at the max value");
		t_unclamped = 25'hFFFFFFF;
		expected = 7'd100;
		#1;
		$write("time: %t, ", $time);
		assert_unit(t_clamped == 7'd100);
		
		#10;
		
		assert_unit_stop();
	end
	
endmodule

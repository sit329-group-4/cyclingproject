// Auto-generated test stimulus. This hasn't been formatted for human readability.

module fan_driver_stimulus
(
	input integer i_case,
	output reg [6:0] o_sweat_level,
	output reg [14:0] o_wind_magnitude,  // Q7.8
	output reg [8:0] o_wind_direction,
	output reg [8:0] o_slipstream_angle = 360,
	output reg [6:0] o_fan_1,
	output reg [6:0] o_fan_2,
	output reg [6:0] o_fan_3,
	output reg [6:0] o_fan_4,
	output reg [6:0] o_fan_5,
	output reg [6:0] o_fan_6,
	output integer o_total_cases = 500
);

	always @(*)
	begin
		case (i_case)
			0:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b010100111001111;  // 41.80859375
				o_wind_direction = 9'b011000100;  // 196
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110111;  // 55
				o_fan_4 = 7'b1000110;  // 70
				o_fan_5 = 7'b1000000;  // 64
				o_fan_6 = 7'b0110010;  // 50
			end
			1:
			begin
				o_sweat_level = 7'b1000100;  // 68
				o_wind_magnitude = 15'b100001011001111;  // 66.80859375
				o_wind_direction = 9'b001110010;  // 114
				o_fan_1 = 7'b0110101;  // 53
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1001101;  // 77
				o_fan_6 = 7'b1010000;  // 80
			end
			2:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b101110001111101;  // 92.48828125
				o_wind_direction = 9'b001100100;  // 100
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1001111;  // 79
				o_fan_6 = 7'b1011111;  // 95
			end
			3:
			begin
				o_sweat_level = 7'b1001100;  // 76
				o_wind_magnitude = 15'b011100001010100;  // 56.328125
				o_wind_direction = 9'b000011001;  // 25
				o_fan_1 = 7'b1001110;  // 78
				o_fan_2 = 7'b1000010;  // 66
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111101;  // 61
			end
			4:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b010101111100101;  // 43.89453125
				o_wind_direction = 9'b000111000;  // 56
				o_fan_1 = 7'b1000101;  // 69
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000100;  // 68
			end
			5:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b001110000100101;  // 28.14453125
				o_wind_direction = 9'b010110111;  // 183
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111110;  // 62
				o_fan_5 = 7'b0111101;  // 61
				o_fan_6 = 7'b0110010;  // 50
			end
			6:
			begin
				o_sweat_level = 7'b0011000;  // 24
				o_wind_magnitude = 15'b101110111111101;  // 93.98828125
				o_wind_direction = 9'b101010011;  // 339
				o_fan_1 = 7'b0111011;  // 59
				o_fan_2 = 7'b1011100;  // 92
				o_fan_3 = 7'b0100001;  // 33
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			7:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b010010111011011;  // 37.85546875
				o_wind_direction = 9'b010010100;  // 148
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b1000100;  // 68
				o_fan_6 = 7'b0111011;  // 59
			end
			8:
			begin
				o_sweat_level = 7'b1010110;  // 86
				o_wind_magnitude = 15'b101100010011001;  // 88.59765625
				o_wind_direction = 9'b101000011;  // 323
				o_fan_1 = 7'b1000011;  // 67
				o_fan_2 = 7'b1011101;  // 93
				o_fan_3 = 7'b1001100;  // 76
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			9:
			begin
				o_sweat_level = 7'b0000001;  // 1
				o_wind_magnitude = 15'b111111110000000;  // 127.5
				o_wind_direction = 9'b000011011;  // 27
				o_fan_1 = 7'b1111111;  // 127
				o_fan_2 = 7'b1000101;  // 69
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0111001;  // 57
			end
			10:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b101100110011101;  // 89.61328125
				o_wind_direction = 9'b100101010;  // 298
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1001011;  // 75
				o_fan_3 = 7'b1001111;  // 79
				o_fan_4 = 7'b0000010;  // 2
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			11:
			begin
				o_sweat_level = 7'b1001100;  // 76
				o_wind_magnitude = 15'b111100010100101;  // 120.64453125
				o_wind_direction = 9'b001000101;  // 69
				o_fan_1 = 7'b1100000;  // 96
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111011;  // 59
				o_fan_6 = 7'b1101010;  // 106
			end
			12:
			begin
				o_sweat_level = 7'b0101000;  // 40
				o_wind_magnitude = 15'b000100001001101;  // 8.30078125
				o_wind_direction = 9'b011100011;  // 227
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			13:
			begin
				o_sweat_level = 7'b0110010;  // 50
				o_wind_magnitude = 15'b111100110001111;  // 121.55859375
				o_wind_direction = 9'b101001010;  // 330
				o_fan_1 = 7'b1010000;  // 80
				o_fan_2 = 7'b1101110;  // 110
				o_fan_3 = 7'b1010000;  // 80
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			14:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b011011111100111;  // 55.90234375
				o_wind_direction = 9'b100011100;  // 284
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000101;  // 69
				o_fan_3 = 7'b1001101;  // 77
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			15:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b000101101010011;  // 11.32421875
				o_wind_direction = 9'b010111010;  // 186
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b0110110;  // 54
				o_fan_6 = 7'b0110010;  // 50
			end
			16:
			begin
				o_sweat_level = 7'b1010111;  // 87
				o_wind_magnitude = 15'b010110111011001;  // 45.84765625
				o_wind_direction = 9'b101000010;  // 322
				o_fan_1 = 7'b0111010;  // 58
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b1000000;  // 64
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			17:
			begin
				o_sweat_level = 7'b1011000;  // 88
				o_wind_magnitude = 15'b011000110001110;  // 49.5546875
				o_wind_direction = 9'b001111111;  // 127
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b1001000;  // 72
				o_fan_6 = 7'b1000101;  // 69
			end
			18:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b100101001100010;  // 74.3828125
				o_wind_direction = 9'b001010001;  // 81
				o_fan_1 = 7'b0101110;  // 46
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0011010;  // 26
				o_fan_6 = 7'b1001001;  // 73
			end
			19:
			begin
				o_sweat_level = 7'b1010001;  // 81
				o_wind_magnitude = 15'b111011110101000;  // 119.65625
				o_wind_direction = 9'b001111011;  // 123
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b1100111;  // 103
				o_fan_6 = 7'b1100100;  // 100
			end
			20:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b111001100100111;  // 115.15234375
				o_wind_direction = 9'b010110111;  // 183
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000101;  // 5
				o_fan_4 = 7'b1100110;  // 102
				o_fan_5 = 7'b1100000;  // 96
				o_fan_6 = 7'b0000000;  // 0
			end
			21:
			begin
				o_sweat_level = 7'b0001100;  // 12
				o_wind_magnitude = 15'b010001010111111;  // 34.74609375
				o_wind_direction = 9'b000010101;  // 21
				o_fan_1 = 7'b0100010;  // 34
				o_fan_2 = 7'b0010101;  // 21
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0001100;  // 12
			end
			22:
			begin
				o_sweat_level = 7'b0000100;  // 4
				o_wind_magnitude = 15'b010001001111110;  // 34.4921875
				o_wind_direction = 9'b000100011;  // 35
				o_fan_1 = 7'b0100010;  // 34
				o_fan_2 = 7'b0001110;  // 14
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010011;  // 19
			end
			23:
			begin
				o_sweat_level = 7'b0011000;  // 24
				o_wind_magnitude = 15'b010011110100100;  // 39.640625
				o_wind_direction = 9'b010101101;  // 173
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0011111;  // 31
				o_fan_5 = 7'b0100100;  // 36
				o_fan_6 = 7'b0000100;  // 4
			end
			24:
			begin
				o_sweat_level = 7'b0100001;  // 33
				o_wind_magnitude = 15'b010001000010111;  // 34.08984375
				o_wind_direction = 9'b010011000;  // 152
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010001;  // 17
				o_fan_5 = 7'b0100001;  // 33
				o_fan_6 = 7'b0001111;  // 15
			end
			25:
			begin
				o_sweat_level = 7'b0010010;  // 18
				o_wind_magnitude = 15'b011110110111000;  // 61.71875
				o_wind_direction = 9'b010000111;  // 135
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0001111;  // 15
				o_fan_5 = 7'b0111011;  // 59
				o_fan_6 = 7'b0101011;  // 43
			end
			26:
			begin
				o_sweat_level = 7'b0110001;  // 49
				o_wind_magnitude = 15'b000101000100001;  // 10.12890625
				o_wind_direction = 9'b000110100;  // 52
				o_fan_1 = 7'b0001001;  // 9
				o_fan_2 = 7'b0000001;  // 1
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000111;  // 7
			end
			27:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b100101101111110;  // 75.4921875
				o_wind_direction = 9'b001101101;  // 109
				o_fan_1 = 7'b0001110;  // 14
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b1000111;  // 71
			end
			28:
			begin
				o_sweat_level = 7'b0010011;  // 19
				o_wind_magnitude = 15'b011000010111110;  // 48.7421875
				o_wind_direction = 9'b010010001;  // 145
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010100;  // 20
				o_fan_5 = 7'b0110000;  // 48
				o_fan_6 = 7'b0011011;  // 27
			end
			29:
			begin
				o_sweat_level = 7'b0101000;  // 40
				o_wind_magnitude = 15'b001001101001011;  // 19.29296875
				o_wind_direction = 9'b100111010;  // 314
				o_fan_1 = 7'b0000100;  // 4
				o_fan_2 = 7'b0010010;  // 18
				o_fan_3 = 7'b0001101;  // 13
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			30:
			begin
				o_sweat_level = 7'b0110100;  // 52
				o_wind_magnitude = 15'b111001011010100;  // 114.828125
				o_wind_direction = 9'b011001011;  // 203
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1001000;  // 72
				o_fan_4 = 7'b1101010;  // 106
				o_fan_5 = 7'b1010100;  // 84
				o_fan_6 = 7'b0110010;  // 50
			end
			31:
			begin
				o_sweat_level = 7'b0111111;  // 63
				o_wind_magnitude = 15'b000110010001111;  // 12.55859375
				o_wind_direction = 9'b010011100;  // 156
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0110100;  // 52
			end
			32:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b101011101101100;  // 87.421875
				o_wind_direction = 9'b001100010;  // 98
				o_fan_1 = 7'b1000010;  // 66
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1001100;  // 76
				o_fan_6 = 7'b1011101;  // 93
			end
			33:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b100011010111010;  // 70.7265625
				o_wind_direction = 9'b000100010;  // 34
				o_fan_1 = 7'b1010101;  // 85
				o_fan_2 = 7'b1000001;  // 65
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000101;  // 69
			end
			34:
			begin
				o_sweat_level = 7'b1001111;  // 79
				o_wind_magnitude = 15'b100101001110000;  // 74.4375
				o_wind_direction = 9'b001100100;  // 100
				o_fan_1 = 7'b0111110;  // 62
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1001001;  // 73
				o_fan_6 = 7'b1010110;  // 86
			end
			35:
			begin
				o_sweat_level = 7'b1000001;  // 65
				o_wind_magnitude = 15'b011101011101100;  // 58.921875
				o_wind_direction = 9'b100001010;  // 266
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111110;  // 62
				o_fan_3 = 7'b1001111;  // 79
				o_fan_4 = 7'b1000010;  // 66
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			36:
			begin
				o_sweat_level = 7'b0000011;  // 3
				o_wind_magnitude = 15'b100111011111001;  // 78.97265625
				o_wind_direction = 9'b100100101;  // 293
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0111110;  // 62
				o_fan_3 = 7'b1001000;  // 72
				o_fan_4 = 7'b0001001;  // 9
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			37:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b111010110000100;  // 117.515625
				o_wind_direction = 9'b010010101;  // 149
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0111000;  // 56
				o_fan_5 = 7'b1110101;  // 117
				o_fan_6 = 7'b0111100;  // 60
			end
			38:
			begin
				o_sweat_level = 7'b0000001;  // 1
				o_wind_magnitude = 15'b011110000001011;  // 60.04296875
				o_wind_direction = 9'b001010111;  // 87
				o_fan_1 = 7'b0100000;  // 32
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0011011;  // 27
				o_fan_6 = 7'b0111011;  // 59
			end
			39:
			begin
				o_sweat_level = 7'b0101101;  // 45
				o_wind_magnitude = 15'b001111110100111;  // 31.65234375
				o_wind_direction = 9'b000010011;  // 19
				o_fan_1 = 7'b0011111;  // 31
				o_fan_2 = 7'b0010100;  // 20
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0001010;  // 10
			end
			40:
			begin
				o_sweat_level = 7'b0110011;  // 51
				o_wind_magnitude = 15'b000110101001001;  // 13.28515625
				o_wind_direction = 9'b010011001;  // 153
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0110101;  // 53
			end
			41:
			begin
				o_sweat_level = 7'b1011010;  // 90
				o_wind_magnitude = 15'b110101100011110;  // 107.1171875
				o_wind_direction = 9'b011011101;  // 221
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1010100;  // 84
				o_fan_4 = 7'b1100110;  // 102
				o_fan_5 = 7'b1000011;  // 67
				o_fan_6 = 7'b0110010;  // 50
			end
			42:
			begin
				o_sweat_level = 7'b1100000;  // 96
				o_wind_magnitude = 15'b101101010100010;  // 90.6328125
				o_wind_direction = 9'b100001010;  // 266
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000101;  // 69
				o_fan_3 = 7'b1011111;  // 95
				o_fan_4 = 7'b1001011;  // 75
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			43:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b001110000001110;  // 28.0546875
				o_wind_direction = 9'b011000000;  // 192
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000101;  // 5
				o_fan_4 = 7'b0011010;  // 26
				o_fan_5 = 7'b0010100;  // 20
				o_fan_6 = 7'b0000000;  // 0
			end
			44:
			begin
				o_sweat_level = 7'b0001100;  // 12
				o_wind_magnitude = 15'b001111010011000;  // 30.59375
				o_wind_direction = 9'b001000000;  // 64
				o_fan_1 = 7'b0011001;  // 25
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000010;  // 2
				o_fan_6 = 7'b0011011;  // 27
			end
			45:
			begin
				o_sweat_level = 7'b0000100;  // 4
				o_wind_magnitude = 15'b011100001110001;  // 56.44140625
				o_wind_direction = 9'b011100001;  // 225
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0100111;  // 39
				o_fan_4 = 7'b0110110;  // 54
				o_fan_5 = 7'b0001110;  // 14
				o_fan_6 = 7'b0000000;  // 0
			end
			46:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b001010110111010;  // 21.7265625
				o_wind_direction = 9'b100010011;  // 275
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0001100;  // 12
				o_fan_3 = 7'b0010101;  // 21
				o_fan_4 = 7'b0001001;  // 9
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			47:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b000011111001010;  // 7.7890625
				o_wind_direction = 9'b100001110;  // 270
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			48:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b001001010110100;  // 18.703125
				o_wind_direction = 9'b011011101;  // 221
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b0111011;  // 59
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			49:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b011011111001001;  // 55.78515625
				o_wind_direction = 9'b001111110;  // 126
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000101;  // 5
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0101101;  // 45
			end
			50:
			begin
				o_sweat_level = 7'b0110110;  // 54
				o_wind_magnitude = 15'b000001001001110;  // 2.3046875
				o_wind_direction = 9'b001010100;  // 84
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110011;  // 51
			end
			51:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b101101101000011;  // 91.26171875
				o_wind_direction = 9'b100110101;  // 309
				o_fan_1 = 7'b0111001;  // 57
				o_fan_2 = 7'b1011100;  // 92
				o_fan_3 = 7'b1010101;  // 85
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			52:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b010100011100101;  // 40.89453125
				o_wind_direction = 9'b101010101;  // 341
				o_fan_1 = 7'b0111111;  // 63
				o_fan_2 = 7'b1000110;  // 70
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			53:
			begin
				o_sweat_level = 7'b0101100;  // 44
				o_wind_magnitude = 15'b110101111001101;  // 107.80078125
				o_wind_direction = 9'b011010011;  // 211
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1001101;  // 77
				o_fan_4 = 7'b1100111;  // 103
				o_fan_5 = 7'b1001100;  // 76
				o_fan_6 = 7'b0110010;  // 50
			end
			54:
			begin
				o_sweat_level = 7'b1100011;  // 99
				o_wind_magnitude = 15'b111011100111000;  // 119.21875
				o_wind_direction = 9'b010101010;  // 170
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1011111;  // 95
				o_fan_5 = 7'b1101001;  // 105
				o_fan_6 = 7'b0111100;  // 60
			end
			55:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b100101101010011;  // 75.32421875
				o_wind_direction = 9'b100001100;  // 268
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000011;  // 67
				o_fan_3 = 7'b1010111;  // 87
				o_fan_4 = 7'b1000101;  // 69
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			56:
			begin
				o_sweat_level = 7'b0010100;  // 20
				o_wind_magnitude = 15'b111000001000101;  // 112.26953125
				o_wind_direction = 9'b011000000;  // 192
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0010111;  // 23
				o_fan_4 = 7'b1101010;  // 106
				o_fan_5 = 7'b1010011;  // 83
				o_fan_6 = 7'b0000000;  // 0
			end
			57:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b111011111001101;  // 119.80078125
				o_wind_direction = 9'b011101000;  // 232
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b1011110;  // 94
				o_fan_4 = 7'b1101110;  // 110
				o_fan_5 = 7'b0010000;  // 16
				o_fan_6 = 7'b0000000;  // 0
			end
			58:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b110101011001111;  // 106.80859375
				o_wind_direction = 9'b001101101;  // 109
				o_fan_1 = 7'b0111100;  // 60
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1011010;  // 90
				o_fan_6 = 7'b1100100;  // 100
			end
			59:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b010110000011101;  // 44.11328125
				o_wind_direction = 9'b001001010;  // 74
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110111;  // 55
				o_fan_6 = 7'b1000111;  // 71
			end
			60:
			begin
				o_sweat_level = 7'b1000100;  // 68
				o_wind_magnitude = 15'b101111011010101;  // 94.83203125
				o_wind_direction = 9'b010101101;  // 173
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1010111;  // 87
				o_fan_5 = 7'b1011101;  // 93
				o_fan_6 = 7'b0110111;  // 55
			end
			61:
			begin
				o_sweat_level = 7'b0010111;  // 23
				o_wind_magnitude = 15'b001000110000110;  // 17.5234375
				o_wind_direction = 9'b011111000;  // 248
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000010;  // 2
				o_fan_3 = 7'b0010000;  // 16
				o_fan_4 = 7'b0001101;  // 13
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			62:
			begin
				o_sweat_level = 7'b0111111;  // 63
				o_wind_magnitude = 15'b001101101111011;  // 27.48046875
				o_wind_direction = 9'b010101101;  // 173
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111100;  // 60
				o_fan_5 = 7'b0111110;  // 62
				o_fan_6 = 7'b0110011;  // 51
			end
			63:
			begin
				o_sweat_level = 7'b1011010;  // 90
				o_wind_magnitude = 15'b110011000000110;  // 102.0234375
				o_wind_direction = 9'b000100100;  // 36
				o_fan_1 = 7'b1100100;  // 100
				o_fan_2 = 7'b1000110;  // 70
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1001111;  // 79
			end
			64:
			begin
				o_sweat_level = 7'b0010001;  // 17
				o_wind_magnitude = 15'b000100000001001;  // 8.03515625
				o_wind_direction = 9'b101011011;  // 347
				o_fan_1 = 7'b0000101;  // 5
				o_fan_2 = 7'b0000111;  // 7
				o_fan_3 = 7'b0000001;  // 1
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			65:
			begin
				o_sweat_level = 7'b0100100;  // 36
				o_wind_magnitude = 15'b110100000101010;  // 104.1640625
				o_wind_direction = 9'b100011001;  // 281
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1000011;  // 67
				o_fan_3 = 7'b1100110;  // 102
				o_fan_4 = 7'b0100001;  // 33
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			66:
			begin
				o_sweat_level = 7'b1001010;  // 74
				o_wind_magnitude = 15'b111000000011001;  // 112.09765625
				o_wind_direction = 9'b010101100;  // 172
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1011110;  // 94
				o_fan_5 = 7'b1100101;  // 101
				o_fan_6 = 7'b0111001;  // 57
			end
			67:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b100101010110011;  // 74.69921875
				o_wind_direction = 9'b100001010;  // 266
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0100000;  // 32
				o_fan_3 = 7'b1001010;  // 74
				o_fan_4 = 7'b0101001;  // 41
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			68:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b111110100000100;  // 125.015625
				o_wind_direction = 9'b000111101;  // 61
				o_fan_1 = 7'b1100111;  // 103
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1101000;  // 104
			end
			69:
			begin
				o_sweat_level = 7'b0110001;  // 49
				o_wind_magnitude = 15'b110011100001100;  // 103.046875
				o_wind_direction = 9'b011101101;  // 237
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1011101;  // 93
				o_fan_4 = 7'b1011111;  // 95
				o_fan_5 = 7'b0110100;  // 52
				o_fan_6 = 7'b0110010;  // 50
			end
			70:
			begin
				o_sweat_level = 7'b0010001;  // 17
				o_wind_magnitude = 15'b001111110101000;  // 31.65625
				o_wind_direction = 9'b001000100;  // 68
				o_fan_1 = 7'b0011000;  // 24
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000100;  // 4
				o_fan_6 = 7'b0011101;  // 29
			end
			71:
			begin
				o_sweat_level = 7'b1011101;  // 93
				o_wind_magnitude = 15'b111000110001010;  // 113.5390625
				o_wind_direction = 9'b001011110;  // 94
				o_fan_1 = 7'b1001010;  // 74
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1010001;  // 81
				o_fan_6 = 7'b1101010;  // 106
			end
			72:
			begin
				o_sweat_level = 7'b1100000;  // 96
				o_wind_magnitude = 15'b000111000111000;  // 14.21875
				o_wind_direction = 9'b010100011;  // 163
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110110;  // 54
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0110100;  // 52
			end
			73:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b110101100011010;  // 107.1015625
				o_wind_direction = 9'b001001110;  // 78
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0100001;  // 33
				o_fan_6 = 7'b1101000;  // 104
			end
			74:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b110110001000101;  // 108.26953125
				o_wind_direction = 9'b001000010;  // 66
				o_fan_1 = 7'b1011101;  // 93
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110111;  // 55
				o_fan_6 = 7'b1100011;  // 99
			end
			75:
			begin
				o_sweat_level = 7'b0000110;  // 6
				o_wind_magnitude = 15'b010111101000111;  // 47.27734375
				o_wind_direction = 9'b101011111;  // 351
				o_fan_1 = 7'b0100100;  // 36
				o_fan_2 = 7'b0101011;  // 43
				o_fan_3 = 7'b0000111;  // 7
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			76:
			begin
				o_sweat_level = 7'b1010101;  // 85
				o_wind_magnitude = 15'b011001110101100;  // 51.671875
				o_wind_direction = 9'b011110110;  // 246
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b1001001;  // 73
				o_fan_4 = 7'b1000110;  // 70
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			77:
			begin
				o_sweat_level = 7'b0100011;  // 35
				o_wind_magnitude = 15'b001000100010011;  // 17.07421875
				o_wind_direction = 9'b011110100;  // 244
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111001;  // 57
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			78:
			begin
				o_sweat_level = 7'b1001010;  // 74
				o_wind_magnitude = 15'b010010010000101;  // 36.51953125
				o_wind_direction = 9'b010100100;  // 164
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111110;  // 62
				o_fan_5 = 7'b1000011;  // 67
				o_fan_6 = 7'b0110110;  // 54
			end
			79:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b100011001101001;  // 70.41015625
				o_wind_direction = 9'b101001011;  // 331
				o_fan_1 = 7'b0100100;  // 36
				o_fan_2 = 7'b1000110;  // 70
				o_fan_3 = 7'b0100010;  // 34
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			80:
			begin
				o_sweat_level = 7'b0100010;  // 34
				o_wind_magnitude = 15'b001100100010001;  // 25.06640625
				o_wind_direction = 9'b010101010;  // 170
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010011;  // 19
				o_fan_5 = 7'b0010111;  // 23
				o_fan_6 = 7'b0000100;  // 4
			end
			81:
			begin
				o_sweat_level = 7'b0110011;  // 51
				o_wind_magnitude = 15'b000110000111101;  // 12.23828125
				o_wind_direction = 9'b101001000;  // 328
				o_fan_1 = 7'b0110100;  // 52
				o_fan_2 = 7'b0111000;  // 56
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			82:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b011000101111001;  // 49.47265625
				o_wind_direction = 9'b010000011;  // 131
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0001001;  // 9
				o_fan_5 = 7'b0101110;  // 46
				o_fan_6 = 7'b0100101;  // 37
			end
			83:
			begin
				o_sweat_level = 7'b1100000;  // 96
				o_wind_magnitude = 15'b111000000100001;  // 112.12890625
				o_wind_direction = 9'b100101011;  // 299
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1100001;  // 97
				o_fan_3 = 7'b1100010;  // 98
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			84:
			begin
				o_sweat_level = 7'b1011110;  // 94
				o_wind_magnitude = 15'b100101000011011;  // 74.10546875
				o_wind_direction = 9'b100100001;  // 289
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1001101;  // 77
				o_fan_3 = 7'b1010101;  // 85
				o_fan_4 = 7'b0111000;  // 56
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			85:
			begin
				o_sweat_level = 7'b0110110;  // 54
				o_wind_magnitude = 15'b101001011001101;  // 82.80078125
				o_wind_direction = 9'b100100001;  // 289
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010001;  // 81
				o_fan_3 = 7'b1011001;  // 89
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			86:
			begin
				o_sweat_level = 7'b0000110;  // 6
				o_wind_magnitude = 15'b010100100100110;  // 41.1484375
				o_wind_direction = 9'b000011110;  // 30
				o_fan_1 = 7'b0101001;  // 41
				o_fan_2 = 7'b0010100;  // 20
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010100;  // 20
			end
			87:
			begin
				o_sweat_level = 7'b1011000;  // 88
				o_wind_magnitude = 15'b110100010111101;  // 104.73828125
				o_wind_direction = 9'b100110000;  // 304
				o_fan_1 = 7'b0110101;  // 53
				o_fan_2 = 7'b1100001;  // 97
				o_fan_3 = 7'b1011101;  // 93
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			88:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b000111110011001;  // 15.59765625
				o_wind_direction = 9'b001100010;  // 98
				o_fan_1 = 7'b0110100;  // 52
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110110;  // 54
				o_fan_6 = 7'b0111001;  // 57
			end
			89:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b010110110110101;  // 45.70703125
				o_wind_direction = 9'b101001000;  // 328
				o_fan_1 = 7'b0111100;  // 60
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b0111110;  // 62
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			90:
			begin
				o_sweat_level = 7'b1010001;  // 81
				o_wind_magnitude = 15'b010001011000111;  // 34.77734375
				o_wind_direction = 9'b101000001;  // 321
				o_fan_1 = 7'b0111000;  // 56
				o_fan_2 = 7'b1000011;  // 67
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			91:
			begin
				o_sweat_level = 7'b0000011;  // 3
				o_wind_magnitude = 15'b101001110101110;  // 83.6796875
				o_wind_direction = 9'b100100110;  // 294
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1000011;  // 67
				o_fan_3 = 7'b1001100;  // 76
				o_fan_4 = 7'b0001000;  // 8
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			92:
			begin
				o_sweat_level = 7'b1000111;  // 71
				o_wind_magnitude = 15'b011111010001001;  // 62.53515625
				o_wind_direction = 9'b000101011;  // 43
				o_fan_1 = 7'b1010000;  // 80
				o_fan_2 = 7'b0111011;  // 59
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000111;  // 71
			end
			93:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b111000010101011;  // 112.66796875
				o_wind_direction = 9'b011100110;  // 230
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b1010110;  // 86
				o_fan_4 = 7'b1101001;  // 105
				o_fan_5 = 7'b0010011;  // 19
				o_fan_6 = 7'b0000000;  // 0
			end
			94:
			begin
				o_sweat_level = 7'b0000101;  // 5
				o_wind_magnitude = 15'b100111011001000;  // 78.78125
				o_wind_direction = 9'b000011100;  // 28
				o_fan_1 = 7'b1001110;  // 78
				o_fan_2 = 7'b0101001;  // 41
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0100100;  // 36
			end
			95:
			begin
				o_sweat_level = 7'b0110111;  // 55
				o_wind_magnitude = 15'b010110011011010;  // 44.8515625
				o_wind_direction = 9'b000010011;  // 19
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b1000000;  // 64
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111001;  // 57
			end
			96:
			begin
				o_sweat_level = 7'b0110010;  // 50
				o_wind_magnitude = 15'b110101011010000;  // 106.8125
				o_wind_direction = 9'b001001110;  // 78
				o_fan_1 = 7'b1010101;  // 85
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000010;  // 66
				o_fan_6 = 7'b1100110;  // 102
			end
			97:
			begin
				o_sweat_level = 7'b1000100;  // 68
				o_wind_magnitude = 15'b001101110111110;  // 27.7421875
				o_wind_direction = 9'b001100111;  // 103
				o_fan_1 = 7'b0110110;  // 54
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111011;  // 59
				o_fan_6 = 7'b0111111;  // 63
			end
			98:
			begin
				o_sweat_level = 7'b0111010;  // 58
				o_wind_magnitude = 15'b001110110010010;  // 29.5703125
				o_wind_direction = 9'b011001100;  // 204
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b1000000;  // 64
				o_fan_5 = 7'b0111010;  // 58
				o_fan_6 = 7'b0110010;  // 50
			end
			99:
			begin
				o_sweat_level = 7'b1100001;  // 97
				o_wind_magnitude = 15'b111111010000101;  // 126.51953125
				o_wind_direction = 9'b011110100;  // 244
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110110;  // 54
				o_fan_3 = 7'b1101010;  // 106
				o_fan_4 = 7'b1100110;  // 102
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			100:
			begin
				o_sweat_level = 7'b0111110;  // 62
				o_wind_magnitude = 15'b001110110010000;  // 29.5625
				o_wind_direction = 9'b011110101;  // 245
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0111111;  // 63
				o_fan_4 = 7'b0111110;  // 62
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			101:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b000101100001100;  // 11.046875
				o_wind_direction = 9'b100001011;  // 267
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000101;  // 5
				o_fan_3 = 7'b0001011;  // 11
				o_fan_4 = 7'b0000101;  // 5
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			102:
			begin
				o_sweat_level = 7'b1001111;  // 79
				o_wind_magnitude = 15'b101001110010100;  // 83.578125
				o_wind_direction = 9'b011010110;  // 214
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1001001;  // 73
				o_fan_4 = 7'b1011011;  // 91
				o_fan_5 = 7'b1000100;  // 68
				o_fan_6 = 7'b0110010;  // 50
			end
			103:
			begin
				o_sweat_level = 7'b0001111;  // 15
				o_wind_magnitude = 15'b011111101111100;  // 63.484375
				o_wind_direction = 9'b100000110;  // 262
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0010111;  // 23
				o_fan_3 = 7'b0111110;  // 62
				o_fan_4 = 7'b0100110;  // 38
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			104:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b011111100101011;  // 63.16796875
				o_wind_direction = 9'b000010011;  // 19
				o_fan_1 = 7'b1010000;  // 80
				o_fan_2 = 7'b1000110;  // 70
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111100;  // 60
			end
			105:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b111100001011111;  // 120.37109375
				o_wind_direction = 9'b011110000;  // 240
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b1100111;  // 103
				o_fan_4 = 7'b1100111;  // 103
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			106:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b101011010101110;  // 86.6796875
				o_wind_direction = 9'b001101010;  // 106
				o_fan_1 = 7'b0010100;  // 20
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0111110;  // 62
				o_fan_6 = 7'b1010011;  // 83
			end
			107:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b111100111011110;  // 121.8671875
				o_wind_direction = 9'b010010100;  // 148
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1001110;  // 78
				o_fan_5 = 7'b1101110;  // 110
				o_fan_6 = 7'b1010010;  // 82
			end
			108:
			begin
				o_sweat_level = 7'b0100110;  // 38
				o_wind_magnitude = 15'b001100111011111;  // 25.87109375
				o_wind_direction = 9'b011010101;  // 213
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111001;  // 57
				o_fan_4 = 7'b0111110;  // 62
				o_fan_5 = 7'b0110111;  // 55
				o_fan_6 = 7'b0110010;  // 50
			end
			109:
			begin
				o_sweat_level = 7'b1010011;  // 83
				o_wind_magnitude = 15'b000000101000110;  // 1.2734375
				o_wind_direction = 9'b010000001;  // 129
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			110:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b110010101100110;  // 101.3984375
				o_wind_direction = 9'b010011000;  // 152
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1001100;  // 76
				o_fan_5 = 7'b1100100;  // 100
				o_fan_6 = 7'b1001001;  // 73
			end
			111:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b100000011100110;  // 64.8984375
				o_wind_direction = 9'b000101011;  // 43
				o_fan_1 = 7'b1010001;  // 81
				o_fan_2 = 7'b0111011;  // 59
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1001000;  // 72
			end
			112:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b000001111111000;  // 3.96875
				o_wind_direction = 9'b001101001;  // 105
				o_fan_1 = 7'b0000001;  // 1
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000010;  // 2
				o_fan_6 = 7'b0000011;  // 3
			end
			113:
			begin
				o_sweat_level = 7'b0110110;  // 54
				o_wind_magnitude = 15'b101001111100100;  // 83.890625
				o_wind_direction = 9'b000101101;  // 45
				o_fan_1 = 7'b1011010;  // 90
				o_fan_2 = 7'b0111100;  // 60
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1001111;  // 79
			end
			114:
			begin
				o_sweat_level = 7'b0000001;  // 1
				o_wind_magnitude = 15'b110101100010001;  // 107.06640625
				o_wind_direction = 9'b000011001;  // 25
				o_fan_1 = 7'b1101010;  // 106
				o_fan_2 = 7'b0111101;  // 61
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0101101;  // 45
			end
			115:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b111100100100000;  // 121.125
				o_wind_direction = 9'b100000011;  // 259
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000101;  // 69
				o_fan_3 = 7'b1101101;  // 109
				o_fan_4 = 7'b1011001;  // 89
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			116:
			begin
				o_sweat_level = 7'b0011110;  // 30
				o_wind_magnitude = 15'b100111010110111;  // 78.71484375
				o_wind_direction = 9'b010001000;  // 136
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010101;  // 21
				o_fan_5 = 7'b1001100;  // 76
				o_fan_6 = 7'b0110110;  // 54
			end
			117:
			begin
				o_sweat_level = 7'b0110100;  // 52
				o_wind_magnitude = 15'b110111101000000;  // 111.25
				o_wind_direction = 9'b100000001;  // 257
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000010;  // 66
				o_fan_3 = 7'b1101000;  // 104
				o_fan_4 = 7'b1010111;  // 87
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			118:
			begin
				o_sweat_level = 7'b0011111;  // 31
				o_wind_magnitude = 15'b010011110100011;  // 39.63671875
				o_wind_direction = 9'b000000110;  // 6
				o_fan_1 = 7'b0100100;  // 36
				o_fan_2 = 7'b0100000;  // 32
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000100;  // 4
			end
			119:
			begin
				o_sweat_level = 7'b0001010;  // 10
				o_wind_magnitude = 15'b101001000111010;  // 82.2265625
				o_wind_direction = 9'b100110110;  // 310
				o_fan_1 = 7'b0001110;  // 14
				o_fan_2 = 7'b1001101;  // 77
				o_fan_3 = 7'b0111110;  // 62
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			120:
			begin
				o_sweat_level = 7'b1011111;  // 95
				o_wind_magnitude = 15'b010110100111010;  // 45.2265625
				o_wind_direction = 9'b100000100;  // 260
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111001;  // 57
				o_fan_3 = 7'b1001000;  // 72
				o_fan_4 = 7'b1000000;  // 64
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			121:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b111001111010000;  // 115.8125
				o_wind_direction = 9'b010110101;  // 181
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1100100;  // 100
				o_fan_5 = 7'b1100011;  // 99
				o_fan_6 = 7'b0110010;  // 50
			end
			122:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b001100001000000;  // 24.25
				o_wind_direction = 9'b100110111;  // 311
				o_fan_1 = 7'b0110100;  // 52
				o_fan_2 = 7'b0111101;  // 61
				o_fan_3 = 7'b0111011;  // 59
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			123:
			begin
				o_sweat_level = 7'b1000101;  // 69
				o_wind_magnitude = 15'b111111011001101;  // 126.80078125
				o_wind_direction = 9'b011100001;  // 225
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1011110;  // 94
				o_fan_4 = 7'b1101111;  // 111
				o_fan_5 = 7'b1000010;  // 66
				o_fan_6 = 7'b0110010;  // 50
			end
			124:
			begin
				o_sweat_level = 7'b0100110;  // 38
				o_wind_magnitude = 15'b011011000111101;  // 54.23828125
				o_wind_direction = 9'b010111110;  // 190
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b1001011;  // 75
				o_fan_5 = 7'b1000110;  // 70
				o_fan_6 = 7'b0110010;  // 50
			end
			125:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b011111100100001;  // 63.12890625
				o_wind_direction = 9'b100011011;  // 283
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0101010;  // 42
				o_fan_3 = 7'b0111101;  // 61
				o_fan_4 = 7'b0010010;  // 18
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			126:
			begin
				o_sweat_level = 7'b0010110;  // 22
				o_wind_magnitude = 15'b010100001100110;  // 40.3984375
				o_wind_direction = 9'b001101000;  // 104
				o_fan_1 = 7'b0001011;  // 11
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0011011;  // 27
				o_fan_6 = 7'b0100111;  // 39
			end
			127:
			begin
				o_sweat_level = 7'b0100101;  // 37
				o_wind_magnitude = 15'b000110101011010;  // 13.3515625
				o_wind_direction = 9'b000101011;  // 43
				o_fan_1 = 7'b0001100;  // 12
				o_fan_2 = 7'b0000011;  // 3
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0001001;  // 9
			end
			128:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b001010110110001;  // 21.69140625
				o_wind_direction = 9'b101100010;  // 354
				o_fan_1 = 7'b0010001;  // 17
				o_fan_2 = 7'b0010011;  // 19
				o_fan_3 = 7'b0000010;  // 2
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			129:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b010010010011000;  // 36.59375
				o_wind_direction = 9'b100110000;  // 304
				o_fan_1 = 7'b0000010;  // 2
				o_fan_2 = 7'b0100000;  // 32
				o_fan_3 = 7'b0011110;  // 30
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			130:
			begin
				o_sweat_level = 7'b1000001;  // 65
				o_wind_magnitude = 15'b001110111011011;  // 29.85546875
				o_wind_direction = 9'b001011001;  // 89
				o_fan_1 = 7'b0111001;  // 57
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111001;  // 57
				o_fan_6 = 7'b1000000;  // 64
			end
			131:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b001101011100001;  // 26.87890625
				o_wind_direction = 9'b101100111;  // 359
				o_fan_1 = 7'b0010110;  // 22
				o_fan_2 = 7'b0010111;  // 23
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			132:
			begin
				o_sweat_level = 7'b0101100;  // 44
				o_wind_magnitude = 15'b111110111100101;  // 125.89453125
				o_wind_direction = 9'b011100111;  // 231
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b1100001;  // 97
				o_fan_4 = 7'b1110101;  // 117
				o_fan_5 = 7'b0010011;  // 19
				o_fan_6 = 7'b0000000;  // 0
			end
			133:
			begin
				o_sweat_level = 7'b1010011;  // 83
				o_wind_magnitude = 15'b001111000111001;  // 30.22265625
				o_wind_direction = 9'b011010000;  // 208
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111001;  // 57
				o_fan_4 = 7'b1000001;  // 65
				o_fan_5 = 7'b0111001;  // 57
				o_fan_6 = 7'b0110010;  // 50
			end
			134:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b101100010110111;  // 88.71484375
				o_wind_direction = 9'b001101110;  // 110
				o_fan_1 = 7'b0111001;  // 57
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1010011;  // 83
				o_fan_6 = 7'b1011011;  // 91
			end
			135:
			begin
				o_sweat_level = 7'b0100110;  // 38
				o_wind_magnitude = 15'b110000010001000;  // 96.53125
				o_wind_direction = 9'b100010000;  // 272
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1001011;  // 75
				o_fan_3 = 7'b1100010;  // 98
				o_fan_4 = 7'b1001000;  // 72
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			136:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b101100100100111;  // 89.15234375
				o_wind_direction = 9'b001011001;  // 89
				o_fan_1 = 7'b1001000;  // 72
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000111;  // 71
				o_fan_6 = 7'b1011110;  // 94
			end
			137:
			begin
				o_sweat_level = 7'b0110110;  // 54
				o_wind_magnitude = 15'b101111101011001;  // 95.34765625
				o_wind_direction = 9'b100100011;  // 291
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010110;  // 86
				o_fan_3 = 7'b1011110;  // 94
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			138:
			begin
				o_sweat_level = 7'b0010111;  // 23
				o_wind_magnitude = 15'b101000110011010;  // 81.6015625
				o_wind_direction = 9'b100100101;  // 293
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1000001;  // 65
				o_fan_3 = 7'b1001010;  // 74
				o_fan_4 = 7'b0001001;  // 9
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			139:
			begin
				o_sweat_level = 7'b1001010;  // 74
				o_wind_magnitude = 15'b001001000111001;  // 18.22265625
				o_wind_direction = 9'b000101011;  // 43
				o_fan_1 = 7'b0111010;  // 58
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111000;  // 56
			end
			140:
			begin
				o_sweat_level = 7'b0011101;  // 29
				o_wind_magnitude = 15'b000110011001010;  // 12.7890625
				o_wind_direction = 9'b011000100;  // 196
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000011;  // 3
				o_fan_4 = 7'b0001100;  // 12
				o_fan_5 = 7'b0001000;  // 8
				o_fan_6 = 7'b0000000;  // 0
			end
			141:
			begin
				o_sweat_level = 7'b0110111;  // 55
				o_wind_magnitude = 15'b001100001100111;  // 24.40234375
				o_wind_direction = 9'b100011111;  // 287
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b0111101;  // 61
				o_fan_4 = 7'b0110100;  // 52
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			142:
			begin
				o_sweat_level = 7'b0001100;  // 12
				o_wind_magnitude = 15'b101110000001110;  // 92.0546875
				o_wind_direction = 9'b010001110;  // 142
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0100010;  // 34
				o_fan_5 = 7'b1011010;  // 90
				o_fan_6 = 7'b0111000;  // 56
			end
			143:
			begin
				o_sweat_level = 7'b0110100;  // 52
				o_wind_magnitude = 15'b010111011101111;  // 46.93359375
				o_wind_direction = 9'b001010001;  // 81
				o_fan_1 = 7'b1000000;  // 64
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111010;  // 58
				o_fan_6 = 7'b1001001;  // 73
			end
			144:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b010001001111000;  // 34.46875
				o_wind_direction = 9'b001001000;  // 72
				o_fan_1 = 7'b0011001;  // 25
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000111;  // 7
				o_fan_6 = 7'b0100000;  // 32
			end
			145:
			begin
				o_sweat_level = 7'b0111100;  // 60
				o_wind_magnitude = 15'b110110010010000;  // 108.5625
				o_wind_direction = 9'b001101000;  // 104
				o_fan_1 = 7'b1000000;  // 64
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1010111;  // 87
				o_fan_6 = 7'b1100110;  // 102
			end
			146:
			begin
				o_sweat_level = 7'b0110010;  // 50
				o_wind_magnitude = 15'b010000010010110;  // 32.5859375
				o_wind_direction = 9'b000001101;  // 13
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0111101;  // 61
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110101;  // 53
			end
			147:
			begin
				o_sweat_level = 7'b0101011;  // 43
				o_wind_magnitude = 15'b110111110001001;  // 111.53515625
				o_wind_direction = 9'b101010000;  // 336
				o_fan_1 = 7'b1010010;  // 82
				o_fan_2 = 7'b1101001;  // 105
				o_fan_3 = 7'b1001000;  // 72
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			148:
			begin
				o_sweat_level = 7'b0000010;  // 2
				o_wind_magnitude = 15'b101011011001101;  // 86.80078125
				o_wind_direction = 9'b000111101;  // 61
				o_fan_1 = 7'b1001010;  // 74
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000001;  // 1
				o_fan_6 = 7'b1001011;  // 75
			end
			149:
			begin
				o_sweat_level = 7'b0110000;  // 48
				o_wind_magnitude = 15'b100010111010011;  // 69.82421875
				o_wind_direction = 9'b101100000;  // 352
				o_fan_1 = 7'b0110110;  // 54
				o_fan_2 = 7'b1000000;  // 64
				o_fan_3 = 7'b0001001;  // 9
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			150:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b100101110000011;  // 75.51171875
				o_wind_direction = 9'b011001101;  // 205
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1000001;  // 65
				o_fan_4 = 7'b1010111;  // 87
				o_fan_5 = 7'b1000111;  // 71
				o_fan_6 = 7'b0110010;  // 50
			end
			151:
			begin
				o_sweat_level = 7'b0011011;  // 27
				o_wind_magnitude = 15'b110111111011100;  // 111.859375
				o_wind_direction = 9'b000110110;  // 54
				o_fan_1 = 7'b1100101;  // 101
				o_fan_2 = 7'b0001011;  // 11
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b1011010;  // 90
			end
			152:
			begin
				o_sweat_level = 7'b1100001;  // 97
				o_wind_magnitude = 15'b010000001100001;  // 32.37890625
				o_wind_direction = 9'b011100010;  // 226
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111101;  // 61
				o_fan_4 = 7'b1000001;  // 65
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			153:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b111101010011000;  // 122.59375
				o_wind_direction = 9'b000111110;  // 62
				o_fan_1 = 7'b1100111;  // 103
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000011;  // 3
				o_fan_6 = 7'b1101100;  // 108
			end
			154:
			begin
				o_sweat_level = 7'b0111011;  // 59
				o_wind_magnitude = 15'b011100110100101;  // 57.64453125
				o_wind_direction = 9'b100100001;  // 289
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000111;  // 71
				o_fan_3 = 7'b1001101;  // 77
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			155:
			begin
				o_sweat_level = 7'b0101101;  // 45
				o_wind_magnitude = 15'b101101100000010;  // 91.0078125
				o_wind_direction = 9'b001111110;  // 126
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110110;  // 54
				o_fan_5 = 7'b1011011;  // 91
				o_fan_6 = 7'b1010110;  // 86
			end
			156:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b011100001100111;  // 56.40234375
				o_wind_direction = 9'b000011001;  // 25
				o_fan_1 = 7'b1001110;  // 78
				o_fan_2 = 7'b1000010;  // 66
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111101;  // 61
			end
			157:
			begin
				o_sweat_level = 7'b0101000;  // 40
				o_wind_magnitude = 15'b110011110101001;  // 103.66015625
				o_wind_direction = 9'b001111110;  // 126
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b1100001;  // 97
				o_fan_6 = 7'b1011011;  // 91
			end
			158:
			begin
				o_sweat_level = 7'b0001010;  // 10
				o_wind_magnitude = 15'b000011100111000;  // 7.21875
				o_wind_direction = 9'b010010110;  // 150
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000011;  // 3
				o_fan_5 = 7'b0000111;  // 7
				o_fan_6 = 7'b0000011;  // 3
			end
			159:
			begin
				o_sweat_level = 7'b0010111;  // 23
				o_wind_magnitude = 15'b001110011100010;  // 28.8828125
				o_wind_direction = 9'b100001101;  // 269
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0001101;  // 13
				o_fan_3 = 7'b0011100;  // 28
				o_fan_4 = 7'b0001110;  // 14
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			160:
			begin
				o_sweat_level = 7'b0111111;  // 63
				o_wind_magnitude = 15'b101101101011101;  // 91.36328125
				o_wind_direction = 9'b101100000;  // 352
				o_fan_1 = 7'b1010101;  // 85
				o_fan_2 = 7'b1011100;  // 92
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			161:
			begin
				o_sweat_level = 7'b0100100;  // 36
				o_wind_magnitude = 15'b100011100010110;  // 71.0859375
				o_wind_direction = 9'b001011100;  // 92
				o_fan_1 = 7'b1000010;  // 66
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000100;  // 68
				o_fan_6 = 7'b1010101;  // 85
			end
			162:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b011111100111101;  // 63.23828125
				o_wind_direction = 9'b011011110;  // 222
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0101010;  // 42
				o_fan_4 = 7'b0111101;  // 61
				o_fan_5 = 7'b0010011;  // 19
				o_fan_6 = 7'b0000000;  // 0
			end
			163:
			begin
				o_sweat_level = 7'b1100001;  // 97
				o_wind_magnitude = 15'b110001110101101;  // 99.67578125
				o_wind_direction = 9'b100100010;  // 290
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1011000;  // 88
				o_fan_3 = 7'b1100000;  // 96
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			164:
			begin
				o_sweat_level = 7'b1000111;  // 71
				o_wind_magnitude = 15'b000010010001101;  // 4.55078125
				o_wind_direction = 9'b101001110;  // 334
				o_fan_1 = 7'b0110011;  // 51
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			165:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b110101111010000;  // 107.8125
				o_wind_direction = 9'b100000100;  // 260
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000100;  // 68
				o_fan_3 = 7'b1100111;  // 103
				o_fan_4 = 7'b1010100;  // 84
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			166:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b101001110011100;  // 83.609375
				o_wind_direction = 9'b010000010;  // 130
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b1011001;  // 89
				o_fan_6 = 7'b1010010;  // 82
			end
			167:
			begin
				o_sweat_level = 7'b0011010;  // 26
				o_wind_magnitude = 15'b101111011110100;  // 94.953125
				o_wind_direction = 9'b000101111;  // 47
				o_fan_1 = 7'b1011010;  // 90
				o_fan_2 = 7'b0010101;  // 21
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b1000101;  // 69
			end
			168:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b100101000011011;  // 74.10546875
				o_wind_direction = 9'b000011000;  // 24
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b0101011;  // 43
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0011110;  // 30
			end
			169:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b111010000001101;  // 116.05078125
				o_wind_direction = 9'b100000110;  // 262
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000111;  // 71
				o_fan_3 = 7'b1101011;  // 107
				o_fan_4 = 7'b1010101;  // 85
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			170:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b110111011011110;  // 110.8671875
				o_wind_direction = 9'b000111010;  // 58
				o_fan_1 = 7'b1100010;  // 98
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1100000;  // 96
			end
			171:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b111100100001000;  // 121.03125
				o_wind_direction = 9'b010010101;  // 149
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1001111;  // 79
				o_fan_5 = 7'b1101110;  // 110
				o_fan_6 = 7'b1010000;  // 80
			end
			172:
			begin
				o_sweat_level = 7'b0011111;  // 31
				o_wind_magnitude = 15'b010010111010100;  // 37.828125
				o_wind_direction = 9'b100101010;  // 298
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0100000;  // 32
				o_fan_3 = 7'b0100001;  // 33
				o_fan_4 = 7'b0000001;  // 1
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			173:
			begin
				o_sweat_level = 7'b1001010;  // 74
				o_wind_magnitude = 15'b111000100111000;  // 113.21875
				o_wind_direction = 9'b001100101;  // 101
				o_fan_1 = 7'b1000100;  // 68
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1010110;  // 86
				o_fan_6 = 7'b1101001;  // 105
			end
			174:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b010111000001000;  // 46.03125
				o_wind_direction = 9'b000101010;  // 42
				o_fan_1 = 7'b0101100;  // 44
				o_fan_2 = 7'b0001110;  // 14
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0011110;  // 30
			end
			175:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b000100110100011;  // 9.63671875
				o_wind_direction = 9'b000100101;  // 37
				o_fan_1 = 7'b0001001;  // 9
				o_fan_2 = 7'b0000011;  // 3
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000101;  // 5
			end
			176:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b010010010111000;  // 36.71875
				o_wind_direction = 9'b101000010;  // 322
				o_fan_1 = 7'b0001101;  // 13
				o_fan_2 = 7'b0100100;  // 36
				o_fan_3 = 7'b0010110;  // 22
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			177:
			begin
				o_sweat_level = 7'b0000101;  // 5
				o_wind_magnitude = 15'b100101011000000;  // 74.75
				o_wind_direction = 9'b100011010;  // 282
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0110001;  // 49
				o_fan_3 = 7'b1001000;  // 72
				o_fan_4 = 7'b0010111;  // 23
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			178:
			begin
				o_sweat_level = 7'b0111101;  // 61
				o_wind_magnitude = 15'b011110100111010;  // 61.2265625
				o_wind_direction = 9'b011111010;  // 250
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110111;  // 55
				o_fan_3 = 7'b1001110;  // 78
				o_fan_4 = 7'b1001001;  // 73
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			179:
			begin
				o_sweat_level = 7'b1100011;  // 99
				o_wind_magnitude = 15'b110101101011010;  // 107.3515625
				o_wind_direction = 9'b100010010;  // 274
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1001111;  // 79
				o_fan_3 = 7'b1100111;  // 103
				o_fan_4 = 7'b1001001;  // 73
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			180:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b000101000110111;  // 10.21484375
				o_wind_direction = 9'b011000001;  // 193
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000010;  // 2
				o_fan_4 = 7'b0001001;  // 9
				o_fan_5 = 7'b0000111;  // 7
				o_fan_6 = 7'b0000000;  // 0
			end
			181:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b111010100111000;  // 117.21875
				o_wind_direction = 9'b010000110;  // 134
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0011011;  // 27
				o_fan_5 = 7'b1110000;  // 112
				o_fan_6 = 7'b1010100;  // 84
			end
			182:
			begin
				o_sweat_level = 7'b0100100;  // 36
				o_wind_magnitude = 15'b100010000010110;  // 68.0859375
				o_wind_direction = 9'b001100100;  // 100
				o_fan_1 = 7'b0010111;  // 23
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0101011;  // 43
				o_fan_6 = 7'b1000011;  // 67
			end
			183:
			begin
				o_sweat_level = 7'b0100001;  // 33
				o_wind_magnitude = 15'b111100101000111;  // 121.27734375
				o_wind_direction = 9'b001111001;  // 121
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000001;  // 1
				o_fan_5 = 7'b1101001;  // 105
				o_fan_6 = 7'b1100111;  // 103
			end
			184:
			begin
				o_sweat_level = 7'b1011110;  // 94
				o_wind_magnitude = 15'b011001001000001;  // 50.25390625
				o_wind_direction = 9'b000011001;  // 25
				o_fan_1 = 7'b1001011;  // 75
				o_fan_2 = 7'b1000000;  // 64
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111100;  // 60
			end
			185:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b010000101101100;  // 33.421875
				o_wind_direction = 9'b010110101;  // 181
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1000000;  // 64
				o_fan_5 = 7'b1000000;  // 64
				o_fan_6 = 7'b0110010;  // 50
			end
			186:
			begin
				o_sweat_level = 7'b0110001;  // 49
				o_wind_magnitude = 15'b101011011111111;  // 86.99609375
				o_wind_direction = 9'b100000010;  // 258
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111111;  // 63
				o_fan_3 = 7'b1011100;  // 92
				o_fan_4 = 7'b1001111;  // 79
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			187:
			begin
				o_sweat_level = 7'b0101011;  // 43
				o_wind_magnitude = 15'b100000111000001;  // 65.75390625
				o_wind_direction = 9'b010001011;  // 139
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111100;  // 60
				o_fan_5 = 7'b1010010;  // 82
				o_fan_6 = 7'b1000111;  // 71
			end
			188:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b100000011000000;  // 64.75
				o_wind_direction = 9'b011000100;  // 196
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111010;  // 58
				o_fan_4 = 7'b1010001;  // 81
				o_fan_5 = 7'b1001000;  // 72
				o_fan_6 = 7'b0110010;  // 50
			end
			189:
			begin
				o_sweat_level = 7'b1010101;  // 85
				o_wind_magnitude = 15'b001001011001111;  // 18.80859375
				o_wind_direction = 9'b101001111;  // 335
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b0111011;  // 59
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			190:
			begin
				o_sweat_level = 7'b1001000;  // 72
				o_wind_magnitude = 15'b011111111011111;  // 63.87109375
				o_wind_direction = 9'b000001100;  // 12
				o_fan_1 = 7'b1010000;  // 80
				o_fan_2 = 7'b1001001;  // 73
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111000;  // 56
			end
			191:
			begin
				o_sweat_level = 7'b1011110;  // 94
				o_wind_magnitude = 15'b001101000011000;  // 26.09375
				o_wind_direction = 9'b101010011;  // 339
				o_fan_1 = 7'b0111010;  // 58
				o_fan_2 = 7'b0111110;  // 62
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			192:
			begin
				o_sweat_level = 7'b0101101;  // 45
				o_wind_magnitude = 15'b110111001100001;  // 110.37890625
				o_wind_direction = 9'b000010101;  // 21
				o_fan_1 = 7'b1101000;  // 104
				o_fan_2 = 7'b1010100;  // 84
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000101;  // 69
			end
			193:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b001110000100110;  // 28.1484375
				o_wind_direction = 9'b100110110;  // 310
				o_fan_1 = 7'b0110100;  // 52
				o_fan_2 = 7'b0111111;  // 63
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			194:
			begin
				o_sweat_level = 7'b1100010;  // 98
				o_wind_magnitude = 15'b100110010001100;  // 76.546875
				o_wind_direction = 9'b011000100;  // 196
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b1010111;  // 87
				o_fan_5 = 7'b1001100;  // 76
				o_fan_6 = 7'b0110010;  // 50
			end
			195:
			begin
				o_sweat_level = 7'b0100100;  // 36
				o_wind_magnitude = 15'b111100101111101;  // 121.48828125
				o_wind_direction = 9'b001110100;  // 116
				o_fan_1 = 7'b0110110;  // 54
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1100100;  // 100
				o_fan_6 = 7'b1101000;  // 104
			end
			196:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b011110001011101;  // 60.36328125
				o_wind_direction = 9'b000111001;  // 57
				o_fan_1 = 7'b1001100;  // 76
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1001011;  // 75
			end
			197:
			begin
				o_sweat_level = 7'b0111010;  // 58
				o_wind_magnitude = 15'b101001001110111;  // 82.46484375
				o_wind_direction = 9'b010110001;  // 177
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1010100;  // 84
				o_fan_5 = 7'b1010110;  // 86
				o_fan_6 = 7'b0110100;  // 52
			end
			198:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b001011000110110;  // 22.2109375
				o_wind_direction = 9'b101001010;  // 330
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b0111101;  // 61
				o_fan_3 = 7'b0110111;  // 55
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			199:
			begin
				o_sweat_level = 7'b1010111;  // 87
				o_wind_magnitude = 15'b000011100110111;  // 7.21484375
				o_wind_direction = 9'b010110101;  // 181
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			200:
			begin
				o_sweat_level = 7'b1010111;  // 87
				o_wind_magnitude = 15'b100111100100000;  // 79.125
				o_wind_direction = 9'b001010111;  // 87
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000011;  // 67
				o_fan_6 = 7'b1011001;  // 89
			end
			201:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b100100010101000;  // 72.65625
				o_wind_direction = 9'b010101011;  // 171
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0111000;  // 56
				o_fan_5 = 7'b1000011;  // 67
				o_fan_6 = 7'b0001011;  // 11
			end
			202:
			begin
				o_sweat_level = 7'b0100011;  // 35
				o_wind_magnitude = 15'b010110011100001;  // 44.87890625
				o_wind_direction = 9'b001101011;  // 107
				o_fan_1 = 7'b0001001;  // 9
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0100000;  // 32
				o_fan_6 = 7'b0101010;  // 42
			end
			203:
			begin
				o_sweat_level = 7'b1001111;  // 79
				o_wind_magnitude = 15'b101100111100010;  // 89.8828125
				o_wind_direction = 9'b100001111;  // 271
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b1011110;  // 94
				o_fan_4 = 7'b1000111;  // 71
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			204:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b110101001110101;  // 106.45703125
				o_wind_direction = 9'b100111100;  // 316
				o_fan_1 = 7'b0011101;  // 29
				o_fan_2 = 7'b1100111;  // 103
				o_fan_3 = 7'b1001001;  // 73
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			205:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b100010011001111;  // 68.80859375
				o_wind_direction = 9'b001111010;  // 122
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000010;  // 2
				o_fan_5 = 7'b0111100;  // 60
				o_fan_6 = 7'b0111010;  // 58
			end
			206:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b000011100001000;  // 7.03125
				o_wind_direction = 9'b101010010;  // 338
				o_fan_1 = 7'b0000100;  // 4
				o_fan_2 = 7'b0000110;  // 6
				o_fan_3 = 7'b0000010;  // 2
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			207:
			begin
				o_sweat_level = 7'b1001111;  // 79
				o_wind_magnitude = 15'b101100010010110;  // 88.5859375
				o_wind_direction = 9'b001100011;  // 99
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1001101;  // 77
				o_fan_6 = 7'b1011101;  // 93
			end
			208:
			begin
				o_sweat_level = 7'b0110001;  // 49
				o_wind_magnitude = 15'b000110000010001;  // 12.06640625
				o_wind_direction = 9'b000001110;  // 14
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b0110110;  // 54
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110011;  // 51
			end
			209:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b111001101011000;  // 115.34375
				o_wind_direction = 9'b101011001;  // 345
				o_fan_1 = 7'b1011010;  // 90
				o_fan_2 = 7'b1101001;  // 105
				o_fan_3 = 7'b1000000;  // 64
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			210:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b000000000101000;  // 0.15625
				o_wind_direction = 9'b001100100;  // 100
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			211:
			begin
				o_sweat_level = 7'b0010101;  // 21
				o_wind_magnitude = 15'b010101001111011;  // 42.48046875
				o_wind_direction = 9'b100101011;  // 299
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0100100;  // 36
				o_fan_3 = 7'b0100101;  // 37
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			212:
			begin
				o_sweat_level = 7'b0100111;  // 39
				o_wind_magnitude = 15'b110001000010001;  // 98.06640625
				o_wind_direction = 9'b100001000;  // 264
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0100111;  // 39
				o_fan_3 = 7'b1100001;  // 97
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			213:
			begin
				o_sweat_level = 7'b0100110;  // 38
				o_wind_magnitude = 15'b010000001010110;  // 32.3359375
				o_wind_direction = 9'b001000010;  // 66
				o_fan_1 = 7'b0011010;  // 26
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000011;  // 3
				o_fan_6 = 7'b0011101;  // 29
			end
			214:
			begin
				o_sweat_level = 7'b0010101;  // 21
				o_wind_magnitude = 15'b101001100100111;  // 83.15234375
				o_wind_direction = 9'b011100000;  // 224
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0111001;  // 57
				o_fan_4 = 7'b1010000;  // 80
				o_fan_5 = 7'b0010110;  // 22
				o_fan_6 = 7'b0000000;  // 0
			end
			215:
			begin
				o_sweat_level = 7'b1011001;  // 89
				o_wind_magnitude = 15'b110110100000101;  // 109.01953125
				o_wind_direction = 9'b101010011;  // 339
				o_fan_1 = 7'b1010100;  // 84
				o_fan_2 = 7'b1100111;  // 103
				o_fan_3 = 7'b1000101;  // 69
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			216:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b001100110100000;  // 25.625
				o_wind_direction = 9'b100101111;  // 303
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111101;  // 61
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			217:
			begin
				o_sweat_level = 7'b0110111;  // 55
				o_wind_magnitude = 15'b110100111001011;  // 105.79296875
				o_wind_direction = 9'b010101011;  // 171
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1011010;  // 90
				o_fan_5 = 7'b1100011;  // 99
				o_fan_6 = 7'b0111010;  // 58
			end
			218:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b000110110000010;  // 13.5078125
				o_wind_direction = 9'b000010100;  // 20
				o_fan_1 = 7'b0111000;  // 56
				o_fan_2 = 7'b0110110;  // 54
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110100;  // 52
			end
			219:
			begin
				o_sweat_level = 7'b0011111;  // 31
				o_wind_magnitude = 15'b111101100010111;  // 123.08984375
				o_wind_direction = 9'b011111001;  // 249
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0010011;  // 19
				o_fan_3 = 7'b1110010;  // 114
				o_fan_4 = 7'b1011111;  // 95
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			220:
			begin
				o_sweat_level = 7'b1011111;  // 95
				o_wind_magnitude = 15'b111010111001111;  // 117.80859375
				o_wind_direction = 9'b101000011;  // 323
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b1101100;  // 108
				o_fan_3 = 7'b1010101;  // 85
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			221:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b010010110110111;  // 37.71484375
				o_wind_direction = 9'b010101011;  // 171
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0011101;  // 29
				o_fan_5 = 7'b0100011;  // 35
				o_fan_6 = 7'b0000101;  // 5
			end
			222:
			begin
				o_sweat_level = 7'b0011011;  // 27
				o_wind_magnitude = 15'b001010100100110;  // 21.1484375
				o_wind_direction = 9'b000100000;  // 32
				o_fan_1 = 7'b0010101;  // 21
				o_fan_2 = 7'b0001001;  // 9
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0001011;  // 11
			end
			223:
			begin
				o_sweat_level = 7'b1100011;  // 99
				o_wind_magnitude = 15'b011010011100000;  // 52.875
				o_wind_direction = 9'b001111011;  // 123
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b1001001;  // 73
				o_fan_6 = 7'b1001000;  // 72
			end
			224:
			begin
				o_sweat_level = 7'b1001001;  // 73
				o_wind_magnitude = 15'b001010101000001;  // 21.25390625
				o_wind_direction = 9'b011110110;  // 246
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0111011;  // 59
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			225:
			begin
				o_sweat_level = 7'b0000111;  // 7
				o_wind_magnitude = 15'b011101010111011;  // 58.73046875
				o_wind_direction = 9'b000110000;  // 48
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b0001100;  // 12
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0101011;  // 43
			end
			226:
			begin
				o_sweat_level = 7'b0110001;  // 49
				o_wind_magnitude = 15'b000100101100101;  // 9.39453125
				o_wind_direction = 9'b010011111;  // 159
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000101;  // 5
				o_fan_5 = 7'b0001001;  // 9
				o_fan_6 = 7'b0000011;  // 3
			end
			227:
			begin
				o_sweat_level = 7'b0001001;  // 9
				o_wind_magnitude = 15'b011000101001000;  // 49.28125
				o_wind_direction = 9'b000110001;  // 49
				o_fan_1 = 7'b0101110;  // 46
				o_fan_2 = 7'b0001001;  // 9
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0100101;  // 37
			end
			228:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b001001110000011;  // 19.51171875
				o_wind_direction = 9'b100111001;  // 313
				o_fan_1 = 7'b0000100;  // 4
				o_fan_2 = 7'b0010010;  // 18
				o_fan_3 = 7'b0001110;  // 14
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			229:
			begin
				o_sweat_level = 7'b0010100;  // 20
				o_wind_magnitude = 15'b011000100111100;  // 49.234375
				o_wind_direction = 9'b101000111;  // 327
				o_fan_1 = 7'b0010110;  // 22
				o_fan_2 = 7'b0110001;  // 49
				o_fan_3 = 7'b0011010;  // 26
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			230:
			begin
				o_sweat_level = 7'b1000001;  // 65
				o_wind_magnitude = 15'b110000000111100;  // 96.234375
				o_wind_direction = 9'b100011110;  // 286
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010100;  // 84
				o_fan_3 = 7'b1100000;  // 96
				o_fan_4 = 7'b0111101;  // 61
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			231:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b101101000011111;  // 90.12109375
				o_wind_direction = 9'b100110000;  // 304
				o_fan_1 = 7'b0110100;  // 52
				o_fan_2 = 7'b1011010;  // 90
				o_fan_3 = 7'b1010111;  // 87
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			232:
			begin
				o_sweat_level = 7'b1011010;  // 90
				o_wind_magnitude = 15'b011000010010101;  // 48.58203125
				o_wind_direction = 9'b001101010;  // 106
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000011;  // 67
				o_fan_6 = 7'b1001001;  // 73
			end
			233:
			begin
				o_sweat_level = 7'b0001111;  // 15
				o_wind_magnitude = 15'b110111110101010;  // 111.6640625
				o_wind_direction = 9'b010000010;  // 130
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010011;  // 19
				o_fan_5 = 7'b1101000;  // 104
				o_fan_6 = 7'b1010101;  // 85
			end
			234:
			begin
				o_sweat_level = 7'b1011010;  // 90
				o_wind_magnitude = 15'b111011110000010;  // 119.5078125
				o_wind_direction = 9'b010001001;  // 137
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1000011;  // 67
				o_fan_5 = 7'b1101100;  // 108
				o_fan_6 = 7'b1011010;  // 90
			end
			235:
			begin
				o_sweat_level = 7'b1100011;  // 99
				o_wind_magnitude = 15'b011100111001001;  // 57.78515625
				o_wind_direction = 9'b001101000;  // 104
				o_fan_1 = 7'b0111001;  // 57
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000101;  // 69
				o_fan_6 = 7'b1001101;  // 77
			end
			236:
			begin
				o_sweat_level = 7'b0000111;  // 7
				o_wind_magnitude = 15'b100111101110100;  // 79.453125
				o_wind_direction = 9'b101011110;  // 350
				o_fan_1 = 7'b0111100;  // 60
				o_fan_2 = 7'b1001010;  // 74
				o_fan_3 = 7'b0001101;  // 13
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			237:
			begin
				o_sweat_level = 7'b1001001;  // 73
				o_wind_magnitude = 15'b111011000110011;  // 118.19921875
				o_wind_direction = 9'b100001111;  // 271
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010000;  // 80
				o_fan_3 = 7'b1101100;  // 108
				o_fan_4 = 7'b1001110;  // 78
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			238:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b011001100100010;  // 51.1328125
				o_wind_direction = 9'b011110100;  // 244
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b1001000;  // 72
				o_fan_4 = 7'b1000111;  // 71
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			239:
			begin
				o_sweat_level = 7'b0111100;  // 60
				o_wind_magnitude = 15'b110101010110111;  // 106.71484375
				o_wind_direction = 9'b011110111;  // 247
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111000;  // 56
				o_fan_3 = 7'b1100010;  // 98
				o_fan_4 = 7'b1011100;  // 92
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			240:
			begin
				o_sweat_level = 7'b1100000;  // 96
				o_wind_magnitude = 15'b010101101110110;  // 43.4609375
				o_wind_direction = 9'b000101010;  // 42
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b0111000;  // 56
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000000;  // 64
			end
			241:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b110101100101101;  // 107.17578125
				o_wind_direction = 9'b101100111;  // 359
				o_fan_1 = 7'b1011011;  // 91
				o_fan_2 = 7'b1011101;  // 93
				o_fan_3 = 7'b0000001;  // 1
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			242:
			begin
				o_sweat_level = 7'b0001010;  // 10
				o_wind_magnitude = 15'b100001000001001;  // 66.03515625
				o_wind_direction = 9'b000101000;  // 40
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0010110;  // 22
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0101010;  // 42
			end
			243:
			begin
				o_sweat_level = 7'b0000010;  // 2
				o_wind_magnitude = 15'b100100010111111;  // 72.74609375
				o_wind_direction = 9'b010110100;  // 180
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0111110;  // 62
				o_fan_5 = 7'b0111110;  // 62
				o_fan_6 = 7'b0000000;  // 0
			end
			244:
			begin
				o_sweat_level = 7'b0110011;  // 51
				o_wind_magnitude = 15'b001001001101100;  // 18.421875
				o_wind_direction = 9'b011100101;  // 229
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b0110011;  // 51
				o_fan_6 = 7'b0110010;  // 50
			end
			245:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b010000100111000;  // 33.21875
				o_wind_direction = 9'b000110100;  // 52
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111111;  // 63
			end
			246:
			begin
				o_sweat_level = 7'b1001001;  // 73
				o_wind_magnitude = 15'b001100100010101;  // 25.08203125
				o_wind_direction = 9'b011100001;  // 225
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111010;  // 58
				o_fan_4 = 7'b0111110;  // 62
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			247:
			begin
				o_sweat_level = 7'b0101011;  // 43
				o_wind_magnitude = 15'b011001101110101;  // 51.45703125
				o_wind_direction = 9'b000111001;  // 57
				o_fan_1 = 7'b1001000;  // 72
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000111;  // 71
			end
			248:
			begin
				o_sweat_level = 7'b1001100;  // 76
				o_wind_magnitude = 15'b000011011111101;  // 6.98828125
				o_wind_direction = 9'b100011010;  // 282
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			249:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b010101101011011;  // 43.35546875
				o_wind_direction = 9'b000111100;  // 60
				o_fan_1 = 7'b1000100;  // 68
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000100;  // 68
			end
			250:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b100101010001100;  // 74.546875
				o_wind_direction = 9'b100111110;  // 318
				o_fan_1 = 7'b0111101;  // 61
				o_fan_2 = 7'b1010110;  // 86
				o_fan_3 = 7'b1001010;  // 74
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			251:
			begin
				o_sweat_level = 7'b0000111;  // 7
				o_wind_magnitude = 15'b100101001110110;  // 74.4609375
				o_wind_direction = 9'b011101111;  // 239
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0111111;  // 63
				o_fan_4 = 7'b1000000;  // 64
				o_fan_5 = 7'b0000001;  // 1
				o_fan_6 = 7'b0000000;  // 0
			end
			252:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b011011101011111;  // 55.37109375
				o_wind_direction = 9'b010000101;  // 133
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111000;  // 56
				o_fan_5 = 7'b1001100;  // 76
				o_fan_6 = 7'b1000110;  // 70
			end
			253:
			begin
				o_sweat_level = 7'b1010011;  // 83
				o_wind_magnitude = 15'b001010101111010;  // 21.4765625
				o_wind_direction = 9'b000011010;  // 26
				o_fan_1 = 7'b0111100;  // 60
				o_fan_2 = 7'b0110111;  // 55
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110110;  // 54
			end
			254:
			begin
				o_sweat_level = 7'b1010001;  // 81
				o_wind_magnitude = 15'b101010001000010;  // 84.2578125
				o_wind_direction = 9'b000110111;  // 55
				o_fan_1 = 7'b1011000;  // 88
				o_fan_2 = 7'b0110101;  // 53
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1010100;  // 84
			end
			255:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b001001000010001;  // 18.06640625
				o_wind_direction = 9'b101001000;  // 328
				o_fan_1 = 7'b0110110;  // 54
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			256:
			begin
				o_sweat_level = 7'b1011111;  // 95
				o_wind_magnitude = 15'b111100101101011;  // 121.41796875
				o_wind_direction = 9'b001100110;  // 102
				o_fan_1 = 7'b1000100;  // 68
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1011010;  // 90
				o_fan_6 = 7'b1101101;  // 109
			end
			257:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b111101010110010;  // 122.6953125
				o_wind_direction = 9'b101000101;  // 325
				o_fan_1 = 7'b1001011;  // 75
				o_fan_2 = 7'b1101111;  // 111
				o_fan_3 = 7'b1010100;  // 84
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			258:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b010110110111101;  // 45.73828125
				o_wind_direction = 9'b000001010;  // 10
				o_fan_1 = 7'b0101010;  // 42
				o_fan_2 = 7'b0100011;  // 35
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000111;  // 7
			end
			259:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b110001010110010;  // 98.6953125
				o_wind_direction = 9'b001111001;  // 121
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000001;  // 1
				o_fan_5 = 7'b1010101;  // 85
				o_fan_6 = 7'b1010100;  // 84
			end
			260:
			begin
				o_sweat_level = 7'b1011001;  // 89
				o_wind_magnitude = 15'b010100000110111;  // 40.21484375
				o_wind_direction = 9'b011000101;  // 197
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110111;  // 55
				o_fan_4 = 7'b1000101;  // 69
				o_fan_5 = 7'b0111111;  // 63
				o_fan_6 = 7'b0110010;  // 50
			end
			261:
			begin
				o_sweat_level = 7'b1000011;  // 67
				o_wind_magnitude = 15'b000100010011000;  // 8.59375
				o_wind_direction = 9'b101100011;  // 355
				o_fan_1 = 7'b0110101;  // 53
				o_fan_2 = 7'b0110101;  // 53
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			262:
			begin
				o_sweat_level = 7'b0110010;  // 50
				o_wind_magnitude = 15'b101101001000100;  // 90.265625
				o_wind_direction = 9'b100100010;  // 290
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010100;  // 84
				o_fan_3 = 7'b1011100;  // 92
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			263:
			begin
				o_sweat_level = 7'b0011000;  // 24
				o_wind_magnitude = 15'b010100111100011;  // 41.88671875
				o_wind_direction = 9'b010011110;  // 158
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0011001;  // 25
				o_fan_5 = 7'b0101001;  // 41
				o_fan_6 = 7'b0001111;  // 15
			end
			264:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b111011000010011;  // 118.07421875
				o_wind_direction = 9'b001011010;  // 90
				o_fan_1 = 7'b0111010;  // 58
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0111010;  // 58
				o_fan_6 = 7'b1110110;  // 118
			end
			265:
			begin
				o_sweat_level = 7'b1000000;  // 64
				o_wind_magnitude = 15'b011110110001111;  // 61.55859375
				o_wind_direction = 9'b100111100;  // 316
				o_fan_1 = 7'b0111010;  // 58
				o_fan_2 = 7'b1001111;  // 79
				o_fan_3 = 7'b1000111;  // 71
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			266:
			begin
				o_sweat_level = 7'b0010011;  // 19
				o_wind_magnitude = 15'b110011010001110;  // 102.5546875
				o_wind_direction = 9'b100010100;  // 276
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0111100;  // 60
				o_fan_3 = 7'b1100101;  // 101
				o_fan_4 = 7'b0101001;  // 41
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			267:
			begin
				o_sweat_level = 7'b1100010;  // 98
				o_wind_magnitude = 15'b101011010101011;  // 86.66796875
				o_wind_direction = 9'b011001101;  // 205
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1000100;  // 68
				o_fan_4 = 7'b1011101;  // 93
				o_fan_5 = 7'b1001010;  // 74
				o_fan_6 = 7'b0110010;  // 50
			end
			268:
			begin
				o_sweat_level = 7'b0011011;  // 27
				o_wind_magnitude = 15'b000010101000011;  // 5.26171875
				o_wind_direction = 9'b001110011;  // 115
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000100;  // 4
				o_fan_6 = 7'b0000100;  // 4
			end
			269:
			begin
				o_sweat_level = 7'b1010111;  // 87
				o_wind_magnitude = 15'b101101010000011;  // 90.51171875
				o_wind_direction = 9'b100011011;  // 283
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010000;  // 80
				o_fan_3 = 7'b1011110;  // 94
				o_fan_4 = 7'b0111111;  // 63
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			270:
			begin
				o_sweat_level = 7'b0001100;  // 12
				o_wind_magnitude = 15'b100100000100011;  // 72.13671875
				o_wind_direction = 9'b000111100;  // 60
				o_fan_1 = 7'b0111110;  // 62
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0111110;  // 62
			end
			271:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b000000100110001;  // 1.19140625
				o_wind_direction = 9'b000101110;  // 46
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			272:
			begin
				o_sweat_level = 7'b0000110;  // 6
				o_wind_magnitude = 15'b101110011111000;  // 92.96875
				o_wind_direction = 9'b011111101;  // 253
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0010100;  // 20
				o_fan_3 = 7'b1011000;  // 88
				o_fan_4 = 7'b1000011;  // 67
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			273:
			begin
				o_sweat_level = 7'b0111100;  // 60
				o_wind_magnitude = 15'b000100101011101;  // 9.36328125
				o_wind_direction = 9'b010010000;  // 144
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110110;  // 54
				o_fan_6 = 7'b0110100;  // 52
			end
			274:
			begin
				o_sweat_level = 7'b0111000;  // 56
				o_wind_magnitude = 15'b001101101001100;  // 27.296875
				o_wind_direction = 9'b100010110;  // 278
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b0111111;  // 63
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			275:
			begin
				o_sweat_level = 7'b0001111;  // 15
				o_wind_magnitude = 15'b100101111010000;  // 75.8125
				o_wind_direction = 9'b000111011;  // 59
				o_fan_1 = 7'b1000010;  // 66
				o_fan_2 = 7'b0000001;  // 1
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b1000000;  // 64
			end
			276:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b110000100000111;  // 97.02734375
				o_wind_direction = 9'b000011101;  // 29
				o_fan_1 = 7'b1100000;  // 96
				o_fan_2 = 7'b0110001;  // 49
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0101110;  // 46
			end
			277:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b010110100000001;  // 45.00390625
				o_wind_direction = 9'b001011111;  // 95
				o_fan_1 = 7'b0010010;  // 18
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0011001;  // 25
				o_fan_6 = 7'b0101100;  // 44
			end
			278:
			begin
				o_sweat_level = 7'b1011100;  // 92
				o_wind_magnitude = 15'b010001010111110;  // 34.7421875
				o_wind_direction = 9'b100001101;  // 269
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b1000011;  // 67
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			279:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b100000100111010;  // 65.2265625
				o_wind_direction = 9'b010011111;  // 159
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1000110;  // 70
				o_fan_5 = 7'b1010010;  // 82
				o_fan_6 = 7'b0111101;  // 61
			end
			280:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b010000010011100;  // 32.609375
				o_wind_direction = 9'b000010010;  // 18
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b0111100;  // 60
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110111;  // 55
			end
			281:
			begin
				o_sweat_level = 7'b1010011;  // 83
				o_wind_magnitude = 15'b110111100110010;  // 111.1953125
				o_wind_direction = 9'b001101011;  // 107
				o_fan_1 = 7'b0111110;  // 62
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1011010;  // 90
				o_fan_6 = 7'b1100110;  // 102
			end
			282:
			begin
				o_sweat_level = 7'b1100010;  // 98
				o_wind_magnitude = 15'b110101100011011;  // 107.10546875
				o_wind_direction = 9'b011101000;  // 232
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1011100;  // 92
				o_fan_4 = 7'b1100011;  // 99
				o_fan_5 = 7'b0111001;  // 57
				o_fan_6 = 7'b0110010;  // 50
			end
			283:
			begin
				o_sweat_level = 7'b0110011;  // 51
				o_wind_magnitude = 15'b000110110011110;  // 13.6171875
				o_wind_direction = 9'b010100011;  // 163
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110110;  // 54
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0110011;  // 51
			end
			284:
			begin
				o_sweat_level = 7'b0111111;  // 63
				o_wind_magnitude = 15'b111100001010001;  // 120.31640625
				o_wind_direction = 9'b100001111;  // 271
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1010000;  // 80
				o_fan_3 = 7'b1101101;  // 109
				o_fan_4 = 7'b1001111;  // 79
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			285:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b110011010111101;  // 102.73828125
				o_wind_direction = 9'b001101101;  // 109
				o_fan_1 = 7'b0111011;  // 59
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1011000;  // 88
				o_fan_6 = 7'b1100010;  // 98
			end
			286:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b001111101101000;  // 31.40625
				o_wind_direction = 9'b101000101;  // 325
				o_fan_1 = 7'b0001101;  // 13
				o_fan_2 = 7'b0011111;  // 31
				o_fan_3 = 7'b0010001;  // 17
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			287:
			begin
				o_sweat_level = 7'b1000011;  // 67
				o_wind_magnitude = 15'b000010100011011;  // 5.10546875
				o_wind_direction = 9'b011110010;  // 242
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110100;  // 52
				o_fan_4 = 7'b0110100;  // 52
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			288:
			begin
				o_sweat_level = 7'b1011000;  // 88
				o_wind_magnitude = 15'b001000000111011;  // 16.23046875
				o_wind_direction = 9'b001111110;  // 126
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111001;  // 57
				o_fan_6 = 7'b0111000;  // 56
			end
			289:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b111010111100011;  // 117.88671875
				o_wind_direction = 9'b010110001;  // 177
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b1100010;  // 98
				o_fan_5 = 7'b1101000;  // 104
				o_fan_6 = 7'b0000101;  // 5
			end
			290:
			begin
				o_sweat_level = 7'b1000111;  // 71
				o_wind_magnitude = 15'b110110011000010;  // 108.7578125
				o_wind_direction = 9'b001000100;  // 68
				o_fan_1 = 7'b1011100;  // 92
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111001;  // 57
				o_fan_6 = 7'b1100100;  // 100
			end
			291:
			begin
				o_sweat_level = 7'b0010111;  // 23
				o_wind_magnitude = 15'b100000110011111;  // 65.62109375
				o_wind_direction = 9'b101011111;  // 351
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111101;  // 61
				o_fan_3 = 7'b0001010;  // 10
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			292:
			begin
				o_sweat_level = 7'b0110111;  // 55
				o_wind_magnitude = 15'b101101101011100;  // 91.359375
				o_wind_direction = 9'b010100100;  // 164
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1010001;  // 81
				o_fan_5 = 7'b1011110;  // 94
				o_fan_6 = 7'b0111110;  // 62
			end
			293:
			begin
				o_sweat_level = 7'b0101101;  // 45
				o_wind_magnitude = 15'b111100001100000;  // 120.375
				o_wind_direction = 9'b001010010;  // 82
				o_fan_1 = 7'b1010110;  // 86
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1001000;  // 72
				o_fan_6 = 7'b1101101;  // 109
			end
			294:
			begin
				o_sweat_level = 7'b0111000;  // 56
				o_wind_magnitude = 15'b111001110101011;  // 115.66796875
				o_wind_direction = 9'b011110111;  // 247
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111001;  // 57
				o_fan_3 = 7'b1100111;  // 103
				o_fan_4 = 7'b1100000;  // 96
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			295:
			begin
				o_sweat_level = 7'b0100101;  // 37
				o_wind_magnitude = 15'b001101110011111;  // 27.62109375
				o_wind_direction = 9'b001001010;  // 74
				o_fan_1 = 7'b0111011;  // 59
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0111111;  // 63
			end
			296:
			begin
				o_sweat_level = 7'b0110100;  // 52
				o_wind_magnitude = 15'b100110011001001;  // 76.78515625
				o_wind_direction = 9'b011101100;  // 236
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1010001;  // 81
				o_fan_4 = 7'b1010100;  // 84
				o_fan_5 = 7'b0110100;  // 52
				o_fan_6 = 7'b0110010;  // 50
			end
			297:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b100001001100010;  // 66.3828125
				o_wind_direction = 9'b011110101;  // 245
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b1010000;  // 80
				o_fan_4 = 7'b1001101;  // 77
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			298:
			begin
				o_sweat_level = 7'b0100101;  // 37
				o_wind_magnitude = 15'b011000010101110;  // 48.6796875
				o_wind_direction = 9'b000111101;  // 61
				o_fan_1 = 7'b1000110;  // 70
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000111;  // 71
			end
			299:
			begin
				o_sweat_level = 7'b1100000;  // 96
				o_wind_magnitude = 15'b010011001101101;  // 38.42578125
				o_wind_direction = 9'b100111000;  // 312
				o_fan_1 = 7'b0110101;  // 53
				o_fan_2 = 7'b1000100;  // 68
				o_fan_3 = 7'b1000000;  // 64
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			300:
			begin
				o_sweat_level = 7'b0100011;  // 35
				o_wind_magnitude = 15'b000111101011101;  // 15.36328125
				o_wind_direction = 9'b001001110;  // 78
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110100;  // 52
				o_fan_6 = 7'b0111001;  // 57
			end
			301:
			begin
				o_sweat_level = 7'b0011101;  // 29
				o_wind_magnitude = 15'b101111110010100;  // 95.578125
				o_wind_direction = 9'b000110011;  // 51
				o_fan_1 = 7'b1011000;  // 88
				o_fan_2 = 7'b0001110;  // 14
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b1001001;  // 73
			end
			302:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b101000011111011;  // 80.98046875
				o_wind_direction = 9'b000000101;  // 5
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b1000010;  // 66
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000110;  // 6
			end
			303:
			begin
				o_sweat_level = 7'b0000110;  // 6
				o_wind_magnitude = 15'b111111011000111;  // 126.77734375
				o_wind_direction = 9'b000010110;  // 22
				o_fan_1 = 7'b1111101;  // 125
				o_fan_2 = 7'b1001101;  // 77
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0101111;  // 47
			end
			304:
			begin
				o_sweat_level = 7'b0000010;  // 2
				o_wind_magnitude = 15'b000100001010011;  // 8.32421875
				o_wind_direction = 9'b101010110;  // 342
				o_fan_1 = 7'b0000101;  // 5
				o_fan_2 = 7'b0001000;  // 8
				o_fan_3 = 7'b0000010;  // 2
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			305:
			begin
				o_sweat_level = 7'b0011111;  // 31
				o_wind_magnitude = 15'b100001010111010;  // 66.7265625
				o_wind_direction = 9'b001010011;  // 83
				o_fan_1 = 7'b0101000;  // 40
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0011010;  // 26
				o_fan_6 = 7'b1000010;  // 66
			end
			306:
			begin
				o_sweat_level = 7'b0001001;  // 9
				o_wind_magnitude = 15'b010110111110001;  // 45.94140625
				o_wind_direction = 9'b011101110;  // 238
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0100110;  // 38
				o_fan_4 = 7'b0101000;  // 40
				o_fan_5 = 7'b0000001;  // 1
				o_fan_6 = 7'b0000000;  // 0
			end
			307:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b100111000001101;  // 78.05078125
				o_wind_direction = 9'b100010100;  // 276
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b1011000;  // 88
				o_fan_4 = 7'b1000001;  // 65
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			308:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b110101001000111;  // 106.27734375
				o_wind_direction = 9'b000111000;  // 56
				o_fan_1 = 7'b1011111;  // 95
				o_fan_2 = 7'b0000111;  // 7
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b1011000;  // 88
			end
			309:
			begin
				o_sweat_level = 7'b0010111;  // 23
				o_wind_magnitude = 15'b111111001000001;  // 126.25390625
				o_wind_direction = 9'b011100001;  // 225
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b1011001;  // 89
				o_fan_4 = 7'b1111001;  // 121
				o_fan_5 = 7'b0100000;  // 32
				o_fan_6 = 7'b0000000;  // 0
			end
			310:
			begin
				o_sweat_level = 7'b0010101;  // 21
				o_wind_magnitude = 15'b001100001010001;  // 24.31640625
				o_wind_direction = 9'b000101010;  // 42
				o_fan_1 = 7'b0010111;  // 23
				o_fan_2 = 7'b0000111;  // 7
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010000;  // 16
			end
			311:
			begin
				o_sweat_level = 7'b0101100;  // 44
				o_wind_magnitude = 15'b100111001000000;  // 78.25
				o_wind_direction = 9'b011101001;  // 233
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0111110;  // 62
				o_fan_4 = 7'b1000111;  // 71
				o_fan_5 = 7'b0001001;  // 9
				o_fan_6 = 7'b0000000;  // 0
			end
			312:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b001001110101010;  // 19.6640625
				o_wind_direction = 9'b010111100;  // 188
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110011;  // 51
				o_fan_4 = 7'b0111011;  // 59
				o_fan_5 = 7'b0111001;  // 57
				o_fan_6 = 7'b0110010;  // 50
			end
			313:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b111000011011011;  // 112.85546875
				o_wind_direction = 9'b100110100;  // 308
				o_fan_1 = 7'b0001111;  // 15
				o_fan_2 = 7'b1101000;  // 104
				o_fan_3 = 7'b1011000;  // 88
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			314:
			begin
				o_sweat_level = 7'b1000110;  // 70
				o_wind_magnitude = 15'b011010011001001;  // 52.78515625
				o_wind_direction = 9'b101010110;  // 342
				o_fan_1 = 7'b1000011;  // 67
				o_fan_2 = 7'b1001011;  // 75
				o_fan_3 = 7'b0111010;  // 58
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			315:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b111001011100010;  // 114.8828125
				o_wind_direction = 9'b001000000;  // 64
				o_fan_1 = 7'b1100001;  // 97
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b1100101;  // 101
			end
			316:
			begin
				o_sweat_level = 7'b1010101;  // 85
				o_wind_magnitude = 15'b001101111010100;  // 27.828125
				o_wind_direction = 9'b101010110;  // 342
				o_fan_1 = 7'b0111011;  // 59
				o_fan_2 = 7'b0111111;  // 63
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			317:
			begin
				o_sweat_level = 7'b0110000;  // 48
				o_wind_magnitude = 15'b011000111001001;  // 49.78515625
				o_wind_direction = 9'b000111101;  // 61
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000111;  // 71
			end
			318:
			begin
				o_sweat_level = 7'b1100001;  // 97
				o_wind_magnitude = 15'b001100011111100;  // 24.984375
				o_wind_direction = 9'b010011011;  // 155
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0111110;  // 62
				o_fan_6 = 7'b0110111;  // 55
			end
			319:
			begin
				o_sweat_level = 7'b0011110;  // 30
				o_wind_magnitude = 15'b010010111110100;  // 37.953125
				o_wind_direction = 9'b011001011;  // 203
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0001110;  // 14
				o_fan_4 = 7'b0100101;  // 37
				o_fan_5 = 7'b0010110;  // 22
				o_fan_6 = 7'b0000000;  // 0
			end
			320:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b101100100001000;  // 89.03125
				o_wind_direction = 9'b001110101;  // 117
				o_fan_1 = 7'b0000100;  // 4
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b1001010;  // 74
				o_fan_6 = 7'b1001111;  // 79
			end
			321:
			begin
				o_sweat_level = 7'b0001001;  // 9
				o_wind_magnitude = 15'b110111100001001;  // 111.03515625
				o_wind_direction = 9'b011110100;  // 244
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000111;  // 7
				o_fan_3 = 7'b1100011;  // 99
				o_fan_4 = 7'b1011011;  // 91
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			322:
			begin
				o_sweat_level = 7'b0100011;  // 35
				o_wind_magnitude = 15'b110111111010000;  // 111.8125
				o_wind_direction = 9'b100011001;  // 281
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b1101101;  // 109
				o_fan_4 = 7'b0100100;  // 36
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			323:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b011001010111101;  // 50.73828125
				o_wind_direction = 9'b011101001;  // 233
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1000110;  // 70
				o_fan_4 = 7'b1001001;  // 73
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			324:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b001000110011010;  // 17.6015625
				o_wind_direction = 9'b101011101;  // 349
				o_fan_1 = 7'b0001101;  // 13
				o_fan_2 = 7'b0010000;  // 16
				o_fan_3 = 7'b0000011;  // 3
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			325:
			begin
				o_sweat_level = 7'b0011101;  // 29
				o_wind_magnitude = 15'b111101001011110;  // 122.3671875
				o_wind_direction = 9'b000111111;  // 63
				o_fan_1 = 7'b1100110;  // 102
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000110;  // 6
				o_fan_6 = 7'b1101100;  // 108
			end
			326:
			begin
				o_sweat_level = 7'b0001001;  // 9
				o_wind_magnitude = 15'b000011111010001;  // 7.81640625
				o_wind_direction = 9'b010000100;  // 132
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000001;  // 1
				o_fan_5 = 7'b0000111;  // 7
				o_fan_6 = 7'b0000101;  // 5
			end
			327:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b001101001101000;  // 26.40625
				o_wind_direction = 9'b100110011;  // 307
				o_fan_1 = 7'b0110011;  // 51
				o_fan_2 = 7'b0111110;  // 62
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			328:
			begin
				o_sweat_level = 7'b0100010;  // 34
				o_wind_magnitude = 15'b001010000000011;  // 20.01171875
				o_wind_direction = 9'b010011010;  // 154
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0001011;  // 11
				o_fan_5 = 7'b0010011;  // 19
				o_fan_6 = 7'b0001000;  // 8
			end
			329:
			begin
				o_sweat_level = 7'b0011001;  // 25
				o_wind_magnitude = 15'b110100011010001;  // 104.81640625
				o_wind_direction = 9'b101100110;  // 358
				o_fan_1 = 7'b1011000;  // 88
				o_fan_2 = 7'b1011100;  // 92
				o_fan_3 = 7'b0000011;  // 3
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			330:
			begin
				o_sweat_level = 7'b0100100;  // 36
				o_wind_magnitude = 15'b111000110110111;  // 113.71484375
				o_wind_direction = 9'b010000111;  // 135
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0011101;  // 29
				o_fan_5 = 7'b1101101;  // 109
				o_fan_6 = 7'b1010000;  // 80
			end
			331:
			begin
				o_sweat_level = 7'b0111000;  // 56
				o_wind_magnitude = 15'b101000111110101;  // 81.95703125
				o_wind_direction = 9'b010111101;  // 189
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b1011000;  // 88
				o_fan_5 = 7'b1010001;  // 81
				o_fan_6 = 7'b0110010;  // 50
			end
			332:
			begin
				o_sweat_level = 7'b1010000;  // 80
				o_wind_magnitude = 15'b100100101111011;  // 73.48046875
				o_wind_direction = 9'b011111101;  // 253
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b1010101;  // 85
				o_fan_4 = 7'b1001100;  // 76
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			333:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b011011111101111;  // 55.93359375
				o_wind_direction = 9'b001001111;  // 79
				o_fan_1 = 7'b0100100;  // 36
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0010010;  // 18
				o_fan_6 = 7'b0110110;  // 54
			end
			334:
			begin
				o_sweat_level = 7'b0001001;  // 9
				o_wind_magnitude = 15'b010010010001011;  // 36.54296875
				o_wind_direction = 9'b001010110;  // 86
				o_fan_1 = 7'b0010100;  // 20
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0001111;  // 15
				o_fan_6 = 7'b0100100;  // 36
			end
			335:
			begin
				o_sweat_level = 7'b0111011;  // 59
				o_wind_magnitude = 15'b100001000010100;  // 66.078125
				o_wind_direction = 9'b101011000;  // 344
				o_fan_1 = 7'b1001000;  // 72
				o_fan_2 = 7'b1010010;  // 82
				o_fan_3 = 7'b0111011;  // 59
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			336:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b000001110010001;  // 3.56640625
				o_wind_direction = 9'b011011111;  // 223
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110011;  // 51
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			337:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b000101010000000;  // 10.5
				o_wind_direction = 9'b001010011;  // 83
				o_fan_1 = 7'b0110101;  // 53
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110100;  // 52
				o_fan_6 = 7'b0110111;  // 55
			end
			338:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b100110000001000;  // 76.03125
				o_wind_direction = 9'b101001101;  // 333
				o_fan_1 = 7'b0101001;  // 41
				o_fan_2 = 7'b1001011;  // 75
				o_fan_3 = 7'b0100010;  // 34
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			339:
			begin
				o_sweat_level = 7'b0010100;  // 20
				o_wind_magnitude = 15'b011101001001001;  // 58.28515625
				o_wind_direction = 9'b100111000;  // 312
				o_fan_1 = 7'b0001100;  // 12
				o_fan_2 = 7'b0110111;  // 55
				o_fan_3 = 7'b0101011;  // 43
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			340:
			begin
				o_sweat_level = 7'b0010001;  // 17
				o_wind_magnitude = 15'b001010000101011;  // 20.16796875
				o_wind_direction = 9'b001010010;  // 82
				o_fan_1 = 7'b0001100;  // 12
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000111;  // 7
				o_fan_6 = 7'b0010011;  // 19
			end
			341:
			begin
				o_sweat_level = 7'b1011000;  // 88
				o_wind_magnitude = 15'b010101011000000;  // 42.75
				o_wind_direction = 9'b100101100;  // 300
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000100;  // 68
				o_fan_3 = 7'b1000100;  // 68
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			342:
			begin
				o_sweat_level = 7'b0011010;  // 26
				o_wind_magnitude = 15'b011011001100000;  // 54.375
				o_wind_direction = 9'b100110000;  // 304
				o_fan_1 = 7'b0000011;  // 3
				o_fan_2 = 7'b0110000;  // 48
				o_fan_3 = 7'b0101101;  // 45
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			343:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b101110001100111;  // 92.40234375
				o_wind_direction = 9'b101001110;  // 334
				o_fan_1 = 7'b1001011;  // 75
				o_fan_2 = 7'b1100000;  // 96
				o_fan_3 = 7'b1000110;  // 70
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			344:
			begin
				o_sweat_level = 7'b1100010;  // 98
				o_wind_magnitude = 15'b100010100000101;  // 69.01953125
				o_wind_direction = 9'b010111011;  // 187
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b1010001;  // 81
				o_fan_5 = 7'b1001101;  // 77
				o_fan_6 = 7'b0110010;  // 50
			end
			345:
			begin
				o_sweat_level = 7'b0000101;  // 5
				o_wind_magnitude = 15'b111110001110010;  // 124.4453125
				o_wind_direction = 9'b000001011;  // 11
				o_fan_1 = 7'b1110101;  // 117
				o_fan_2 = 7'b1011101;  // 93
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010111;  // 23
			end
			346:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b010001001001001;  // 34.28515625
				o_wind_direction = 9'b011100101;  // 229
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0011001;  // 25
				o_fan_4 = 7'b0100000;  // 32
				o_fan_5 = 7'b0000110;  // 6
				o_fan_6 = 7'b0000000;  // 0
			end
			347:
			begin
				o_sweat_level = 7'b0111110;  // 62
				o_wind_magnitude = 15'b011111111101000;  // 63.90625
				o_wind_direction = 9'b101001100;  // 332
				o_fan_1 = 7'b1000010;  // 66
				o_fan_2 = 7'b1010001;  // 81
				o_fan_3 = 7'b1000000;  // 64
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			348:
			begin
				o_sweat_level = 7'b0011111;  // 31
				o_wind_magnitude = 15'b001011101011111;  // 23.37109375
				o_wind_direction = 9'b001110101;  // 117
				o_fan_1 = 7'b0000001;  // 1
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0010011;  // 19
				o_fan_6 = 7'b0010100;  // 20
			end
			349:
			begin
				o_sweat_level = 7'b1000001;  // 65
				o_wind_magnitude = 15'b110100010100011;  // 104.63671875
				o_wind_direction = 9'b011100110;  // 230
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1011010;  // 90
				o_fan_4 = 7'b1100011;  // 99
				o_fan_5 = 7'b0111010;  // 58
				o_fan_6 = 7'b0110010;  // 50
			end
			350:
			begin
				o_sweat_level = 7'b1011010;  // 90
				o_wind_magnitude = 15'b110000101000010;  // 97.2578125
				o_wind_direction = 9'b001101000;  // 104
				o_fan_1 = 7'b0111111;  // 63
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1010011;  // 83
				o_fan_6 = 7'b1100001;  // 97
			end
			351:
			begin
				o_sweat_level = 7'b0100101;  // 37
				o_wind_magnitude = 15'b111110101001011;  // 125.29296875
				o_wind_direction = 9'b100100100;  // 292
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1100011;  // 99
				o_fan_3 = 7'b1101011;  // 107
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			352:
			begin
				o_sweat_level = 7'b1001100;  // 76
				o_wind_magnitude = 15'b111000111110110;  // 113.9609375
				o_wind_direction = 9'b100111001;  // 313
				o_fan_1 = 7'b0111110;  // 62
				o_fan_2 = 7'b1101000;  // 104
				o_fan_3 = 7'b1011011;  // 91
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			353:
			begin
				o_sweat_level = 7'b0100010;  // 34
				o_wind_magnitude = 15'b011111101011101;  // 63.36328125
				o_wind_direction = 9'b010000111;  // 135
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010000;  // 16
				o_fan_5 = 7'b0111101;  // 61
				o_fan_6 = 7'b0101100;  // 44
			end
			354:
			begin
				o_sweat_level = 7'b0011011;  // 27
				o_wind_magnitude = 15'b000110111011010;  // 13.8515625
				o_wind_direction = 9'b001110010;  // 114
				o_fan_1 = 7'b0000001;  // 1
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0001011;  // 11
				o_fan_6 = 7'b0001100;  // 12
			end
			355:
			begin
				o_sweat_level = 7'b0101101;  // 45
				o_wind_magnitude = 15'b001001001101010;  // 18.4140625
				o_wind_direction = 9'b100111001;  // 313
				o_fan_1 = 7'b0000100;  // 4
				o_fan_2 = 7'b0010001;  // 17
				o_fan_3 = 7'b0001101;  // 13
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			356:
			begin
				o_sweat_level = 7'b1010001;  // 81
				o_wind_magnitude = 15'b011011010110001;  // 54.69140625
				o_wind_direction = 9'b001011001;  // 89
				o_fan_1 = 7'b0111111;  // 63
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111111;  // 63
				o_fan_6 = 7'b1001101;  // 77
			end
			357:
			begin
				o_sweat_level = 7'b1000010;  // 66
				o_wind_magnitude = 15'b100101100011000;  // 75.09375
				o_wind_direction = 9'b001000010;  // 66
				o_fan_1 = 7'b1010000;  // 80
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b1010100;  // 84
			end
			358:
			begin
				o_sweat_level = 7'b0100100;  // 36
				o_wind_magnitude = 15'b011011100001110;  // 55.0546875
				o_wind_direction = 9'b010110110;  // 182
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1001010;  // 74
				o_fan_5 = 7'b1001001;  // 73
				o_fan_6 = 7'b0110010;  // 50
			end
			359:
			begin
				o_sweat_level = 7'b1001000;  // 72
				o_wind_magnitude = 15'b010101001110111;  // 42.46484375
				o_wind_direction = 9'b000100011;  // 35
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111110;  // 62
			end
			360:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b111101011100001;  // 122.87890625
				o_wind_direction = 9'b010011111;  // 159
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1011000;  // 88
				o_fan_5 = 7'b1101110;  // 110
				o_fan_6 = 7'b1000111;  // 71
			end
			361:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b100011110000011;  // 71.51171875
				o_wind_direction = 9'b010000010;  // 130
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0001100;  // 12
				o_fan_5 = 7'b1000011;  // 67
				o_fan_6 = 7'b0110110;  // 54
			end
			362:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b100000100011011;  // 65.10546875
				o_wind_direction = 9'b001010111;  // 87
				o_fan_1 = 7'b1000011;  // 67
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000000;  // 64
				o_fan_6 = 7'b1010010;  // 82
			end
			363:
			begin
				o_sweat_level = 7'b0010110;  // 22
				o_wind_magnitude = 15'b100001100011000;  // 67.09375
				o_wind_direction = 9'b011111000;  // 248
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0001001;  // 9
				o_fan_3 = 7'b0111110;  // 62
				o_fan_4 = 7'b0110100;  // 52
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			364:
			begin
				o_sweat_level = 7'b0101011;  // 43
				o_wind_magnitude = 15'b101000101110101;  // 81.45703125
				o_wind_direction = 9'b100100010;  // 290
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0111110;  // 62
				o_fan_3 = 7'b1001100;  // 76
				o_fan_4 = 7'b0001110;  // 14
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			365:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b010110010011001;  // 44.59765625
				o_wind_direction = 9'b011110110;  // 246
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110100;  // 52
				o_fan_3 = 7'b1000110;  // 70
				o_fan_4 = 7'b1000100;  // 68
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			366:
			begin
				o_sweat_level = 7'b0000010;  // 2
				o_wind_magnitude = 15'b000001001001111;  // 2.30859375
				o_wind_direction = 9'b100011111;  // 287
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000001;  // 1
				o_fan_3 = 7'b0000010;  // 2
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			367:
			begin
				o_sweat_level = 7'b1000000;  // 64
				o_wind_magnitude = 15'b010000011110011;  // 32.94921875
				o_wind_direction = 9'b001000100;  // 68
				o_fan_1 = 7'b0111110;  // 62
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110100;  // 52
				o_fan_6 = 7'b1000001;  // 65
			end
			368:
			begin
				o_sweat_level = 7'b0100101;  // 37
				o_wind_magnitude = 15'b100001001000100;  // 66.265625
				o_wind_direction = 9'b011000011;  // 195
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111010;  // 58
				o_fan_4 = 7'b1010001;  // 81
				o_fan_5 = 7'b1001001;  // 73
				o_fan_6 = 7'b0110010;  // 50
			end
			369:
			begin
				o_sweat_level = 7'b0001000;  // 8
				o_wind_magnitude = 15'b011110101010110;  // 61.3359375
				o_wind_direction = 9'b010111001;  // 185
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000101;  // 5
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0000000;  // 0
			end
			370:
			begin
				o_sweat_level = 7'b0000101;  // 5
				o_wind_magnitude = 15'b111001110011001;  // 115.59765625
				o_wind_direction = 9'b101000011;  // 323
				o_fan_1 = 7'b0101101;  // 45
				o_fan_2 = 7'b1110010;  // 114
				o_fan_3 = 7'b1000101;  // 69
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			371:
			begin
				o_sweat_level = 7'b0100101;  // 37
				o_wind_magnitude = 15'b010000101101010;  // 33.4140625
				o_wind_direction = 9'b000110101;  // 53
				o_fan_1 = 7'b0011110;  // 30
				o_fan_2 = 7'b0000100;  // 4
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0011010;  // 26
			end
			372:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b101100000110000;  // 88.1875
				o_wind_direction = 9'b100100001;  // 289
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1000010;  // 66
				o_fan_3 = 7'b1010011;  // 83
				o_fan_4 = 7'b0010000;  // 16
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			373:
			begin
				o_sweat_level = 7'b1000010;  // 66
				o_wind_magnitude = 15'b100100111111111;  // 73.99609375
				o_wind_direction = 9'b010011001;  // 153
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1000110;  // 70
				o_fan_5 = 7'b1010110;  // 86
				o_fan_6 = 7'b1000010;  // 66
			end
			374:
			begin
				o_sweat_level = 7'b0111011;  // 59
				o_wind_magnitude = 15'b000110110011100;  // 13.609375
				o_wind_direction = 9'b001111111;  // 127
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0110111;  // 55
			end
			375:
			begin
				o_sweat_level = 7'b1001010;  // 74
				o_wind_magnitude = 15'b000000010101011;  // 0.66796875
				o_wind_direction = 9'b101000100;  // 324
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			376:
			begin
				o_sweat_level = 7'b1010000;  // 80
				o_wind_magnitude = 15'b011000001010010;  // 48.3203125
				o_wind_direction = 9'b100111000;  // 312
				o_fan_1 = 7'b0110111;  // 55
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b1000011;  // 67
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			377:
			begin
				o_sweat_level = 7'b0101000;  // 40
				o_wind_magnitude = 15'b101111010001000;  // 94.53125
				o_wind_direction = 9'b100101001;  // 297
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1011001;  // 89
				o_fan_3 = 7'b1011100;  // 92
				o_fan_4 = 7'b0110100;  // 52
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			378:
			begin
				o_sweat_level = 7'b1011011;  // 91
				o_wind_magnitude = 15'b111011001001111;  // 118.30859375
				o_wind_direction = 9'b001110110;  // 118
				o_fan_1 = 7'b0110011;  // 51
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1100100;  // 100
				o_fan_6 = 7'b1100110;  // 102
			end
			379:
			begin
				o_sweat_level = 7'b1001000;  // 72
				o_wind_magnitude = 15'b100001000011000;  // 66.09375
				o_wind_direction = 9'b100011100;  // 284
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1001000;  // 72
				o_fan_3 = 7'b1010010;  // 82
				o_fan_4 = 7'b0111011;  // 59
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			380:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b011001100110111;  // 51.21484375
				o_wind_direction = 9'b010100101;  // 165
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1000100;  // 68
				o_fan_5 = 7'b1001010;  // 74
				o_fan_6 = 7'b0111000;  // 56
			end
			381:
			begin
				o_sweat_level = 7'b0000101;  // 5
				o_wind_magnitude = 15'b011101011110011;  // 58.94921875
				o_wind_direction = 9'b100001011;  // 267
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0011010;  // 26
				o_fan_3 = 7'b0111010;  // 58
				o_fan_4 = 7'b0100000;  // 32
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			382:
			begin
				o_sweat_level = 7'b0011101;  // 29
				o_wind_magnitude = 15'b000001001100101;  // 2.39453125
				o_wind_direction = 9'b010100000;  // 160
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000001;  // 1
				o_fan_5 = 7'b0000010;  // 2
				o_fan_6 = 7'b0000000;  // 0
			end
			383:
			begin
				o_sweat_level = 7'b1000110;  // 70
				o_wind_magnitude = 15'b010001101010001;  // 35.31640625
				o_wind_direction = 9'b011101111;  // 239
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1000001;  // 65
				o_fan_4 = 7'b1000001;  // 65
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			384:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b001100000101010;  // 24.1640625
				o_wind_direction = 9'b101011001;  // 345
				o_fan_1 = 7'b0010001;  // 17
				o_fan_2 = 7'b0010111;  // 23
				o_fan_3 = 7'b0000110;  // 6
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			385:
			begin
				o_sweat_level = 7'b0011000;  // 24
				o_wind_magnitude = 15'b110100001101010;  // 104.4140625
				o_wind_direction = 9'b001101110;  // 110
				o_fan_1 = 7'b0010001;  // 17
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b1001111;  // 79
				o_fan_6 = 7'b1100001;  // 97
			end
			386:
			begin
				o_sweat_level = 7'b0110011;  // 51
				o_wind_magnitude = 15'b111101001110011;  // 122.44921875
				o_wind_direction = 9'b100100001;  // 289
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1100000;  // 96
				o_fan_3 = 7'b1101011;  // 107
				o_fan_4 = 7'b0111101;  // 61
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			387:
			begin
				o_sweat_level = 7'b1000111;  // 71
				o_wind_magnitude = 15'b110110000100010;  // 108.1328125
				o_wind_direction = 9'b000100100;  // 36
				o_fan_1 = 7'b1100111;  // 103
				o_fan_2 = 7'b1000111;  // 71
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1010001;  // 81
			end
			388:
			begin
				o_sweat_level = 7'b1000100;  // 68
				o_wind_magnitude = 15'b101001010001100;  // 82.546875
				o_wind_direction = 9'b101011111;  // 351
				o_fan_1 = 7'b1010001;  // 81
				o_fan_2 = 7'b1011000;  // 88
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			389:
			begin
				o_sweat_level = 7'b0111010;  // 58
				o_wind_magnitude = 15'b011100010000100;  // 56.515625
				o_wind_direction = 9'b000101010;  // 42
				o_fan_1 = 7'b1001101;  // 77
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000100;  // 68
			end
			390:
			begin
				o_sweat_level = 7'b0110000;  // 48
				o_wind_magnitude = 15'b101010101011110;  // 85.3671875
				o_wind_direction = 9'b101001001;  // 329
				o_fan_1 = 7'b1000110;  // 70
				o_fan_2 = 7'b1011100;  // 92
				o_fan_3 = 7'b1000111;  // 71
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			391:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b000010011111101;  // 4.98828125
				o_wind_direction = 9'b010111100;  // 188
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000100;  // 4
				o_fan_5 = 7'b0000011;  // 3
				o_fan_6 = 7'b0000000;  // 0
			end
			392:
			begin
				o_sweat_level = 7'b0000111;  // 7
				o_wind_magnitude = 15'b000110010100100;  // 12.640625
				o_wind_direction = 9'b011111101;  // 253
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000010;  // 2
				o_fan_3 = 7'b0001100;  // 12
				o_fan_4 = 7'b0001001;  // 9
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			393:
			begin
				o_sweat_level = 7'b0001100;  // 12
				o_wind_magnitude = 15'b010101000001011;  // 42.04296875
				o_wind_direction = 9'b010101110;  // 174
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0100001;  // 33
				o_fan_5 = 7'b0100110;  // 38
				o_fan_6 = 7'b0000100;  // 4
			end
			394:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b011100011000101;  // 56.76953125
				o_wind_direction = 9'b100111001;  // 313
				o_fan_1 = 7'b0111000;  // 56
				o_fan_2 = 7'b1001101;  // 77
				o_fan_3 = 7'b1000110;  // 70
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			395:
			begin
				o_sweat_level = 7'b0101011;  // 43
				o_wind_magnitude = 15'b110010111101110;  // 101.9296875
				o_wind_direction = 9'b100110111;  // 311
				o_fan_1 = 7'b0111011;  // 59
				o_fan_2 = 7'b1100010;  // 98
				o_fan_3 = 7'b1011000;  // 88
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			396:
			begin
				o_sweat_level = 7'b0000000;  // 0
				o_wind_magnitude = 15'b100110010111111;  // 76.74609375
				o_wind_direction = 9'b001101001;  // 105
				o_fan_1 = 7'b0010011;  // 19
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0110110;  // 54
				o_fan_6 = 7'b1001010;  // 74
			end
			397:
			begin
				o_sweat_level = 7'b1001010;  // 74
				o_wind_magnitude = 15'b101100010111101;  // 88.73828125
				o_wind_direction = 9'b010110001;  // 177
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1010111;  // 87
				o_fan_5 = 7'b1011001;  // 89
				o_fan_6 = 7'b0110100;  // 52
			end
			398:
			begin
				o_sweat_level = 7'b0110000;  // 48
				o_wind_magnitude = 15'b011010111010010;  // 53.8203125
				o_wind_direction = 9'b011010100;  // 212
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1000000;  // 64
				o_fan_4 = 7'b1001100;  // 76
				o_fan_5 = 7'b0111110;  // 62
				o_fan_6 = 7'b0110010;  // 50
			end
			399:
			begin
				o_sweat_level = 7'b0110111;  // 55
				o_wind_magnitude = 15'b000010000000110;  // 4.0234375
				o_wind_direction = 9'b100001010;  // 266
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110100;  // 52
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			400:
			begin
				o_sweat_level = 7'b0010110;  // 22
				o_wind_magnitude = 15'b010100001001101;  // 40.30078125
				o_wind_direction = 9'b000011011;  // 27
				o_fan_1 = 7'b0101000;  // 40
				o_fan_2 = 7'b0010101;  // 21
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010010;  // 18
			end
			401:
			begin
				o_sweat_level = 7'b1000000;  // 64
				o_wind_magnitude = 15'b110111111011000;  // 111.84375
				o_wind_direction = 9'b011011001;  // 217
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1010011;  // 83
				o_fan_4 = 7'b1101001;  // 105
				o_fan_5 = 7'b1000111;  // 71
				o_fan_6 = 7'b0110010;  // 50
			end
			402:
			begin
				o_sweat_level = 7'b0001011;  // 11
				o_wind_magnitude = 15'b001111110000110;  // 31.5234375
				o_wind_direction = 9'b000011111;  // 31
				o_fan_1 = 7'b0011111;  // 31
				o_fan_2 = 7'b0001111;  // 15
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010000;  // 16
			end
			403:
			begin
				o_sweat_level = 7'b0110000;  // 48
				o_wind_magnitude = 15'b000100101010110;  // 9.3359375
				o_wind_direction = 9'b000111000;  // 56
				o_fan_1 = 7'b0001000;  // 8
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000111;  // 7
			end
			404:
			begin
				o_sweat_level = 7'b0011101;  // 29
				o_wind_magnitude = 15'b010001010111100;  // 34.734375
				o_wind_direction = 9'b101010000;  // 336
				o_fan_1 = 7'b0010100;  // 20
				o_fan_2 = 7'b0100010;  // 34
				o_fan_3 = 7'b0001110;  // 14
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			405:
			begin
				o_sweat_level = 7'b0000010;  // 2
				o_wind_magnitude = 15'b011110111011100;  // 61.859375
				o_wind_direction = 9'b000100011;  // 35
				o_fan_1 = 7'b0111101;  // 61
				o_fan_2 = 7'b0011010;  // 26
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0100011;  // 35
			end
			406:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b100010010101010;  // 68.6640625
				o_wind_direction = 9'b011101001;  // 233
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b0111111;  // 63
				o_fan_5 = 7'b0001000;  // 8
				o_fan_6 = 7'b0000000;  // 0
			end
			407:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b010101110100100;  // 43.640625
				o_wind_direction = 9'b101011000;  // 344
				o_fan_1 = 7'b1000001;  // 65
				o_fan_2 = 7'b1000111;  // 71
				o_fan_3 = 7'b0110111;  // 55
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			408:
			begin
				o_sweat_level = 7'b0111101;  // 61
				o_wind_magnitude = 15'b000100110000110;  // 9.5234375
				o_wind_direction = 9'b010100011;  // 163
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b0110110;  // 54
				o_fan_6 = 7'b0110011;  // 51
			end
			409:
			begin
				o_sweat_level = 7'b1000110;  // 70
				o_wind_magnitude = 15'b001111010100010;  // 30.6328125
				o_wind_direction = 9'b100111110;  // 318
				o_fan_1 = 7'b0110110;  // 54
				o_fan_2 = 7'b1000000;  // 64
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			410:
			begin
				o_sweat_level = 7'b0101011;  // 43
				o_wind_magnitude = 15'b001000110110011;  // 17.69921875
				o_wind_direction = 9'b101100110;  // 358
				o_fan_1 = 7'b0111001;  // 57
				o_fan_2 = 7'b0111001;  // 57
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			411:
			begin
				o_sweat_level = 7'b0111101;  // 61
				o_wind_magnitude = 15'b100000111000111;  // 65.77734375
				o_wind_direction = 9'b001000101;  // 69
				o_fan_1 = 7'b1001011;  // 75
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110111;  // 55
				o_fan_6 = 7'b1010000;  // 80
			end
			412:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b110111000100101;  // 110.14453125
				o_wind_direction = 9'b011110110;  // 246
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0001011;  // 11
				o_fan_3 = 7'b1100100;  // 100
				o_fan_4 = 7'b1011001;  // 89
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			413:
			begin
				o_sweat_level = 7'b1011101;  // 93
				o_wind_magnitude = 15'b010110110100011;  // 45.63671875
				o_wind_direction = 9'b011111001;  // 249
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110101;  // 53
				o_fan_3 = 7'b1000111;  // 71
				o_fan_4 = 7'b1000011;  // 67
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			414:
			begin
				o_sweat_level = 7'b0001100;  // 12
				o_wind_magnitude = 15'b101110101001000;  // 93.28125
				o_wind_direction = 9'b000010010;  // 18
				o_fan_1 = 7'b1011011;  // 91
				o_fan_2 = 7'b0111110;  // 62
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0011100;  // 28
			end
			415:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b101101011010010;  // 90.8203125
				o_wind_direction = 9'b010011001;  // 153
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0110001;  // 49
				o_fan_5 = 7'b1011010;  // 90
				o_fan_6 = 7'b0101001;  // 41
			end
			416:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b111001111111001;  // 115.97265625
				o_wind_direction = 9'b000010111;  // 23
				o_fan_1 = 7'b1101011;  // 107
				o_fan_2 = 7'b1010100;  // 84
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1001000;  // 72
			end
			417:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b000101000011100;  // 10.109375
				o_wind_direction = 9'b010011001;  // 153
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110100;  // 52
				o_fan_5 = 7'b0110111;  // 55
				o_fan_6 = 7'b0110100;  // 52
			end
			418:
			begin
				o_sweat_level = 7'b0011001;  // 25
				o_wind_magnitude = 15'b001101111000011;  // 27.76171875
				o_wind_direction = 9'b100000110;  // 262
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0001010;  // 10
				o_fan_3 = 7'b0011011;  // 27
				o_fan_4 = 7'b0010001;  // 17
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			419:
			begin
				o_sweat_level = 7'b1001110;  // 78
				o_wind_magnitude = 15'b110010111000110;  // 101.7734375
				o_wind_direction = 9'b010000100;  // 132
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111100;  // 60
				o_fan_5 = 7'b1100010;  // 98
				o_fan_6 = 7'b1010111;  // 87
			end
			420:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b001100011110011;  // 24.94921875
				o_wind_direction = 9'b100011110;  // 286
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111010;  // 58
				o_fan_3 = 7'b0111101;  // 61
				o_fan_4 = 7'b0110100;  // 52
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			421:
			begin
				o_sweat_level = 7'b0110001;  // 49
				o_wind_magnitude = 15'b111101101001101;  // 123.30078125
				o_wind_direction = 9'b010010010;  // 146
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1001100;  // 76
				o_fan_5 = 7'b1101111;  // 111
				o_fan_6 = 7'b1010100;  // 84
			end
			422:
			begin
				o_sweat_level = 7'b0010101;  // 21
				o_wind_magnitude = 15'b111011111101101;  // 119.92578125
				o_wind_direction = 9'b010001101;  // 141
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0101010;  // 42
				o_fan_5 = 7'b1110110;  // 118
				o_fan_6 = 7'b1001011;  // 75
			end
			423:
			begin
				o_sweat_level = 7'b0000101;  // 5
				o_wind_magnitude = 15'b001101111011010;  // 27.8515625
				o_wind_direction = 9'b000011110;  // 30
				o_fan_1 = 7'b0011011;  // 27
				o_fan_2 = 7'b0001101;  // 13
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0001101;  // 13
			end
			424:
			begin
				o_sweat_level = 7'b0010101;  // 21
				o_wind_magnitude = 15'b101111101010100;  // 95.328125
				o_wind_direction = 9'b101001000;  // 328
				o_fan_1 = 7'b0101100;  // 44
				o_fan_2 = 7'b1011110;  // 94
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			425:
			begin
				o_sweat_level = 7'b0010100;  // 20
				o_wind_magnitude = 15'b011101111100010;  // 59.8828125
				o_wind_direction = 9'b011100010;  // 226
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0101011;  // 43
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0001110;  // 14
				o_fan_6 = 7'b0000000;  // 0
			end
			426:
			begin
				o_sweat_level = 7'b1000011;  // 67
				o_wind_magnitude = 15'b000111111110110;  // 15.9609375
				o_wind_direction = 9'b001101110;  // 110
				o_fan_1 = 7'b0110011;  // 51
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0111001;  // 57
			end
			427:
			begin
				o_sweat_level = 7'b1001000;  // 72
				o_wind_magnitude = 15'b001001110011010;  // 19.6015625
				o_wind_direction = 9'b010110100;  // 180
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0111010;  // 58
				o_fan_5 = 7'b0111010;  // 58
				o_fan_6 = 7'b0110010;  // 50
			end
			428:
			begin
				o_sweat_level = 7'b0000111;  // 7
				o_wind_magnitude = 15'b010010101000000;  // 37.25
				o_wind_direction = 9'b000100001;  // 33
				o_fan_1 = 7'b0100101;  // 37
				o_fan_2 = 7'b0010000;  // 16
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010100;  // 20
			end
			429:
			begin
				o_sweat_level = 7'b1000011;  // 67
				o_wind_magnitude = 15'b110010011100001;  // 100.87890625
				o_wind_direction = 9'b010100001;  // 161
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1010010;  // 82
				o_fan_5 = 7'b1100011;  // 99
				o_fan_6 = 7'b1000010;  // 66
			end
			430:
			begin
				o_sweat_level = 7'b0111100;  // 60
				o_wind_magnitude = 15'b011110110010001;  // 61.56640625
				o_wind_direction = 9'b001001111;  // 79
				o_fan_1 = 7'b1000110;  // 70
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111011;  // 59
				o_fan_6 = 7'b1010000;  // 80
			end
			431:
			begin
				o_sweat_level = 7'b0100010;  // 34
				o_wind_magnitude = 15'b100000100101110;  // 65.1796875
				o_wind_direction = 9'b101010110;  // 342
				o_fan_1 = 7'b0101011;  // 43
				o_fan_2 = 7'b0111111;  // 63
				o_fan_3 = 7'b0010100;  // 20
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			432:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b100011101100001;  // 71.37890625
				o_wind_direction = 9'b000010101;  // 21
				o_fan_1 = 7'b1000110;  // 70
				o_fan_2 = 7'b0101100;  // 44
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0011001;  // 25
			end
			433:
			begin
				o_sweat_level = 7'b0011110;  // 30
				o_wind_magnitude = 15'b000001000010010;  // 2.0703125
				o_wind_direction = 9'b101011100;  // 348
				o_fan_1 = 7'b0000001;  // 1
				o_fan_2 = 7'b0000001;  // 1
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			434:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b001010000111000;  // 20.21875
				o_wind_direction = 9'b001100101;  // 101
				o_fan_1 = 7'b0000110;  // 6
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0001101;  // 13
				o_fan_6 = 7'b0010011;  // 19
			end
			435:
			begin
				o_sweat_level = 7'b0101101;  // 45
				o_wind_magnitude = 15'b001010010100111;  // 20.65234375
				o_wind_direction = 9'b001001100;  // 76
				o_fan_1 = 7'b0001110;  // 14
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000101;  // 5
				o_fan_6 = 7'b0010100;  // 20
			end
			436:
			begin
				o_sweat_level = 7'b0011001;  // 25
				o_wind_magnitude = 15'b010100011100000;  // 40.875
				o_wind_direction = 9'b001000011;  // 67
				o_fan_1 = 7'b0100000;  // 32
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000100;  // 4
				o_fan_6 = 7'b0100101;  // 37
			end
			437:
			begin
				o_sweat_level = 7'b1000100;  // 68
				o_wind_magnitude = 15'b101101010110111;  // 90.71484375
				o_wind_direction = 9'b000001000;  // 8
				o_fan_1 = 7'b1011011;  // 91
				o_fan_2 = 7'b1010101;  // 85
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111000;  // 56
			end
			438:
			begin
				o_sweat_level = 7'b0110111;  // 55
				o_wind_magnitude = 15'b000101010010110;  // 10.5859375
				o_wind_direction = 9'b011000100;  // 196
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110011;  // 51
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			439:
			begin
				o_sweat_level = 7'b0011101;  // 29
				o_wind_magnitude = 15'b011001001110110;  // 50.4609375
				o_wind_direction = 9'b001101011;  // 107
				o_fan_1 = 7'b0001011;  // 11
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0100100;  // 36
				o_fan_6 = 7'b0110000;  // 48
			end
			440:
			begin
				o_sweat_level = 7'b0001001;  // 9
				o_wind_magnitude = 15'b010011001110011;  // 38.44921875
				o_wind_direction = 9'b011001001;  // 201
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0001101;  // 13
				o_fan_4 = 7'b0100101;  // 37
				o_fan_5 = 7'b0011000;  // 24
				o_fan_6 = 7'b0000000;  // 0
			end
			441:
			begin
				o_sweat_level = 7'b1000011;  // 67
				o_wind_magnitude = 15'b011000001100110;  // 48.3984375
				o_wind_direction = 9'b010011111;  // 159
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1000001;  // 65
				o_fan_5 = 7'b1001001;  // 73
				o_fan_6 = 7'b0111010;  // 58
			end
			442:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b101110100100010;  // 93.1328125
				o_wind_direction = 9'b101100100;  // 356
				o_fan_1 = 7'b1011000;  // 88
				o_fan_2 = 7'b1011011;  // 91
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			443:
			begin
				o_sweat_level = 7'b1000000;  // 64
				o_wind_magnitude = 15'b010011011001110;  // 38.8046875
				o_wind_direction = 9'b011001011;  // 203
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111001;  // 57
				o_fan_4 = 7'b1000101;  // 69
				o_fan_5 = 7'b0111101;  // 61
				o_fan_6 = 7'b0110010;  // 50
			end
			444:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b101101111110100;  // 91.953125
				o_wind_direction = 9'b001110111;  // 119
				o_fan_1 = 7'b0000001;  // 1
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b1001110;  // 78
				o_fan_6 = 7'b1010000;  // 80
			end
			445:
			begin
				o_sweat_level = 7'b1000111;  // 71
				o_wind_magnitude = 15'b100001111000011;  // 67.76171875
				o_wind_direction = 9'b011000110;  // 198
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111100;  // 60
				o_fan_4 = 7'b1010011;  // 83
				o_fan_5 = 7'b1001000;  // 72
				o_fan_6 = 7'b0110010;  // 50
			end
			446:
			begin
				o_sweat_level = 7'b1001101;  // 77
				o_wind_magnitude = 15'b011000111110111;  // 49.96484375
				o_wind_direction = 9'b101011111;  // 351
				o_fan_1 = 7'b1000101;  // 69
				o_fan_2 = 7'b1001001;  // 73
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			447:
			begin
				o_sweat_level = 7'b0010000;  // 16
				o_wind_magnitude = 15'b010100001100100;  // 40.390625
				o_wind_direction = 9'b010010100;  // 148
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010010;  // 18
				o_fan_5 = 7'b0101000;  // 40
				o_fan_6 = 7'b0010101;  // 21
			end
			448:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b001110011111001;  // 28.97265625
				o_wind_direction = 9'b101000000;  // 320
				o_fan_1 = 7'b0110110;  // 54
				o_fan_2 = 7'b1000000;  // 64
				o_fan_3 = 7'b0111011;  // 59
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			449:
			begin
				o_sweat_level = 7'b1010010;  // 82
				o_wind_magnitude = 15'b011001010011101;  // 50.61328125
				o_wind_direction = 9'b011100001;  // 225
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1000011;  // 67
				o_fan_4 = 7'b1001010;  // 74
				o_fan_5 = 7'b0111000;  // 56
				o_fan_6 = 7'b0110010;  // 50
			end
			450:
			begin
				o_sweat_level = 7'b1001011;  // 75
				o_wind_magnitude = 15'b111111101110011;  // 127.44921875
				o_wind_direction = 9'b010101100;  // 172
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1100100;  // 100
				o_fan_5 = 7'b1101100;  // 108
				o_fan_6 = 7'b0111010;  // 58
			end
			451:
			begin
				o_sweat_level = 7'b0011100;  // 28
				o_wind_magnitude = 15'b001010010010101;  // 20.58203125
				o_wind_direction = 9'b001000110;  // 70
				o_fan_1 = 7'b0001111;  // 15
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000011;  // 3
				o_fan_6 = 7'b0010011;  // 19
			end
			452:
			begin
				o_sweat_level = 7'b1010011;  // 83
				o_wind_magnitude = 15'b011100111110101;  // 57.95703125
				o_wind_direction = 9'b001000011;  // 67
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b1001100;  // 76
			end
			453:
			begin
				o_sweat_level = 7'b1000101;  // 69
				o_wind_magnitude = 15'b011111000000001;  // 62.00390625
				o_wind_direction = 9'b011101001;  // 233
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1001010;  // 74
				o_fan_4 = 7'b1001110;  // 78
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			454:
			begin
				o_sweat_level = 7'b0011000;  // 24
				o_wind_magnitude = 15'b101001000011001;  // 82.09765625
				o_wind_direction = 9'b011111101;  // 253
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0010010;  // 18
				o_fan_3 = 7'b1001110;  // 78
				o_fan_4 = 7'b0111011;  // 59
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			455:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b100111001001100;  // 78.296875
				o_wind_direction = 9'b100110010;  // 306
				o_fan_1 = 7'b0000111;  // 7
				o_fan_2 = 7'b1000111;  // 71
				o_fan_3 = 7'b0111111;  // 63
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			456:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b011111111000000;  // 63.75
				o_wind_direction = 9'b011000010;  // 194
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0111001;  // 57
				o_fan_4 = 7'b1010000;  // 80
				o_fan_5 = 7'b1001000;  // 72
				o_fan_6 = 7'b0110010;  // 50
			end
			457:
			begin
				o_sweat_level = 7'b0101100;  // 44
				o_wind_magnitude = 15'b100001110111001;  // 67.72265625
				o_wind_direction = 9'b010110000;  // 176
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b1001110;  // 78
				o_fan_5 = 7'b1010000;  // 80
				o_fan_6 = 7'b0110100;  // 52
			end
			458:
			begin
				o_sweat_level = 7'b1100100;  // 100
				o_wind_magnitude = 15'b000011001011011;  // 6.35546875
				o_wind_direction = 9'b100000110;  // 262
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			459:
			begin
				o_sweat_level = 7'b1100010;  // 98
				o_wind_magnitude = 15'b101101110000110;  // 91.5234375
				o_wind_direction = 9'b100001001;  // 265
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000101;  // 69
				o_fan_3 = 7'b1011111;  // 95
				o_fan_4 = 7'b1001100;  // 76
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			460:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b110101111010100;  // 107.828125
				o_wind_direction = 9'b100101010;  // 298
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b1011011;  // 91
				o_fan_3 = 7'b1011111;  // 95
				o_fan_4 = 7'b0000011;  // 3
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			461:
			begin
				o_sweat_level = 7'b0100000;  // 32
				o_wind_magnitude = 15'b000110011011100;  // 12.859375
				o_wind_direction = 9'b010010000;  // 144
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000101;  // 5
				o_fan_5 = 7'b0001100;  // 12
				o_fan_6 = 7'b0000111;  // 7
			end
			462:
			begin
				o_sweat_level = 7'b0100110;  // 38
				o_wind_magnitude = 15'b001001011100111;  // 18.90234375
				o_wind_direction = 9'b010010001;  // 145
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000111;  // 7
				o_fan_5 = 7'b0010010;  // 18
				o_fan_6 = 7'b0001010;  // 10
			end
			463:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b001000000001110;  // 16.0546875
				o_wind_direction = 9'b010100010;  // 162
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0001010;  // 10
				o_fan_5 = 7'b0001111;  // 15
				o_fan_6 = 7'b0000100;  // 4
			end
			464:
			begin
				o_sweat_level = 7'b1000101;  // 69
				o_wind_magnitude = 15'b010111001111011;  // 46.48046875
				o_wind_direction = 9'b101011110;  // 350
				o_fan_1 = 7'b1000011;  // 67
				o_fan_2 = 7'b1000111;  // 71
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			465:
			begin
				o_sweat_level = 7'b0100001;  // 33
				o_wind_magnitude = 15'b100011001010011;  // 70.32421875
				o_wind_direction = 9'b011111000;  // 248
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0001001;  // 9
				o_fan_3 = 7'b1000001;  // 65
				o_fan_4 = 7'b0110111;  // 55
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			466:
			begin
				o_sweat_level = 7'b0010110;  // 22
				o_wind_magnitude = 15'b010011110001001;  // 39.53515625
				o_wind_direction = 9'b011110100;  // 244
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000010;  // 2
				o_fan_3 = 7'b0100011;  // 35
				o_fan_4 = 7'b0100000;  // 32
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			467:
			begin
				o_sweat_level = 7'b0101010;  // 42
				o_wind_magnitude = 15'b011000000000110;  // 48.0234375
				o_wind_direction = 9'b001101000;  // 104
				o_fan_1 = 7'b0001101;  // 13
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0100001;  // 33
				o_fan_6 = 7'b0101110;  // 46
			end
			468:
			begin
				o_sweat_level = 7'b1000000;  // 64
				o_wind_magnitude = 15'b000111010110000;  // 14.6875
				o_wind_direction = 9'b100110111;  // 311
				o_fan_1 = 7'b0110011;  // 51
				o_fan_2 = 7'b0111000;  // 56
				o_fan_3 = 7'b0110111;  // 55
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			469:
			begin
				o_sweat_level = 7'b0001110;  // 14
				o_wind_magnitude = 15'b010000001110011;  // 32.44921875
				o_wind_direction = 9'b000101110;  // 46
				o_fan_1 = 7'b0011111;  // 31
				o_fan_2 = 7'b0000111;  // 7
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0010111;  // 23
			end
			470:
			begin
				o_sweat_level = 7'b0101111;  // 47
				o_wind_magnitude = 15'b011010110001100;  // 53.546875
				o_wind_direction = 9'b100001010;  // 266
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0010111;  // 23
				o_fan_3 = 7'b0110101;  // 53
				o_fan_4 = 7'b0011101;  // 29
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			471:
			begin
				o_sweat_level = 7'b0010111;  // 23
				o_wind_magnitude = 15'b100111100111010;  // 79.2265625
				o_wind_direction = 9'b101100101;  // 357
				o_fan_1 = 7'b1000010;  // 66
				o_fan_2 = 7'b1000110;  // 70
				o_fan_3 = 7'b0000100;  // 4
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			472:
			begin
				o_sweat_level = 7'b0101001;  // 41
				o_wind_magnitude = 15'b110111111100001;  // 111.87890625
				o_wind_direction = 9'b001000111;  // 71
				o_fan_1 = 7'b1010100;  // 84
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0010100;  // 20
				o_fan_6 = 7'b1101001;  // 105
			end
			473:
			begin
				o_sweat_level = 7'b0101110;  // 46
				o_wind_magnitude = 15'b111010101010100;  // 117.328125
				o_wind_direction = 9'b011111001;  // 249
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0010010;  // 18
				o_fan_3 = 7'b1101101;  // 109
				o_fan_4 = 7'b1011010;  // 90
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			474:
			begin
				o_sweat_level = 7'b0100111;  // 39
				o_wind_magnitude = 15'b100110111111000;  // 77.96875
				o_wind_direction = 9'b000110001;  // 49
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b0001110;  // 14
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0111010;  // 58
			end
			475:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b010100010000110;  // 40.5234375
				o_wind_direction = 9'b010111001;  // 185
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000011;  // 3
				o_fan_4 = 7'b0100100;  // 36
				o_fan_5 = 7'b0100001;  // 33
				o_fan_6 = 7'b0000000;  // 0
			end
			476:
			begin
				o_sweat_level = 7'b0111111;  // 63
				o_wind_magnitude = 15'b101101110110011;  // 91.69921875
				o_wind_direction = 9'b001010000;  // 80
				o_fan_1 = 7'b1001111;  // 79
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b1000001;  // 65
				o_fan_6 = 7'b1011111;  // 95
			end
			477:
			begin
				o_sweat_level = 7'b1010100;  // 84
				o_wind_magnitude = 15'b000001001000110;  // 2.2734375
				o_wind_direction = 9'b010111111;  // 191
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			478:
			begin
				o_sweat_level = 7'b1100011;  // 99
				o_wind_magnitude = 15'b011000010111010;  // 48.7265625
				o_wind_direction = 9'b000010100;  // 20
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b1000001;  // 65
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0111010;  // 58
			end
			479:
			begin
				o_sweat_level = 7'b0000110;  // 6
				o_wind_magnitude = 15'b001000110100010;  // 17.6328125
				o_wind_direction = 9'b101011011;  // 347
				o_fan_1 = 7'b0001100;  // 12
				o_fan_2 = 7'b0010000;  // 16
				o_fan_3 = 7'b0000011;  // 3
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			480:
			begin
				o_sweat_level = 7'b1010011;  // 83
				o_wind_magnitude = 15'b101001010000010;  // 82.5078125
				o_wind_direction = 9'b011101110;  // 238
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1010100;  // 84
				o_fan_4 = 7'b1010110;  // 86
				o_fan_5 = 7'b0110011;  // 51
				o_fan_6 = 7'b0110010;  // 50
			end
			481:
			begin
				o_sweat_level = 7'b1001000;  // 72
				o_wind_magnitude = 15'b100000101011111;  // 65.37109375
				o_wind_direction = 9'b011101101;  // 237
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b1001101;  // 77
				o_fan_4 = 7'b1001111;  // 79
				o_fan_5 = 7'b0110011;  // 51
				o_fan_6 = 7'b0110010;  // 50
			end
			482:
			begin
				o_sweat_level = 7'b0110100;  // 52
				o_wind_magnitude = 15'b110000000011100;  // 96.109375
				o_wind_direction = 9'b000011001;  // 25
				o_fan_1 = 7'b1100001;  // 97
				o_fan_2 = 7'b1001101;  // 77
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000110;  // 70
			end
			483:
			begin
				o_sweat_level = 7'b0000111;  // 7
				o_wind_magnitude = 15'b100100010110111;  // 72.71484375
				o_wind_direction = 9'b010001011;  // 139
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0010111;  // 23
				o_fan_5 = 7'b1000111;  // 71
				o_fan_6 = 7'b0101111;  // 47
			end
			484:
			begin
				o_sweat_level = 7'b0110011;  // 51
				o_wind_magnitude = 15'b011100011010101;  // 56.83203125
				o_wind_direction = 9'b101100011;  // 355
				o_fan_1 = 7'b1001001;  // 73
				o_fan_2 = 7'b1001011;  // 75
				o_fan_3 = 7'b0110100;  // 52
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			485:
			begin
				o_sweat_level = 7'b0101001;  // 41
				o_wind_magnitude = 15'b001101000000001;  // 26.00390625
				o_wind_direction = 9'b100001001;  // 265
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110111;  // 55
				o_fan_3 = 7'b0111110;  // 62
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			486:
			begin
				o_sweat_level = 7'b0111001;  // 57
				o_wind_magnitude = 15'b001000100111010;  // 17.2265625
				o_wind_direction = 9'b001000111;  // 71
				o_fan_1 = 7'b0111000;  // 56
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110011;  // 51
				o_fan_6 = 7'b0111010;  // 58
			end
			487:
			begin
				o_sweat_level = 7'b0110101;  // 53
				o_wind_magnitude = 15'b011011110010111;  // 55.58984375
				o_wind_direction = 9'b001000100;  // 68
				o_fan_1 = 7'b1000111;  // 71
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b1001011;  // 75
			end
			488:
			begin
				o_sweat_level = 7'b1000110;  // 70
				o_wind_magnitude = 15'b010000111110010;  // 33.9453125
				o_wind_direction = 9'b010001000;  // 136
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110110;  // 54
				o_fan_5 = 7'b1000010;  // 66
				o_fan_6 = 7'b0111101;  // 61
			end
			489:
			begin
				o_sweat_level = 7'b1000000;  // 64
				o_wind_magnitude = 15'b011011110000100;  // 55.515625
				o_wind_direction = 9'b000100111;  // 39
				o_fan_1 = 7'b1001101;  // 77
				o_fan_2 = 7'b0111011;  // 59
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000011;  // 67
			end
			490:
			begin
				o_sweat_level = 7'b0101100;  // 44
				o_wind_magnitude = 15'b011110010000100;  // 60.515625
				o_wind_direction = 9'b001010101;  // 85
				o_fan_1 = 7'b1000011;  // 67
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111110;  // 62
				o_fan_6 = 7'b1010000;  // 80
			end
			491:
			begin
				o_sweat_level = 7'b0111111;  // 63
				o_wind_magnitude = 15'b000111110101000;  // 15.65625
				o_wind_direction = 9'b011010011;  // 211
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110110;  // 54
				o_fan_4 = 7'b0111001;  // 57
				o_fan_5 = 7'b0110101;  // 53
				o_fan_6 = 7'b0110010;  // 50
			end
			492:
			begin
				o_sweat_level = 7'b1011000;  // 88
				o_wind_magnitude = 15'b000111011111110;  // 14.9921875
				o_wind_direction = 9'b100101000;  // 296
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111000;  // 56
				o_fan_3 = 7'b0111000;  // 56
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			493:
			begin
				o_sweat_level = 7'b0110010;  // 50
				o_wind_magnitude = 15'b010101000010010;  // 42.0703125
				o_wind_direction = 9'b100100010;  // 290
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b1000010;  // 66
				o_fan_3 = 7'b1000101;  // 69
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			494:
			begin
				o_sweat_level = 7'b0011111;  // 31
				o_wind_magnitude = 15'b001101100000000;  // 27.0
				o_wind_direction = 9'b001111010;  // 122
				o_fan_1 = 7'b0000000;  // 0
				o_fan_2 = 7'b0000000;  // 0
				o_fan_3 = 7'b0000000;  // 0
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0010111;  // 23
				o_fan_6 = 7'b0010110;  // 22
			end
			495:
			begin
				o_sweat_level = 7'b1011000;  // 88
				o_wind_magnitude = 15'b001111111000100;  // 31.765625
				o_wind_direction = 9'b100101000;  // 296
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0111111;  // 63
				o_fan_3 = 7'b1000000;  // 64
				o_fan_4 = 7'b0110011;  // 51
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b0110010;  // 50
			end
			496:
			begin
				o_sweat_level = 7'b0001101;  // 13
				o_wind_magnitude = 15'b010100011101011;  // 40.91796875
				o_wind_direction = 9'b100110011;  // 307
				o_fan_1 = 7'b0000100;  // 4
				o_fan_2 = 7'b0100101;  // 37
				o_fan_3 = 7'b0100000;  // 32
				o_fan_4 = 7'b0000000;  // 0
				o_fan_5 = 7'b0000000;  // 0
				o_fan_6 = 7'b0000000;  // 0
			end
			497:
			begin
				o_sweat_level = 7'b1100011;  // 99
				o_wind_magnitude = 15'b011100011001101;  // 56.80078125
				o_wind_direction = 9'b001010101;  // 85
				o_fan_1 = 7'b1000010;  // 66
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0111101;  // 61
				o_fan_6 = 7'b1001110;  // 78
			end
			498:
			begin
				o_sweat_level = 7'b0111110;  // 62
				o_wind_magnitude = 15'b010111001011000;  // 46.34375
				o_wind_direction = 9'b000110111;  // 55
				o_fan_1 = 7'b1000110;  // 70
				o_fan_2 = 7'b0110011;  // 51
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110010;  // 50
				o_fan_5 = 7'b0110010;  // 50
				o_fan_6 = 7'b1000100;  // 68
			end
			499:
			begin
				o_sweat_level = 7'b1001000;  // 72
				o_wind_magnitude = 15'b110011001011000;  // 102.34375
				o_wind_direction = 9'b001111100;  // 124
				o_fan_1 = 7'b0110010;  // 50
				o_fan_2 = 7'b0110010;  // 50
				o_fan_3 = 7'b0110010;  // 50
				o_fan_4 = 7'b0110101;  // 53
				o_fan_5 = 7'b1011111;  // 95
				o_fan_6 = 7'b1011100;  // 92
			end
		endcase
	end
endmodule
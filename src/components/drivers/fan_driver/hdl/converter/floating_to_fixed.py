import csv
import traceback

from fxpmath import Fxp
from openpyxl import load_workbook

try:
    with open("../fan_lut.v", "w") as fan_lut, open("../slipstream/slipstream_lut.v", "w") as slipstream_lut:
        f = load_workbook(filename="fan_angles.xlsx", read_only=True, data_only=True)
        sheet = f[f.sheetnames[0]]

        fan_lut.write("// NOTE: This module is auto-generated, not intended to be human-readable.\n// It won't be formatted\n\n")
        fan_lut.write("module fan_lut (input [8:0] i_angle, output reg [31:0] o_fan_1, output reg [31:0] o_fan_2, output reg [31:0] o_fan_3, output reg [31:0] o_fan_4, output reg [31:0] o_fan_5, output reg [31:0] o_fan_6);\n")
        fan_lut.write("\n\talways @(i_angle)\n\tbegin\n\t\tcase (i_angle)\n")
        
        slipstream_lut.write("// NOTE: This module is auto-generated, not intended to be human-readable.\n// It won't be formatted\n\n")
        slipstream_lut.write("module slipstream_lut (input [8:0] i_angle, output reg [9:0] o_fan_1, output reg [9:0] o_fan_2, output reg [9:0] o_fan_3, output reg [9:0] o_fan_4, output reg [9:0] o_fan_5, output reg [9:0] o_fan_6);\n")
        slipstream_lut.write("\n\talways @(i_angle)\n\tbegin\n\t\tcase (i_angle)\n")

        angle = 0
        for row in sheet["J2":"O361"]:
            row_text_fan: str = f"\t\t\t{angle}:\n\t\t\t\tbegin\n"
            row_text_slipstream: str = f"\t\t\t{angle}:\n\t\t\t\tbegin\n"
            fan_id = 1
            for val in row:
                value = val.value
                value_inverse = 1 - value
                
                fixed_val_fan = Fxp(value, signed=False, n_int=2, n_frac=30)
                fixed_val_slipstream = Fxp(value_inverse, signed=False, n_int=2, n_frac=8)

                print(val.value, value, value_inverse)
                print(fixed_val_fan, fixed_val_fan.bin(), fixed_val_slipstream, fixed_val_slipstream.bin())
                
                row_text_fan += f"\t\t\t\t\to_fan_{fan_id} = 32'b{fixed_val_fan.bin()};  // {value}\n"
                row_text_slipstream += f"\t\t\t\t\to_fan_{fan_id} = 10'b{fixed_val_slipstream.bin()};  // {value_inverse}\n"
                
                fan_id += 1
                
            row_text_fan += "\t\t\t\tend\n"
            row_text_slipstream += "\t\t\t\tend\n"
            
            fan_lut.write(row_text_fan)
            slipstream_lut.write(row_text_slipstream)
            
            angle += 1
        
        fan_lut.write("\t\t\tdefault:\n\t\t\t\tbegin\n\t\t\t\t\to_fan_1 = 32'd0;  // 0\n\t\t\t\t\to_fan_2 = 32'd0;  // 0\n\t\t\t\t\to_fan_3 = 32'd0;  // 0\n\t\t\t\t\to_fan_4 = 32'd0;  // 0\n\t\t\t\t\to_fan_5 = 32'd0;  // 0\n\t\t\t\t\to_fan_6 = 32'd0;  // 0\n\t\t\t\tend")
        fan_lut.write("\n\t\tendcase\n\tend")
        fan_lut.write("\nendmodule")
        
        fixed_1 = Fxp(1, signed=False, n_int=2, n_frac=8)
        slipstream_lut.write(f"\t\t\tdefault:\n\t\t\t\tbegin\n\t\t\t\t\to_fan_1 = 10'b{fixed_1.bin()};  // 1\n\t\t\t\t\to_fan_2 = 10'b{fixed_1.bin()};  // 1\n\t\t\t\t\to_fan_3 = 10'b{fixed_1.bin()};  // 1\n\t\t\t\t\to_fan_4 = 10'b{fixed_1.bin()};  // 1\n\t\t\t\t\to_fan_5 = 10'b{fixed_1.bin()};  // 1\n\t\t\t\t\to_fan_6 = 10'b{fixed_1.bin()};  // 1\n\t\t\t\tend")
        slipstream_lut.write("\n\t\tendcase\n\tend")

        slipstream_lut.write("\nendmodule")

except:
    print(traceback.format_exc())
    input("press enter . . .")
finally:
    if f is not None:
        f.close()

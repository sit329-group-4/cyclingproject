// Use this to assert if a condition is true or false, displaying
// if the assert was successful or not.
task automatic assert_display(input condition);
	begin
		if (condition == 1)
		begin
			$display("Success");
		end
		else
		begin
			$display("Failed");
		end
	end
endtask

// Create a couple private integers for keeping track of tests
integer _assert_unit_successful_tests = 0;
integer _assert_unit_number_of_tests = 0;

// Same as assert_display but it also tracks the number of
// tests run and how many were successful.
task automatic assert_unit(input condition);
	begin
		_assert_unit_number_of_tests = _assert_unit_number_of_tests + 1;
		
		if (condition == 1)
		begin
			$display("Success");
			_assert_unit_successful_tests = _assert_unit_successful_tests + 1;
		end
		else
		begin
			$display("Failed");
		end
	end
endtask

// displays the test statistics then stops the simulation
task automatic assert_unit_stop();
	begin
		$display("");
		$display("Number of tests run: %d, Number of successful tests: %d", _assert_unit_number_of_tests, _assert_unit_successful_tests);
		$display("%d/%d", _assert_unit_successful_tests, _assert_unit_number_of_tests);
		$stop();
	end
endtask

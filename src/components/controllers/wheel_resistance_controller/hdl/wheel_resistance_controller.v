// ===== Wheel Resistance Controller =====
// --- Design Doc ---
//		Description:
//			The Wheel Resistance Controller will oversee calculating the new wheel resistance from
//			the road design (such as slopes) and the gear setting, as well as taking into consideration the wind resistance.
//
//		System Integrations:
//			Input Systems (If any):
//				Wind Controller – how much wind there is for it to consider
//			Output Systems (If any):
//				Wheel Resistance Driver – It will output how much resistance there should be
//				Power Detector – It will send the current resistance to the power detector so that it can calculate the power
//
//		Inputs (and what they mean):
//			Current Slope – This will be the current slope, likely as a signed number. Positive values will be
//							an upwards slope, negative numbers will be a downwards slope. It is a percentage.
//			Gear Setting – To adjust the resistance for different gears (assuming the bike isn’t using its gears?)
//			Wind Resistance – The wind resistance that it receives will be how much wind resistance there is
//								in the axis parallel to the bike and will be normalised for the riders cross
//								section according to their cross section
//			Weight Detector - This module will output the resulting weight to the wheel resistance module so it
//								can normalise the weight to performance ratio
//
//		Outputs:
//			Wheel Resistance – This will be the amount of wheel resistance to be applied to the wheel.
//								The units will be a half percentage.
//
//		Considerations:
//			This will probably all be combinational, the output may need to be gated
//		
//		Note: Looks like I accidentally deleted what the Q values of each number are. 

module wheel_resistance_controller 
#(
	parameter BASE_RESISTANCE = 18'sb0000001010_00000000,
		SLOPE_WEIGHTING = 18'sb00000000001_00000000,
		WIND_RESISTANCE_WEIGHTING = 18'sb0000000001_00000000,
		GEAR_SETTING_WEIGHTING = 18'sb0000000001_00000000,
		WEIGHT_WEIGHTING = 18'sb0000000001_00000000,
		GEAR_1_VALUE = 18'sd10 << 8,
		GEAR_2_VALUE = 18'sd20 << 8,
		GEAR_3_VALUE = 18'sd30 << 8,
		GEAR_4_VALUE = 18'sd40 << 8,
		GEAR_5_VALUE = 18'sd50 << 8
)
(
	input signed [7:0] i_current_slope,
	input [2:0] i_gear_setting,
	input signed [7:0] i_wind_resistance,
	input [16:0] i_weight,  // Q9.8
	
	output [7:0] o_wheel_resistance
);
	
	function automatic signed [17:0] gear_case(input reg [2:0] gear_input);
		case (gear_input)
			3'b000 : gear_case = GEAR_1_VALUE;
			3'b001 : gear_case = GEAR_2_VALUE;
			3'b010 : gear_case = GEAR_3_VALUE;
			3'b011 : gear_case = GEAR_4_VALUE;
			3'b100 : gear_case = GEAR_5_VALUE;
			default : gear_case = 18'sd0;
		endcase
	endfunction
	
	wire signed [17:0] w_gear_setting_quantized;
	
	wire signed [17:0] w_weight;
	assign w_weight = i_weight;
	
	wire signed [17:0] w_slope_shifted;
	wire signed [17:0] w_wind_resistance_shifted;
	assign w_slope_shifted = i_current_slope << 8;
	assign w_wind_resistance_shifted = i_wind_resistance << 8;
	
	// This will store the weighted versions of each input
	wire signed [17:0] w_slope_weighted;
	wire signed [17:0] w_gear_setting_weighted;
	wire signed [17:0] w_wind_resistance_weighted;
	wire signed [17:0] w_weight_weighted;
	
	wire signed [18:0] w_wheel_resistance;
	wire signed [10:0] w_wheel_resistance_clamped;
	
	
	// Now onto assigning all the wires with their combinational logic.
	
	assign w_gear_setting_quantized = gear_case(i_gear_setting);
	
	
	wheel_resistance_multiplier slope_multiplier
	(
		.i_value(w_slope_shifted),
		.i_multiplier(SLOPE_WEIGHTING),
		.o_result(w_slope_weighted)
	);
	
	wheel_resistance_multiplier gear_setting_multiplier
	(
		.i_value(w_gear_setting_quantized),
		.i_multiplier(GEAR_SETTING_WEIGHTING),
		.o_result(w_gear_setting_weighted)
	);
	
	wheel_resistance_multiplier wind_resistance_multiplier
	(
		.i_value(w_wind_resistance_shifted),
		.i_multiplier(WIND_RESISTANCE_WEIGHTING),
		.o_result(w_wind_resistance_weighted)
	);
	
	wheel_resistance_multiplier weight_multiplier
	(
		.i_value(w_weight),
		.i_multiplier(WEIGHT_WEIGHTING),
		.o_result(w_weight_weighted)
	);
	
	
	assign w_wheel_resistance = (BASE_RESISTANCE +
										w_slope_weighted +
										w_gear_setting_weighted +
										w_wind_resistance_weighted +
										w_weight_weighted);
	
	assign w_wheel_resistance_clamped = $unsigned(w_wheel_resistance[7:0]) >= 128 ? w_wheel_resistance[18:8] + 11'd1 : w_wheel_resistance[18:8];
	
	assign o_wheel_resistance = w_wheel_resistance_clamped < 0 ? 8'sd0 : (w_wheel_resistance_clamped > 100 ? 8'sd100 : w_wheel_resistance_clamped[7:0]);
	
endmodule
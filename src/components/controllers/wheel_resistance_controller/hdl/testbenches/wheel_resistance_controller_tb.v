`timescale 1ns/1ns

module wheel_resistance_controller_tb;
	// UUT Inputs
	wire signed [7:0] t_current_slope;
	wire [2:0] t_gear_setting;
	wire signed [7:0] t_wind_resistance;
	wire [17:0] t_weight;  // Q10.8

	// UUT Output
	wire [7:0] t_wheel_resistance;
	
	// Expected Wheel Resistance for comparison. Calculated in python
	wire [7:0] t_expected_wheel_resistance;
	
	// The current test case being tested
	reg [31:0] t_test_case = 0;
	// The total number of test cases
	wire [31:0] t_total_cases;
	
	wheel_resistance_controller UUT
	(
		.i_current_slope(t_current_slope),
		.i_gear_setting(t_gear_setting),
		.i_wind_resistance(t_wind_resistance),
		.i_weight(t_weight),
		.o_wheel_resistance(t_wheel_resistance)
	);
	
	// Auto-generated test stimulus
	test_stimulus test_stimulus
	(
		.i_test_case(t_test_case),
		.o_current_slope(t_current_slope),
		.o_gear_setting(t_gear_setting),
		.o_wind_resistance(t_wind_resistance),
		.o_weight(t_weight),
		.o_expected_wheel_resistance(t_expected_wheel_resistance),
		.o_total_cases(t_total_cases)
	);
	
	initial
	begin : tb_block
		integer correct_cases;
		
		correct_cases = 0;
		
		$dumpfile("out.vcd");
		$dumpvars(1, wheel_resistance_controller_tb);
		
		#2
		
		$display("Beginning Tests...");
		
		for (t_test_case = 0; t_test_case < t_total_cases; t_test_case = t_test_case + 1)
		begin
			#6;
			if (t_expected_wheel_resistance == t_wheel_resistance)
			begin
				correct_cases = correct_cases + 1;
			end
			else
			begin
				$display("Test case failed: %d", t_test_case);
			end
		end
		
		#10;
		
		$display("All cases tested!");
		$display("Total Number of Cases: %d, Number of Successful Cases: %d", t_total_cases, correct_cases);
		$display("%d/%d", correct_cases, t_test_case);
		
		$stop;
	end

endmodule
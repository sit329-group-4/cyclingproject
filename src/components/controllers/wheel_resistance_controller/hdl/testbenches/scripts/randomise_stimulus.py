import random
import traceback

from fxpmath import Fxp

"""
Template for the file that this will generate...

module test_stimulus
(
    input [31:0] i_test_case,
    
    output reg signed [7:0] o_current_slope,
    output reg [2:0] o_gear_setting,
    output reg signed [7:0] o_wind_resistance,
    output reg [17:0] i_weight,
    
    output reg [7:0] o_expected_wheel_resistance,
    
    output reg [31:0] o_max_value = max
);

    always @(i_test_case)
    begin
        case (i_test_case)
            0:
                begin
                    blah blah blah
                end
        endcase
    end
endmodule
"""

total_cases = 10000

gear_setting_dict = {
    0: 10,
    1: 20,
    2: 30,
    3: 40,
    4: 50
}
try:
    with open("../test_stimulus.v", "w") as f:
        header = \
f"""// Auto-generated test stimulus. This hasn't been formatted for human readability.

module test_stimulus
(
\tinput [31:0] i_test_case,
\t
\toutput reg signed [7:0] o_current_slope,
\toutput reg [2:0] o_gear_setting,
\toutput reg signed [7:0] o_wind_resistance,
\toutput reg [17:0] o_weight,  // Q10.8
\t
\toutput reg [7:0] o_expected_wheel_resistance,
\t
\toutput reg [31:0] o_total_cases = {total_cases}
);
\t
\talways @(i_test_case)
\tbegin
\t\tcase (i_test_case)
"""

        footer = \
"""\t\tendcase
\tend
\t
endmodule
"""

        f.write(header)

        for index in range(total_cases):
            print(f"Generating case {index}")
            current_slope = Fxp(random.uniform(-100, 100), signed=True, n_int=7, n_frac=0)
            gear_setting = Fxp(random.randint(0, 4), signed=False, n_int=3, n_frac=0)
            wind_resistance = Fxp(random.uniform(-100, 100), signed=True, n_int=7, n_frac=0)
            weight = Fxp(random.uniform(0, 300), signed=False, n_int=9, n_frac=8)

            expected: Fxp = 10 + (current_slope * 1) + (wind_resistance * 1) + (weight * 1) + (gear_setting_dict[int(gear_setting.astype(int))] * 1)

            expected_temp = expected.deepcopy()

            if float(expected.astype(float)) > 100:
                expected(100)
            elif float(expected.astype(float)) < 0:
                expected(0)
            else:
                # We need to seperate out the integer part from the fractional part
                # Then we need to check if the fractional part is greater or equal to 1/2
                # If it is then we increment the integer part and turn it back into a fixed point
                expected_int_str = str(int(expected.astype(int)))
                expected_fract_string = str(expected)[len(expected_int_str):]

                if len(expected_fract_string) > 1:
                    if int(expected_fract_string[1]) >= 5:
                        expected_int_str = str(int(expected_int_str) + 1)
                        print(f"Expected ({expected}) rounded to {expected_int_str}")

                expected(int(expected_int_str))

            expected.resize(signed=False, n_int=8, n_frac=0)

            template = \
f"""\t\t\t{index}:
\t\t\t\tbegin
\t\t\t\t\to_current_slope = 8'sb{current_slope.bin()};  // {str(current_slope)}
\t\t\t\t\to_gear_setting = 3'b{gear_setting.bin()};  // {str(gear_setting)}
\t\t\t\t\to_wind_resistance = 8'sb{wind_resistance.bin()};  // {str(wind_resistance)}
\t\t\t\t\to_weight = 17'b{weight.bin()};  // {str(weight)}
\t\t\t\t\to_expected_wheel_resistance = 8'b{expected.bin()};  // {str(expected)}, {str(expected_temp)}
\t\t\t\tend
"""
            f.write(template)

        f.write(footer)
except:
    traceback.print_exc()
    input("Press enter to continue . . .")
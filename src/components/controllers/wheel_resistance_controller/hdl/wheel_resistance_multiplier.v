module wheel_resistance_multiplier
(
	input signed [17:0] i_value,  // signed Q10.8
	input signed [17:0] i_multiplier,
	
	output reg signed [17:0] o_result
);
	
	function automatic signed [35:0] multiply(input signed [17:0] fi_value, input signed [17:0] fi_multiplier);
		begin
			multiply = fi_value * fi_multiplier;
		end
	endfunction
	
	function automatic signed [17:0] clamp(input signed [35:0] fi_value);
		begin
			clamp = {fi_value[35], fi_value[24:8]};
		end
	endfunction
	
	always @(*)
	begin
		o_result = clamp(multiply(i_value, i_multiplier));
	end
	
endmodule
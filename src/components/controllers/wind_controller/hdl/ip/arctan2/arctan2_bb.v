
module arctan2 (
	clk,
	areset,
	x,
	y,
	q);	

	input		clk;
	input		areset;
	input	[24:0]	x;
	input	[24:0]	y;
	output	[10:0]	q;
endmodule

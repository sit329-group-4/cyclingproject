import csv
import traceback
import math

from fxpmath import Fxp

def rad_to_deg_range_lower(deg):
    return Fxp(math.radians(deg - 0.5), signed=True, n_int=3, n_frac=8)
    
def rad_to_deg_range_upper(deg):
    return Fxp(math.radians(deg + 0.5), signed=True, n_int=3, n_frac=8)
    

try:
    with open("../radians_lut.v", "w") as radians_lut:
        radians_lut.write("// NOTE: This module is auto-generated, not intended to be human-readable.\n// It won't be formatted\n\n")
        radians_lut.write("module radians_lut (input [8:0] i_angle, input signed [10:0] i_radians, output reg signed [15:0] o_radians, output reg [8:0] o_angle);\n")
        radians_lut.write("\n\talways @(i_angle)\n\tbegin\n\t\tcase (i_angle)\n")

        for degree in range(360):
            row_text_fan: str = f"\t\t\t{degree}:\n\t\t\t\tbegin\n"
            if degree > 180:
                degree = degree - 360
            
            fixed_val_fan = Fxp(math.radians(degree), signed=True, n_int=2, n_frac=13)
                
            row_text_fan += f"\t\t\t\t\to_radians = 16'b{fixed_val_fan.bin()};  // {math.radians(degree)}, {fixed_val_fan}\n"
                
            row_text_fan += "\t\t\t\tend\n"
            
            radians_lut.write(row_text_fan)
        
        radians_lut.write("\t\t\tdefault:\n\t\t\t\tbegin\n\t\t\t\t\to_radians = 16'd0;  // 0\n\t\t\t\tend")
        radians_lut.write("\n\t\tendcase\n\tend")
        
        # now to convert radians back to degrees. Need to use if ranges for this...
        radians_lut.write("\n\talways @(i_radians)\n\tbegin\n")
        for deg in range(-180, 180):
            radians_lut.write(f"\t\tif (i_radians >= 11'b{rad_to_deg_range_lower(deg).bin()} && i_radians < 11'b{rad_to_deg_range_upper(deg).bin()})\n\t\tbegin  // {rad_to_deg_range_lower(deg)} <= i_radians < {rad_to_deg_range_upper(deg)}\n")
            if deg < 0:
                deg = deg + 360
            radians_lut.write(f"\t\t\to_angle = {deg};\n")
            radians_lut.write("\t\tend\n\t\telse\n")
        
        radians_lut.write("\t\tbegin\n\t\t\to_angle = 0;\n\t\tend\n\tend\n") 
        
        radians_lut.write("\nendmodule")

except:
    print(traceback.format_exc())
    input("press enter . . .")
finally:
    if radians_lut is not None:
        radians_lut.close()

`timescale 1ns/1ns

module wind_controller_tb;
	
	reg t_clk = 1'b0;
	
	// Q7.8
	reg [14:0] t_speed = 0;
	// Q7.8. Range 0.0 - 100.0
	reg [14:0] t_wind_magnitude = 0;
	// Range 0 - 359
	reg [8:0] t_wind_direction = 0;
	
	// Q7.8. Range 0.0 - 100.0
	wire [14:0] t_normalised_magnitude;
	// Range 0 - 359
	wire [8:0] t_normalised_direction;
	
	wind_controller UUT
	(
		.i_clk(t_clk),
		.i_speed(t_speed),
		.i_wind_magnitude(t_wind_magnitude),
		.i_wind_direction(t_wind_direction),
		.o_normalised_magnitude(t_normalised_magnitude),
		.o_normalised_direction(t_normalised_direction)
	);
	
	always #1 t_clk = ~t_clk;
	
	initial
	begin : tb_block
		
		$dumpfile("out.vcd");
		$dumpvars(1, wind_controller_tb);
		
		t_wind_direction = 90;
		t_wind_magnitude = 100 << 8;
		t_speed = 100 << 8;
		
		#500;
		
		$stop;
	end
	
endmodule
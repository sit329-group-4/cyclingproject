// ===== Wind Controller =====
// --- Design Doc ---
//		Description:
//			The wind controller works out the wind effects for both wheel resistance and emulated wind by fans based on
//			the bike speed, wind gusts, and slipstreams.
//
//		System Integrations:
//			Input Systems (If any):
//				(Potentially could be integrated with the HPS with a WiFi game client)
//				Wind Emulation System (For Testing) – This will give a wind strength with a direction and a
//														percent blocked (positive or negative, where positive is how
//														much of the + axis from the middle of the bike is blocked and negative
//														will be the opposite) and will be simulated with potentially some
//														random numbers for now as there is no real way to emulate
//														wind for testbenches in a nice and clean way
//				Body Position Detector – Will use the body cross section to work out how the wind resistance will affect
//											the body. That will be the majority of the wind resistance. See the “Considerations”
//			Output Systems (If any):
//				Fan Driver – the wind controller will give the fan driver a direction and strength for the wind.
//
//		Inputs (and what they mean):
//			Bike Speed – The system needs the bike speed to calculate the wind resistance and wind speed from
//							forward movement (Calculate what the relative zero wind speed is)
//			Wind Magnitude and Direction – The wind speed and direction in the game world
//			Wind blocking range & offset – will give the range and offset of the blocking object(s) for the wind.
//											If there is an object that blocks all of the wind, there will be a 0% range
//											with a 0 offset. Otherwise it gives the areas that the wind can affect the bike. 
//
//		Outputs:
//			Normalised Wind Resistance – This is the amount of wind resistance normalised to be parallel to the bike
//
//		Considerations
//			The output stage for the wind resistance is likely to either be extremely expensive in terms of path length,
//			reducing the max clock speed or else we can pipeline it in which case it will take more clock cycles for
//			results to come through.
//			Really the only option is to pipeline it and then changes to wind will be delayed by a couple cycles which
//			means we can just eat it or we can delay everything else by the same amount, or else we can make it only
//			change output every couple of cycles.
//			Ideally we should pipeline.
//			The slipstream vs everything else may be a bit harder because slipstreams usually cause a sorta deadzone of air that
//			follows the leading object which cancels out the headwind due to speed. Do we want that dealt with here or in
//			the fan driver? Or do we want to deal with that at all?
//			The majority of the wind resistance will be based on the body cross section rather than the bike
//			as the bike is a very small cross section as compared to the body cross section. In this case we can
//			likely approximate the cross section simply using a multiplier based on the position of the body which will
//			massively simplify things as we can basically use enumerations of body positions (Verilog doesn’t really have
//			the idea of enums like VHDL does so we can abstract that out to actual integer values)

// The implementation is a little messed up. Gonna reimplement closer to what was intended
// Edit: I couldn't get it to work right but oh well. I don't have time so this will have to do
// 			I tried to use the algorithm from https://math.stackexchange.com/questions/1365622/adding-two-polar-vectors
// 			using CORDIC to do the fixed point operations but it doesn't currently work and I don't know why.

// NOTE: Needs to have a front wind resistance added

module wind_controller
(
	input i_clk,
	
	// The bikes speed, it will be measured in km/h.
	// Q7.8 to fill up 32 bits and to give a good amount of precision and enough range in the integer part
	input [14:0] i_speed,
	
	// Q7.8. Range 0.0 - 100.0
	input [14:0] i_wind_magnitude,
	// Range 0 - 359
	input [8:0] i_wind_direction,
	
	// Q7.8. Range 0.0 - 100.0
	output reg [14:0] o_normalised_magnitude = 15'd0,
	// Range 0 - 359
	output [8:0] o_normalised_direction = 9'd0
);
	
	// Q3.13
	wire signed [15:0] w_wind_direction_corrected;
	
	// Q2.8
	wire signed [9:0] w_angle_sin;
	wire signed [9:0] w_angle_cos;
	
	// Q7.8. This is the speed clamped to 100 max
	reg signed [14:0] r_normalised_speed = 0;
	
	// Q9.16. these are the inputs to arctan2
	reg signed [24:0] r_r1_r2_cos = 0;
	reg signed [24:0] r_r2_sin = 0;
	
	// Q3.8. The angle of the resultant vector
	wire signed [10:0] w_angle_radians;
	// Range 0 - 359
	wire [8:0] w_angle;
	
	// Q14.16 + Q14.16 + Q18.32 = Q18.32
	reg [49:0] r_magnitude_pre_sqrt = 0;
	wire [49:0] w_magnitude_sqrtd;
	reg [49:0] r_magnitude_sqrtd = 0;
	
	// Sqrt related signals
	wire w_busy;
	wire w_start;
	
	reg r_busy = 1;
	
	reg r_first_start = 1;
	
	assign o_normalised_direction = w_angle;
	
	assign w_start = ~w_busy || r_first_start;
	
	radians_lut radians_lut
	(
		.i_angle(i_wind_direction),
		.o_radians(w_wind_direction_corrected),
		.i_radians(w_angle_radians),
		.o_angle(w_angle)
	);
	
	sin_cos sin_cos
	(
		.clk(i_clk),
		.areset(1'b0),
		.a(w_wind_direction_corrected),
		.s(w_angle_sin),
		.c(w_angle_cos)
	);
	
	arctan2 arctan2
	(
		.clk(i_clk),
		.areset(1'b0),
		.x(r_r2_sin),
		.y(r_r1_r2_cos),
		.q(w_angle_radians)
	);
	
	sqrt
	#(
		.WIDTH(50),
		.FBITS(32)
	)
	sqrt
	(
		.clk(i_clk),
		.start(w_start),
		.busy(w_busy),
		.valid(),
		.rad(r_magnitude_pre_sqrt),
		.root(w_magnitude_sqrtd),
		.rem()
	);
	
	// Clamps the speed to 100
	always @(*)
	begin
		if (i_speed >= 100)
		begin
			r_normalised_speed = 15'h6400;
		end
		else
		begin
			r_normalised_speed = i_speed;
		end
	end
	
	// This block calculates the parameters to arctan2
	always @(*)
	begin : param_block
		// Q7.8 * Q2.8 = Q9.16
		reg signed [24:0] r_r2_cos;
		
		r_r2_cos = i_wind_magnitude * w_angle_cos;
		r_r1_r2_cos = r_normalised_speed + r_r2_cos;
		
		r_r2_sin = i_wind_magnitude * w_angle_sin;
	end
	
	// This block calculates the magnitude
	always @(*)
	begin : mag_block
		// Q2.8 * Q7.8 * Q7.8 * Q2.8 = Q18.32
		reg [49:0] r_sub_expression_mult;
		// Q7.8 * Q7.8 = Q14.16
		reg [29:0] r_mag_1;
		reg [29:0] r_mag_2;
		
		r_mag_1 = r_normalised_speed ** 2;
		r_mag_2 = i_wind_magnitude ** 2;
		
		// 2*speed*wind_magnitude*cos(angle)
		r_sub_expression_mult = (10'd2 << 8) * r_normalised_speed * i_wind_magnitude * w_angle_cos;
		
		r_magnitude_pre_sqrt = (r_mag_1 << 16) + (r_mag_2 << 16) + r_sub_expression_mult;
	end
	
	always @(posedge i_clk)
	begin
		r_first_start <= 0;
	
		r_busy <= w_busy;
		
		if (r_busy == 1 && w_busy == 0)
		begin
			r_magnitude_sqrtd <= w_magnitude_sqrtd;
		end
	end
	
	always @(*)
	begin
		// Q18.32 -> Q7.8. Right shift it 24 positions
		if ((r_magnitude_sqrtd >> 32) > 100)
		begin
			o_normalised_magnitude = 100 << 8;
		end
		else
		begin
			o_normalised_magnitude = r_magnitude_sqrtd >> 24;
		end
	end
	
endmodule 

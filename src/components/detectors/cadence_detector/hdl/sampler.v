// Sampler circuit
// It takes in pulses and counts them up in certain amounts of time spans, outputting the amount
// found in the previous time span

module sampler
#(
	parameter CLK_SPEED_HZ = 50_000_000,
	parameter real SAMPLE_PERIOD = 0.5 // seconds
)
(
	input i_clk,
	
	input i_pulse,
	
	output reg [9:0] o_pulse_count = 1'd0,
	output reg o_output_updated = 1'b0
);

	// Convert the sample period to a counter value. That value can then be used to reset the counter and hopefully
	// get the pulse count for each of those time spans.
	localparam real SAMPLE_FREQUENCY = 1 / SAMPLE_PERIOD;
	localparam integer SAMPLE_RESET_VALUE = CLK_SPEED_HZ / SAMPLE_FREQUENCY;

	// Specifies when a time span has completed
	wire w_count_complete;
	
	// Used to make the cadence update as the pulses come in from the very start
	reg r_initial_start = 1'b1;
	
	reg [31:0] r_counter = 32'd0;
	reg [9:0] r_pulse_count = 10'd0;
	
	assign w_count_complete = r_counter < SAMPLE_RESET_VALUE ? 1'b0 : 1'b1;
	
	always @(posedge i_clk)
	begin
		o_output_updated <= 1'b0;
	
		// Hold the existing values in case there are no changes
		r_pulse_count <= r_pulse_count;
		o_pulse_count <= o_pulse_count;
	
		r_counter <= r_counter + 32'd1;
		
		// Initial start loading of the pulse count
		if (r_initial_start == 1'b1)
		begin
			o_pulse_count <= r_pulse_count;
		end
	
		if (w_count_complete == 1'b0)
		begin
			if (i_pulse == 1'b1)
			begin
				r_pulse_count <= r_pulse_count + 1;
			end
		end
		else
		begin
			r_counter <= 32'd0;
			o_pulse_count <= r_pulse_count;
			r_pulse_count <= 10'd0;
			r_initial_start <= 1'b0;
			o_output_updated <= 1'b1;
		end
	end

endmodule
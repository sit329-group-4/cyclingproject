`timescale 10ns/10ns

module cadence_detector_tb;
	
	localparam CLK_SPEED = 50_000_000;
	localparam INCREMENTS_PER_SECOND = 50_000_000;
	
	reg t_clk = 0;
	reg t_cadence_pulse = 0;
	
	wire [20:0] t_cadence;
	
	cadence_detector #(CLK_SPEED, INCREMENTS_PER_SECOND) UUT
	(
		.i_clk(t_clk),
		.i_cadence_pulse(t_cadence_pulse),
		.o_cadence(t_cadence)
	);
	
	always #1 t_clk = ~t_clk;
	
//	always
//	begin
//		#1 t_cadence_pulse = 1;
//		#1 t_cadence_pulse = 0;
//		#18;
//	end
	
	initial
	begin
		$dumpfile("out.vcd");
		$dumpvars(1, cadence_detector_tb);
	
		#4000;
		
		$stop;
	end
	
endmodule
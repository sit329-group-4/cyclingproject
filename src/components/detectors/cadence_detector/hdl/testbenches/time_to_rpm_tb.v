`timescale 1ns/1ns

module time_to_rpm_tb;
	
	reg t_clk = 1'b0;
	reg [11:0] t_time = 12'd0;
	
	wire [31:0] t_rpm; //Q20.12
	wire t_complete;
	
	time_to_rpm uut
	(
		.i_clk(t_clk),
		.i_time(t_time),
		.o_rpm(t_rpm),
		.o_complete(t_complete)
	);
	
	always #1 t_clk = ~t_clk;
	
	initial
	begin
		$dumpfile("out.vcd");
		$dumpvars(1, time_to_rpm_tb);
		
		@(posedge t_complete);
		t_time = 1000;
		@(posedge t_complete);
		t_time = 500;
		@(posedge t_complete);
		t_time = 250;
		@(posedge t_complete);
		t_time = 2000;
		@(posedge t_complete);
		t_time = 3500;
		@(posedge t_complete);
		
		#10;
		
		$stop;
	end
	
endmodule
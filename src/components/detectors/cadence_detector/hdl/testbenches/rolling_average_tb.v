`timescale 1ns/1ns

module rolling_average_tb;
	
	reg t_clk = 1'b0;
	reg t_new_value = 1'b0;
	reg [11:0] t_value = 12'd0;
	
	wire [11:0] t_average;
	
	rolling_average UUT
	(
		.i_clk(t_clk),
		.i_new_value(t_new_value),
		.i_value(t_value),
		.o_average(t_average)
	);
	
	always #1 t_clk = ~t_clk;
	
	initial
	begin
		$dumpfile("out.vcd");
		$dumpvars(1, rolling_average_tb);
	
		t_value = 12'd10;
		t_new_value = 1'b1;
		
		#(12*2)
		
		$stop;
	end
	
endmodule
`timescale 10ns/10ns

module pulse_timer_tb;
	
	reg t_clk = 1'b0;
	reg t_pulse = 1'b0;
	
	wire [11:0] t_pulse_time;
	wire t_new_value;
	
	wire [11:0] t_pulse_time_1;
	wire t_new_value_1;
	
	wire [11:0] t_pulse_time_2;
	wire t_new_value_2;
	
	reg r_cur_under_test = 0;
	
	assign t_pulse_time = r_cur_under_test == 0 ? t_pulse_time_1 : t_pulse_time_2;
	assign t_new_value = r_cur_under_test == 0 ? t_new_value_1 : t_new_value_2;
	
	pulse_timer #(50_000_000, 50_000_000) UUT
	(
		.i_clk(t_clk),
		.i_pulse(t_pulse),
		.o_pulse_time(t_pulse_time_1),
		.o_new_value(t_new_value_1)
	);
	
	pulse_timer #(50_000_000, 25_000_000) UUT2
	(
		.i_clk(t_clk),
		.i_pulse(t_pulse),
		.o_pulse_time(t_pulse_time_2),
		.o_new_value(t_new_value_2)
	);
	
	always #1 t_clk = ~t_clk;
	
	task automatic pulse_input();
		begin
			t_pulse = 1'b1;
			#1;
			t_pulse = 1'b0;
			#1;
		end
	endtask
	
	task automatic assert_display(input condition);
		begin
			if (condition == 1)
			begin
				$display("Success");
			end
			else
			begin
				$display("Failed");
			end
		end
	endtask
	
	integer i;
	
	initial
	begin
		$dumpfile("out.vcd");
		$dumpvars(1, pulse_timer_tb);
		
		// Sync to the clocks rising edge
		@(posedge t_clk);
		
		#2;
		
		// send a pulse to reset the counter
		pulse_input();
		
		$display("Starting Tests");
		$display("Test 1: Confirming the timing for one cycle is accurate");
		
		// Wait for 10 full cycles of the clock (equivelent to 10 milliseconds between pulses)
		#20;
		// Now pulse the input
		pulse_input();
		
		$display("pulse_time should be 10...");
		assert_display(t_pulse_time == 10);
		
		$display("Test 2: Confirming the second cycle times correctly");
		
		// Wait for 30 full cycles
		#60;
		pulse_input();
		$display("pulse_time should be 10...");
		assert_display(t_pulse_time == 30);
		
		// Now we will test with a different time amount to make sure it still tracks...
		
		r_cur_under_test = 1;
		
		pulse_input();
		
		$display("Test 3: Testing at a different speed");
		// Wait for 20 full cycles
		#40;
		pulse_input();
		$display("pulse_time should be 10...");
		assert_display(t_pulse_time == 10);
		
		$display("Test 4: Testing again at the same differing speed");
		// Wait for 16 full cycles
		#32;
		pulse_input();
		$display("pulse_time should be 8...");
		assert_display(t_pulse_time == 8);
		
		// Now we want to test the counting continues to work correctly over a long time
		r_cur_under_test = 0;
		
		for (i = 0; i < 100; i = i + 1)
		begin
			$display("Test 5 Iteration %d: Confirming the timing for one cycle is accurate", i+1);
			
			// Wait for 10 full cycles of the clock (equivelent to 10 milliseconds between pulses)
			#20;
			// Now pulse the input
			pulse_input();
			
			$display("pulse_time should be 10...");
			assert_display(t_pulse_time == 10);
		end
		
		#10;
		
		$stop;
	end
	
	
endmodule
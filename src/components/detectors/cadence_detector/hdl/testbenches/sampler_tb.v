// Testbench for the sampler circuit.
//
// Needs to test that the sampler correctly counts the amount of pulses in its time span without missing any counts

`timescale 1ns/1ns

module sampler_tb;

	localparam CLK_SPEED_HZ = 500_000_000;
	localparam real SAMPLE_PERIOD = 0.0000001;  // seconds

	// Inputs
	reg t_clk = 1'b0;
	reg t_pulse = 1'b0;
	
	// Outputs
	wire [9:0] t_pulse_count = 0;

	// Unit Under Test (UUT)
	sampler #(CLK_SPEED_HZ, SAMPLE_PERIOD) uut
	(
		.i_clk(t_clk),
		.i_pulse(t_pulse),
		.o_pulse_count(t_pulse_count)
	);
	
	always #1 t_clk = ~t_clk;
	
	integer i;
	
	initial begin
		$dumpfile("out.vcd");
		$dumpvars(1, sampler_tb);
		
		#10
		
		for (i = 0; i < 100; i = i + 1)
		begin
			#1
			@(negedge t_clk)
			t_pulse = ~t_pulse;
			#2 t_pulse = ~t_pulse;
		end
		
		#10
		
		$stop;
	end
	
endmodule
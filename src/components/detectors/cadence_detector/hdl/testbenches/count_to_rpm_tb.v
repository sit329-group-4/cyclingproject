//

`timescale 1ns/1ns

module count_to_rpm_tb;

	localparam real SAMPLE_PERIOD = 1;  // seconds
	
	reg [9:0] t_count;
	wire [9:0] t_rpm;
	
	count_to_rpm #(SAMPLE_PERIOD) UUT
	(
		.i_count(t_count),
		.o_rpm(t_rpm)
	);
	
	initial begin
		$dumpfile("out.vcd");
		$dumpvars(1, count_to_rpm_tb);
		
		t_count = 10;
		
		#10
		
		$stop;
	end
	
endmodule
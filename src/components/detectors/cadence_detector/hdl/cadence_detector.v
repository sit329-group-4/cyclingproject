// ===== Cadence Detector =====
// --- Design Doc ---
//		Description:
//			The cadence detection will take some kind of rotary encoded input representing the pedal rotation to be converted to a cadence value
//
//		System Integrations:
//			Input Systems (If any):
//			Output Systems (If any):
//
//		Inputs (and what they mean):
//			Pedal Cadence – the pedal rotations represented as a single pulse every full rotation of the pedals.
//
//		Outputs:
//			Cadence – the cadence of the pedalling as an unsigned integer representing the RPM
//
//		Considerations 
//			• There could be an issue deciding if this should be combinatorial and have a very long path from it to wheel resistance controller back again, or it could be pipelined or delayed. It entirely depends on the level of complexity we want to achieve.
//
// --- Personal Notes ---
//		So I think that the way I will approach this is that I will create a sampler which will record samples for approx 500 ms then update
//		its output which will then be sent out.
//		It will also likely need debouncing and some edge detection on the cadence input
//		The cadence is a pulse train for every full rotation which should hopefully make the maths not too hard.
//		I may change the sample time depending on if that will simplify the logic (maths using powers of 2 is always more efficient with
//		binary systems).

module cadence_detector
#(
	parameter CLK_SPEED = 50_000_000,
	parameter INCREMENTS_PER_SECOND = 1000
)
(
	input i_clk,
	
	input i_cadence_pulse,
	
	output [20:0] o_cadence // In RPM Q9.12
);
	
	wire w_conditioned_pulse;
	
	wire [11:0] w_pulse_time;
	wire w_new_pulse_time;
	wire [11:0] w_average_pulse_time;
	
	pulse_conditioner pulse_conditioner
	(
		.i_clk(i_clk),
		.i_pulse_train(i_cadence_pulse),
		.o_pulse(w_conditioned_pulse)
	);
	
	// Swapped from counting pulses per second to time between pulses because cadence is likely pretty slow
	// and would be a bit inaccurate to do that way
	pulse_timer #(CLK_SPEED, INCREMENTS_PER_SECOND) pulse_timer
	(
		.i_clk(i_clk),
		.i_pulse(w_conditioned_pulse),
		.o_pulse_time(w_pulse_time),
		.o_new_value(w_new_pulse_time)
	);
	
	rolling_average rolling_average
	(
		.i_clk(i_clk),
		.i_new_value(w_new_pulse_time),
		.i_value(w_pulse_time),
		.o_average(w_average_pulse_time)
	);
	
	time_to_rpm time_to_rpm
	(
		.i_clk(i_clk),
		.i_time(w_average_pulse_time),
		.o_rpm(o_cadence)
	);

endmodule
module time_to_rpm
#(
	parameter CLK_SPEED = 50_000_000,
	parameter INCREMENTS_PER_SECOND = 1000
)
(
	input i_clk,
	
	input [11:0] i_time,
	
	output reg [20:0] o_rpm, // //Q9.12 
	output o_complete
);

	localparam INCREMENTS_PER_MINUTE = 60 * INCREMENTS_PER_SECOND;
	
	reg [31:0] r_dividend = INCREMENTS_PER_MINUTE << 12;
	
	wire [31:0] w_time;
	
	assign w_time = i_time << 12;
	
	wire w_complete;
	wire [31:0] w_quotient;
	
	qdiv #(12, 32) qdiv
	(
		.i_clk(i_clk),
		.i_dividend(r_dividend),
		.i_divisor(w_time),
		.i_start(1'b1),
		.o_quotient_out(w_quotient),
		.o_complete(w_complete)
	);
	
	assign o_complete = w_complete;
	
	always @(posedge i_clk)
	begin
		o_rpm <= o_rpm;
		
		if (w_complete == 1'b1)
		begin
			o_rpm <= w_quotient;
		end
	end
	
endmodule
// Rolling Average
//
// Computes a rolling average of the 5 most recent inputs

module rolling_average
(
	input i_clk,
	
	input i_new_value,
	input [11:0] i_value,
	
	output reg [11:0] o_average
);
	
	// Implement a circular buffer with its index pointer
	// This implementation could be vastly improved by having it be inferred in blockram instead of ff's
	reg [11:0] r_circ_buffer [4:0];
	reg [3:0] r_circ_buffer_index = 4'd0;
	
	initial
	begin : circ_buffer_init
		integer i;
		
		for (i = 0; i < 5; i = i + 1)
		begin
			r_circ_buffer[i] = 12'd0;
		end
	end
	
	reg [19:0] r_added = 0;
	reg [31:0] r_interim = 0;
	reg [35:0] r_multiplied = 0;
	reg [11:0] r_result = 0;
	
	always @(r_circ_buffer[0], r_circ_buffer[1], r_circ_buffer[2], r_circ_buffer[3], r_circ_buffer[4])
	begin : comb_block
		integer i;
		
		r_added = 0;
		for (i = 0; i < 5; i = i + 1)
		begin
			r_added = r_added + r_circ_buffer[i];
		end
		
		r_interim = r_added << 12;
		
		r_multiplied = r_interim * 32'h00000333;  // times by 0.2, aka divide by 5
		
		r_result = r_multiplied[35:24];
		
		if (r_multiplied[23] == 1'b1)
		begin
			r_result = r_result + 12'd1;
		end
		
		o_average = r_result;
	end
	
	always @(posedge i_clk)
	begin
		r_circ_buffer_index <= r_circ_buffer_index;
		
		if (i_new_value == 1'b1)
		begin
			r_circ_buffer[r_circ_buffer_index] <= i_value;
			if (r_circ_buffer_index >= 4'd4)
			begin
				r_circ_buffer_index <= 4'd0;
			end
			else
			begin
				r_circ_buffer_index <= r_circ_buffer_index + 4'd1;
			end
		end
	end
	
endmodule
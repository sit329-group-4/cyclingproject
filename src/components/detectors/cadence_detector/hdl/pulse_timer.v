// Pulse Timer
//
// Records the time between pulses
// It is currently setup to report in increments of 1 ms

module pulse_timer
#(
	parameter CLK_SPEED = 50_000_000,
	parameter INCREMENTS_PER_SECOND = 1000
)
(
	input i_clk,
	
	input i_pulse,
	
	output reg [11:0] o_pulse_time = 0,
	output reg o_new_value = 0
);

	localparam TICK_VALUE = CLK_SPEED/INCREMENTS_PER_SECOND;

	reg [15:0] r_small_counter = 16'd0;
	
	// could remove this and assign directly to o_pulse_time but oh well
	reg [11:0] r_pulse_time = 12'd0;
	
	always @(posedge i_clk)
	begin
		o_pulse_time <= o_pulse_time;
		o_new_value <= 1'b0;
		
		r_small_counter <= r_small_counter + 16'd1;
		
		if (r_small_counter >= TICK_VALUE-1)
		begin
			r_pulse_time <= r_pulse_time + 12'd1;
			r_small_counter <= 16'd0;
		end
		
		if (i_pulse == 1'b1)
		begin
			o_pulse_time <= r_pulse_time;
			o_new_value <= 1'b1;
			r_pulse_time <= 12'd1; // Apparently this needs to be immediately initialised to 1 otherwise it causes timing errors
		end
	end
	
endmodule
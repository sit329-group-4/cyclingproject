// Count to RPM converter

module count_to_rpm
#(
	parameter real SAMPLE_PERIOD = 0.5  // seconds
)
(
	input [9:0] i_count,
	
	output [9:0] o_rpm
);
	
	localparam real SAMPLE_RATE = (1/SAMPLE_PERIOD) * 60;
	
	assign o_rpm = i_count * SAMPLE_RATE; 
	
endmodule
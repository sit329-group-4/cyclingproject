// Pulse Conditioner

module pulse_conditioner
(
	input i_clk,
	input i_pulse_train,
	
	output reg o_pulse
);

	reg r_prev_value;
	
	always @(posedge i_clk)
	begin
		o_pulse <= 1'b0;
		r_prev_value <= i_pulse_train;
		
		
		if (r_prev_value == 1'b0 && i_pulse_train == 1'b1)  // pos edge of the pulse train
		begin
			o_pulse <= 1'b1;
		end
	end

endmodule
transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/detectors/weight_detector/hdl/testbenches {E:/FPGA Projects/cyclingproject/src/components/detectors/weight_detector/hdl/testbenches/weight_detector_tb.v}
vlog -vlog01compat -work work +incdir+E:/FPGA\ Projects/cyclingproject/src/components/detectors/weight_detector/hdl {E:/FPGA Projects/cyclingproject/src/components/detectors/weight_detector/hdl/weight_detector.v}


module weight_detector_tb(
	input wire clk, 			//50Mhz FPGA Clock
	input wire btn, 			//Sensor Board Button (Pin D5 on the "Arduino Sensor Board")
	output wire MAIN_LED, 		//Sensor Board LED (Pin D2 on the "Arduino Sensor Board")
	output reg LED0, 			//Onboard FPGA LED	
	output reg LED1, 			//Onboard FPGA LED 
	output reg LED2, 			//Onboard FPGA LED 
	output reg LED3				//Onboard FPGA LED
);

wire [18:0] weight;

weight_detector WD (
	.clk(clk),
	.btn(btn),
	.main_led(MAIN_LED),
	.weight(weight)
);

initial begin
	//Turn off all leds
	LED0 = 0;
	LED1 = 0;
	LED2 = 0;
	LED3 = 0;

	$dumpfile("wd_tb.vcd");
	$dumpvars(1, weight_detector_tb);
	$finish;
end

always @(negedge clk) 
begin
	///SHOW WEIGHT USING ONBOARD LEDS
	if (weight > 10) 
	begin
		LED0 = 1;
	end
	
	if (weight > 20) 
	begin
		LED1 = 1;
	end
	
	if (weight > 30) 
	begin
		LED2 = 1;
	end
	
	if (weight > 40) 
	begin
		LED3 = 1;
	end
	
	$display("weight=%d", weight);
	$finish;
end

endmodule
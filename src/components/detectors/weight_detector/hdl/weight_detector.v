module weight_detector (
	input wire clk, 			  //50Mhz FPGA Clock
	input wire btn, 			  //Sensor Board Button (Pin D5 on the "Arduino Sensor Board")
	output reg main_led, 	  //Sensor Board LED (Pin D2 on the "Arduino Sensor Board")
	output reg[17:0] weight   //Weight output (Q10.8)
);

//Counter for clock division 
reg[32:0] cnt;

//Constants
reg[32:0] clkResetCount = 50000000;

//Trackers
reg[32:0] startClk;
reg prevState;

initial begin
	cnt = 32'h00000000;
	weight = 18'h0000000000;
	startClk = 32'h00000000;
	prevState = 0;
	weight = 0;
end 

always @(posedge clk) 
begin
	cnt = cnt + 1;
	
	//STATE: unpressed -> pressed 
	if (prevState == 0 && btn == 1) 
	begin
		main_led = 1;
		prevState = 1;
		startClk = cnt;
	end
	
	//STATE: pressed -> unpressed < 1 seconds
	else if (prevState == 1 && btn == 0 && (cnt - startClk) < clkResetCount && startClk > 0) 
	begin
		startClk = 0;
		weight = weight + (1 << 8);
	end
	
	//STATE: pressed -> pressed > 1 seconds
	else if (prevState == 1 && btn == 1 && (cnt - startClk) > clkResetCount && startClk > 0) 
	begin
		main_led = 0;
		weight = weight + (1 << 8);
		startClk = 0;
	end
	
	//STATE: pressed -> unpressed
	else if (prevState == 1 && btn == 0) 
	begin
		main_led = 0;
		prevState = 0;
	end
	
end

endmodule
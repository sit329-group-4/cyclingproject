module gear_register
(
	input clk,
	input up,
	input down,
	output [2:0] out
);

	reg [2:0] count = 0;
	
	assign out = count;
	
	always @(posedge clk)
	begin
		if (up && count < 4)
		begin
			count <= count + 3'd1;
		end
		else if (down && count > 0)
		begin
			count <= count - 3'd1;
		end
	end
	
endmodule 
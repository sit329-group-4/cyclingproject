// Note from Matt: I have gone through and refactored your top level component to get it more inline with the
// rest of the design. I have also rewritten some of the boilerplate stuff (debouncing) to use a tried and tested debouncer
// Should be better than making a divided clock signal (as far as correctness goes. Your way should have been fine mostly too
// New debouncing from: https://forum.digikey.com/t/debounce-logic-circuit-vhdl/12573

module gear_detector
(
	input i_clk,
	//gear_up and gear_down are the two push buttons
	input i_gear_up,
	input i_gear_down,
	
	output [2:0] o_gear_setting,  // The current gear setting, outputted as 0 - 4 representing 1 - 5
	output [6:0] o_seven_seg  // Debug 7 Segment display control output
);
	
	wire w_gear_up_debounced;
	wire w_gear_down_debounced;
	
	wire [2:0] w_gear_setting;  // output of gear registry
	
	assign o_gear_setting = w_gear_setting;
	
	//Debounce circuitry for push button 1 (gear_up)
	debounce
	#(
		.clk_freq(50_000_000),
		.stable_time(10)
	)
	gear_up_debouncer
	(
		.clk(i_clk),
		.reset_n(1'b1),
		.button(i_gear_up),
		.result(w_gear_up_debounced)
	);
	
	//Debounce circuitry for push button 2 (gear_down)
	debounce
	#(
		.clk_freq(50_000_000),
		.stable_time(10)
	)
	gear_down_debouncer
	(
		.clk(i_clk),
		.reset_n(1'b1),
		.button(i_gear_down),
		.result(w_gear_down_debounced)
	);
	
	gear_register gear_register
	(
		.clk(i_clk),
		.up(w_gear_up_debounced),
		.down(w_gear_down_debounced),
		.out(w_gear_setting)
	);
	
	seven_segment_display display
	(
		.bcd(w_gear_setting),
		.segment(o_seven_seg)
	);
	
endmodule 
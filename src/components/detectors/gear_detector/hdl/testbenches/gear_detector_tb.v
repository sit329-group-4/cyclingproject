`timescale 1ns / 1ns

// This testbench tests the gear register and ensures it is to spec
// The gear detector is not tested as its debouncers mean that the simulation will end up
// running for a very long time
module gear_register_tb;
	
	`include "../../../misc/tb_functions/assert_display.v"
	
	//Inputs
	reg t_clk = 1'b0;
	
	reg t_up = 1'b0;
	reg t_down = 1'b0;
	
	//Outputs
	wire [2:0] t_out;
	
	gear_register UUT
	(
		.clk(t_clk),
		.up(t_up),
		.down(t_down),
		.out(t_out)
	);
	
	always #1 t_clk = ~t_clk;
	
	initial
	begin : tb_block
		integer i;
		
		$dumpfile("out.vcd");
		$dumpvars(1, gear_register_tb);
		
		// Line up with the positive edge of the clk
		// After this, all clk period waits are 2
		@(posedge t_clk);
		#2;
		
		$display("Starting Tests");
		$display("Test 1: Confirming the initial gear is 1");
		assert_unit(t_out == 3'b000);
		
		#2;
		
		$display("Test 2: Confirm that pressing up goes up 1 gear");
		t_up = 1'b1;
		#1;
		t_up = 1'b0;
		assert_unit(t_out == 3'b001);
		
		#1;
		
		$display("Test 3: Confirm that pressing down goes down 1 gear");
		t_down = 1'b1;
		#1;
		t_down = 1'b0;
		assert_unit(t_out == 3'b000);
		
		#1;
		
		$display("Test 4: Check that 4 presses of up goes to gear 5 (slow)");
		for (i = 0; i < 4; i = i + 1)
		begin
			t_up = 1'b1;
			#2;
			t_up = 1'b0;
			#4;
		end
		assert_unit(t_out == 3'b100);
		
		$display("Test 5: Check that 4 presses of down goes to gear 1 (slow)");
		for (i = 0; i < 4; i = i + 1)
		begin
			t_down = 1'b1;
			#2;
			t_down = 1'b0;
			#4;
		end
		assert_unit(t_out == 3'b000);
		
		$display("Test 6: Check that 4 presses of up goes to gear 5 (fast)");
		t_up = 1'b1;
		#(2*4)
		t_up = 1'b0;
		assert_unit(t_out == 3'b100);
		
		$display("Test 7: Check that 4 presses of down goes to gear 1 (fast)");
		t_down = 1'b1;
		#(2*4)
		t_down = 1'b0;
		assert_unit(t_out == 3'b000);
		
		$display("Test 8: Gear down doesn't underflow");
		t_down = 1'b1;
		#2
		t_down = 1'b0;
		assert_unit(t_out == 3'b000);
		
		$display("Test 9: Gear up doesn't go past 5");
		// Go up to gear 5 fast
		t_up = 1'b1;
		#(2*4)
		// Now wait an extra clk cycle
		#2
		t_up = 1'b0;
		assert_unit(t_out == 3'b100);
		
		
		#10
		
		assert_unit_stop();
	end
	
endmodule 
// ===== Body Position Detector =====
// --- Design Doc ---
//		Description:
//			The body detection module will oversee working out what riding position the user is in while on the trainer.
//
//		System Integrations:
//			Input Systems (If any):
//				The body detection module doesn’t require input from other modules.
//			Output Systems (If any):
//				Wind Resistance Controller – It will send it the current cross section so it can work out how the
//												resistance will affect it
//												“The body position sensing module will detect which riding position the user
//												is currently in and output that to the modules responsible for calculating
//												wind resistance, drag and pedalling resistance.”
//
//		Inputs (and what they mean):
//			Body Position Sensors - • The body position sensing module can be simulated using a push button. With
//										each press of the push button, it will cycle through to a different riding position.
//									• The potentiometer can be used for minor adjustments to the riding
//										position (e.g., higher, or lower on the bike).
//
//		Outputs:
//			Body Position - The body position sensing module will output an integer representative of which riding
//							position the user is in.
//							The definition will be:
//								•	0 = Supertuck
//								•	1 = Back Down
//								•	2 = Drops
//								•	3 = Sitting
//								•	4 = Standing
//
//	--- Notes ---
//		Some weirdness seems to have happened where some of the stuff has been lost. Will assume that this file is correct for now

module body_position_detector
(
	input  i_clk,
	input  i_switch_1,
	output o_led_1,
	output reg [31:0] o_cda //Cda output
);
	
	reg  r_LED_1    = 1'b0;
	reg  r_Switch_1 = 1'b0;
	
	wire w_Switch_1;
	
	initial begin
		o_cda = 32'h00000;
	end
	
	
	// Instantiate Debounce Module
	Debounce_Switch Debounce_Inst(
		.i_Clk(i_clk), 
		.i_Switch(i_switch_1),
		.o_Switch(w_Switch_1)
	);
	
	
	assign w_Switch_1 = i_switch_1;
	
	// Purpose: Toggle LED output when w_Switch_1 is released.
	// Note: It might be better to make this a rising edge detector instead of a falling edge detector
	always @(posedge i_clk)
	begin
		r_Switch_1 <= w_Switch_1;         // Creates a Register

		if (w_Switch_1 == 1'b0 && r_Switch_1 == 1'b1)
		begin
			r_LED_1 <= ~r_LED_1;       // Toggle LED output
			o_cda = o_cda + (1 << 8);  // Increment the CDA
		end
	end
	
	assign o_led_1 = r_LED_1;
	
endmodule 

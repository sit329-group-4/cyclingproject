`timescale 1ns/1ns 

module body_position_detector_tb;
	
	`include "../../../misc/tb_functions/assert_display.v"
	
	//Inputs
	reg t_clk = 1'b0;
	reg t_switch_1 = 1'b0;
	
	//Outputs
	wire t_led_1;
	wire [31:0] t_cda;
	
	//Instantiate the Unit Under Test (UUT)
	body_position_detector UUT
	(
		.i_clk(t_clk),
		.i_switch_1(t_switch_1),
		.o_led_1(t_led_1),
		.o_cda(t_cda)
	);
	
	always #1 t_clk = ~t_clk;
	
	initial
	begin : tb_block
		// Note: this UUT only updates the output once the button is released
		
		integer i;
		integer tmp;
		
		$dumpfile("out.vcd");
		$dumpvars(1, body_position_detector_tb);
		
		// Sync up to the positive edge of the clk
		@(posedge t_clk);
		#2
		
		// Test 1
		$display("Starting Tests");
		$display("Test 1: Initialises the cda at 0");
		assert_unit(t_cda == 32'h00000000);
		
		// Test 2
		$display("Test 2: Check UUT only updates when button is released");
		
		t_switch_1 = 1'b1;
		for (i = 0; i < 10; i = i + 1)
		begin
			#2;
		end
		assert_unit(t_cda == 32'h00000000);
		t_switch_1 = 1'b0;
		#0
		assert_unit(t_cda != 32'h00000000);
		
		// Test 3
		$display("Test 3: 1 Button press increases the cda by 256 (or 1 in Q24.8)");
		assert_unit(t_cda == 32'h00000100);
		
		#2
		
		// Test 4
		$display("Test 4: Check that it hits max value after another 16,777,215 presses");
		
		for (i = 0; i < 16_777_214; i = i + 1)
		begin
			t_switch_1 = 1'b1;
			#2;
			t_switch_1 = 1'b0;
			#2;
		end
		#0
		assert_unit(t_cda == 32'hffffff00);
		
		// Test 5
		$display("Test 5: Check that it overflows back to 0");
		t_switch_1 = 1'b1;
		#2
		t_switch_1 = 1'b0;
		#2
		assert_unit(t_cda == 32'h00000000);
		
		// Test 6
		$display("Test 6: Check that it still increments");
		t_switch_1 = 1'b1;
		#2
		t_switch_1 = 1'b0;
		#2
		assert_unit(t_cda == 32'h00000100);
		
		#10;
		
		assert_unit_stop;
	end

endmodule

module biometrics_detector (
	input wire clk, 			  
	input wire btn, 			   	  
	output reg[17:0] bpm   
);

//Counter for clock division 
reg[31:0] cnt;

//Constants
reg[31:0] clkResetCount = 50000000;

//Trackers
reg[31:0] startClk;
reg prevState;


initial begin
	cnt = 32'h00000000;
	bpm = 18'h00000;
	startClk = 32'h00000000;
	
	prevState = 0;
	bpm = 0;
end 

always @(posedge clk) 
begin
	cnt = cnt + 1;
	
	//STATE: unpressed -> pressed 
	if (prevState == 0 && btn == 1) 
	begin
		prevState = 1;
		startClk = cnt;
	end
	
	//STATE: pressed -> unpressed < 1 seconds
	else if (prevState == 1 && btn == 0 && (cnt - startClk) < clkResetCount && startClk > 0) 
	begin
		startClk = 0;
		bpm = bpm + 1;
	end
	
	//STATE: pressed -> unpressed
	else if (prevState == 1 && btn == 0) 
	begin
		prevState = 0;
	end
	
	//Been 1 Minute reset everything
	if (cnt >= (clkResetCount * 60))
	begin
		cnt = 0;
		bpm = 0;
	end
	
end

endmodule

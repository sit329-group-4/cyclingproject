module biometrics_detector_tb(
	input wire clk, 			
	input wire btn, 			
	output reg LED0,	
	output reg LED1,		
	output reg LED2,		
	output reg LED3	
);

wire [17:0] bpm;

biometrics_detector bm (
	.clk(clk),
	.btn(btn),
	.bpm(bpm)
);


initial begin
	//Turn off leds
	LED0 = 0;
	LED1 = 0;
	LED2 = 0;
	LED3 = 0;
	
	$dumpfile("bm_tb.vcd");
	$dumpvars(1, biometrics_detector_tb);
	$finish;
end

always @(negedge clk) 
begin
	///SHOW WEIGHT USING ONBOARD LEDS
	if (bpm > 10) 
	begin
		LED0 = 1;
	end
	
	if (bpm > 20) 
	begin
		LED1 = 1;
	end
	
	if (bpm > 30) 
	begin
		LED2 = 1;
	end
	
	if (bpm > 40) 
	begin
		LED3 = 1;
	end
	
	if (bpm == 0)
	begin
		LED0 = 0;
		LED1 = 0;
		LED2 = 0;
		LED3 = 0;
	end
	
	$display("bpm=%d", bpm);
	$finish;
end

endmodule

from fxpmath import Fxp

with open("../power_inverse.memb", "w") as f:
    for i in range(0, 101):
        f.write(f"{Fxp(((i/100)**2.5)*24, False, 32, 10).bin()}\n")

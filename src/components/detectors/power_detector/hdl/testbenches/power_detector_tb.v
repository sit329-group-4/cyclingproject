`timescale 1ns/1ns

module power_detector_tb;
	
	reg [7:0] t_wheel_resistance = 0;
	reg [20:0] t_cadence = 0;
	
	wire [20:0] t_power;
	
	power_detector UUT
	(
		.i_wheel_resistance(t_wheel_resistance),
		.i_cadence(t_cadence),
		.o_power(t_power)
	);
	
	initial
	begin : tb
		integer i;
		integer o;
		
		$dumpfile("out.vcd");
		$dumpvars(1, power_detector_tb);
		
		#1;
		
		for (i = 0; i <= 100; i = i + 1)
		begin
			for (o = 0; o <= 2_097_151; o = o + 1)
			begin
				t_wheel_resistance = i;
				t_cadence = o;
				#1;
			end
		end
		
		#10;
		
		$stop;
	end
	
endmodule
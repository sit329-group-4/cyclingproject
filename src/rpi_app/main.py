import spidev
from time import sleep

spi = spidev.SpiDev()

spi.open(0, 0)

def read_gear_setting(spi_controller):
    gear_setting = 0b01000000
    spi_controller.xfer([gear_setting, 0b001 << 5, 0, 0, 0])
    return spi_controller.readbytes(4)

while True:
    print(read_gear_setting(spi))
    sleep(1000)